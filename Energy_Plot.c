void format_h(TH1F* h, int linecolor){
	h->SetLineWidth(3);
	h->SetLineColor(linecolor);
   // h->SetFillColor(linecolor);
}
// Will format the histogramas for readabality 


void Energy_Plot(){

format_h(EdepArtery,kRed);
format_h(EdepVein,kBlue);


auto sum_h = new TH1F("Energy","Energy Deposited per Event",700, 0, 700);

sum_h->Add(EdepVein);
sum_h->Add(EdepArtery);

TCanvas* c_sum = new TCanvas();
c_sum->Divide (2,1,0,0);
c_sum->cd(1);
sum_h->Draw();
c_sum->cd(2);
TVirtualPad* c2 = c_sum->GetPad(2);

EdepArtery->Draw();
EdepVein->Draw("Same");

TLegend* l = new TLegend(0.55,0.7,0.99,0.94);
l->SetHeader("Legend","C");
l->AddEntry(EdepVein,"Events originating from Vein","f");
l->AddEntry(EdepArtery,"Events originating from Artery","f");

l->Draw();



}