#include <string>

#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"

#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4VUserParallelWorld.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4RunManager.hh"

#include "SourceWorldConstruction.hh"

#include "SourceMaterial.hh"
#include "SourceWorldMessenger.hh"

#include "SourceModel.hh"
#include "Intrabeam.hh"
//#include "FlexiSource.hh"
#include "MicroSelectronV2.hh"
#include "MicroSelectronV2Shielded.hh"
#include "CustomSource.hh"
#include "CustomSourceShielded.hh"
#include "BebigHdrIr.hh"
#include "GenericHDR.hh"
#include "FlexiNoShield.hh"
#include "GammaMedPlus.hh"
#include "ControlPoint.hh"

SourceWorldConstruction::SourceWorldConstruction(G4String &parallelWorldName)
    : G4VUserParallelWorld(parallelWorldName)
{
    iCoreA = 192;
    iCoreZ = 77;
    sourceGeometry = "MicroSelectronV2";
    sourceActiveCore = "G4_Ir";
    fShieldRadius = 0.725 * mm;
    pMaterial = new SourceMaterial();
    messenger = new SourceWorldMessenger(this);
    sourceLoaded = false;
}

SourceWorldConstruction::~SourceWorldConstruction()
{
    delete pMaterial;
    delete messenger;

    if (sourceLoaded) {
        delete source;
    }
}

void SourceWorldConstruction::Construct()
{
    G4VPhysicalVolume *ghostWorld = GetWorld();
    G4LogicalVolume *worldLogical = ghostWorld->GetLogicalVolume();
    worldLogical->SetMaterial(NULL);
    worldLogical->SetVisAttributes(G4VisAttributes::Invisible);
    pMaterial->DefineMaterials();
    loadSource();
}

void SourceWorldConstruction::selectSourceModel(G4String val)
{
    sourceGeometry = val;
    G4cout << "The source is set to " << val << G4endl;
}

void SourceWorldConstruction::selectSourceCore(G4String val)
{
    sourceActiveCore = val;
    G4cout << "The core is set to " << val << G4endl;
}

void SourceWorldConstruction::setCoreA(G4int coreA)
{
    iCoreA = coreA;
}

void SourceWorldConstruction::setCoreZ(G4int coreZ)
{
    iCoreZ = coreZ;
}

void SourceWorldConstruction::setShieldRadius(G4double radius)
{
    fShieldRadius = radius;
    G4cout << "The shield radius is set to " << radius / mm << " mm" << G4endl;
}

void SourceWorldConstruction::addParticleGenerator(PrimaryGeneratorAction *primGen)
{
    particleGenerators.push_back(primGen);
}

void SourceWorldConstruction::loadSource()
{
    /*
    if (sourceGeometry == "FlexiSource")
    {
        this->source = new FlexiSource(sourceActiveCore);
    }
    */

    if (sourceGeometry == "MicroSelectronV2")
    {
        this->source = new MicroSelectronV2(sourceActiveCore);
    }
    else if (sourceGeometry == "MicroSelectronV2Shielded")
    {
        this->source = new MicroSelectronV2Shielded(sourceActiveCore, fShieldRadius);
    }
    else if (sourceGeometry == "FlexiNoShield")
    {
        this->source = new FlexiNoShield(sourceActiveCore);
    }
    else if (sourceGeometry == "BebigHdrIr")
    {
        this->source = new BebigHdrIr(sourceActiveCore);
    }
    else if (sourceGeometry == "GenericHDR")
    {
        this->source = new GenericHDR(sourceActiveCore);
    }
    else if (sourceGeometry == "CustomSource")
    {
        this->source = new CustomSource(sourceActiveCore);
    }
    else if (sourceGeometry == "CustomSourceShielded")
    {
        this->source = new CustomSourceShielded(sourceActiveCore, fShieldRadius);
    }
    else if (sourceGeometry == "GammaMedPlus")
    {
        this->source = new GammaMedPlus(sourceActiveCore);
    }

    this->source->setCoreA(iCoreA);
    this->source->setCoreZ(iCoreZ);
    this->source->createSource();
    sourceLoaded = true;
}

void SourceWorldConstruction::placeControlPoint(ControlPoint &cpt)
{
    this->source->cleanSource();
    this->source->placeSource(cpt, GetWorld());
    this->particleGenerators[0]->setControlPoint(cpt, this->source);
}
