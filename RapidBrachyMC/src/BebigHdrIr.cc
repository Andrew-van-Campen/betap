// Gabriel Famulari <gabriel.famulari@mail.mcgill.ca>

#include <iostream>
#include <string>

#include "BebigHdrIr.hh"
#include "SourceMaterial.hh"
#include "DetectorConstruction.hh"
#include "PositionList.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"

#include "G4Sphere.hh"
#include "G4Orb.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"

#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

#include "G4VisAttributes.hh"

#include "ControlPoint.hh"

using namespace std;

BebigHdrIr::BebigHdrIr(G4String core)
{
    pMaterial = new SourceMaterial();
    SetupDimensions();
    fCore = core;
}

BebigHdrIr::BebigHdrIr()
{
    pMaterial = new SourceMaterial();

    SetupDimensions();
    fPositionFilename = "";
    fCore = "G4_Ir";
}

BebigHdrIr::~BebigHdrIr()
{
    delete pMaterial;
}

void BebigHdrIr::SetCore(G4String core)
{
    fCore = core;
}

void BebigHdrIr::SetupDimensions()
{
    // Active core
    sourceRadius = 0.3 * mm;
    halfSourceHeight = 1.75 * mm;

    // Active core parameter
    coreRadius = sourceRadius;
    coreHalfZ = halfSourceHeight;

    // capsule covering core
    capsuleInnerRadius = 0.35 * mm;
    capsuleOuterRadius = 0.50 * mm;
    capsuleHalfHeight  = 1.75 * mm;

    // NOT USED IN GEOMETRY!!!!!!
    capsuleTipMinConRadius = 0.175 * mm;
    capsuleTipMaxConRadius = 0.50 * mm;
    capsuleTipHalfHeight = 0.05 * mm;

    //Capsule cylinder at top
    capsuleSolidFrontInnerRadius = 0.0 * mm;
    capsuleSolidFrontOuterRadius = 0.50 * mm;
    capsuleSolidFrontHalfHeight = 0.42 * mm;

    // Conical air capsule tip at the bottom
    capsuleTipAirMinConRadius = 0.0 * mm;
    capsuleTipAirMaxConRadius = 0.35 * mm;
    capsuleTipAirHalfHeight = 0.1 * mm;

    // Extra capsule cylinder between core and cable containing the air capsule tip
    capsuleSolidBottomInnerRadius = 0.0 * mm;
    capsuleSolidBottomOuterRadius = 0.50 * mm;
    capsuleSolidBottomHalfHeight = 0.275 * mm;

    cableRadius = 0.50 * mm;
    halfCableHeight = 3.0 * mm;

    startPhi = 0.;
    spanPhi = 2. * CLHEP::pi;
    spanTheta = CLHEP::pi / 2.;
}

void BebigHdrIr::createSource()
{
    G4Material *SS316L = pMaterial->GetMat("SS316L");
    G4Material *Pt = pMaterial->GetMat("G4_Pt");
    G4Material *air = pMaterial->GetMat("G4_AIR");
    G4Material *coreMaterial = pMaterial->GetMat(fCore);

    // Source
    G4Tubs *source = new G4Tubs("source", 0.0 * mm, sourceRadius,
                                    halfSourceHeight, startPhi, spanPhi);

    source_log = new G4LogicalVolume(source, coreMaterial, "source_log");
    G4VisAttributes *source_att = new G4VisAttributes();
    source_att->SetColor(1, 0, 0);
    source_att->SetVisibility(true);
    source_att->SetForceSolid(true);
    source_log->SetVisAttributes(source_att);

    // Air Gap
    // Conical air capsule tip placed inside the solid steel tube
    G4Tubs *airGapCyl = new G4Tubs("airGapCyl", sourceRadius,
                       capsuleInnerRadius, capsuleHalfHeight,
                       startPhi, spanPhi);
    G4Cons *airGapCon = new G4Cons("airGapCon", 0.0 * mm, capsuleTipAirMinConRadius,
                       0.0 * mm, capsuleTipAirMaxConRadius, capsuleTipAirHalfHeight,
                       startPhi, spanPhi);
    G4VSolid *airGap = new G4UnionSolid("airGap", airGapCyl, airGapCon, 0,
                                              G4ThreeVector(0.0, 0.0, -(capsuleHalfHeight + capsuleTipAirHalfHeight)));
    
    airGap_log = new G4LogicalVolume(airGap, air, "airGap_log");
    G4VisAttributes *airGap_att = new G4VisAttributes();
    airGap_att->SetColor(0.5, 0.5, 0.5);
    airGap_att->SetVisibility(true);
    airGap_att->SetForceSolid(false);
    airGap_log ->SetVisAttributes(airGap_att);

    // Capsule
    // Hollow capsule part containing the source
    G4Tubs *capsuleHollow = new G4Tubs("capsuleHollow", capsuleInnerRadius,
				       capsuleOuterRadius, capsuleHalfHeight,
				       startPhi, spanPhi);
    // capsule solid part in the top
    G4Tubs *capsuleSolidFront = new G4Tubs("capsuleSolidFront", 0.0 * mm,
                                           capsuleSolidFrontOuterRadius, capsuleSolidFrontHalfHeight,
                                           startPhi, spanPhi);
    // capsule cylinder in the back
    G4Tubs *capsuleSolidBack = new G4Tubs("capsuleSolidBack", 0.0 * mm,
                                           capsuleSolidBottomOuterRadius, capsuleSolidBottomHalfHeight,
                                           startPhi, spanPhi);
    // Join capsule front solid cylinder with Hollow capsule
    G4VSolid *capsuleHalf1 = new G4UnionSolid("capsuleHalf1", capsuleHollow, capsuleSolidFront, 0,
                                              G4ThreeVector(0.0, 0.0, (capsuleHalfHeight + capsuleSolidFrontHalfHeight)));
    // Add the cylinder in the back
    G4VSolid *capsuleHalf2 = new G4UnionSolid("capsuleHalf2", capsuleHalf1, capsuleSolidBack, 0,
                                              G4ThreeVector(0.0, 0.0, -(capsuleHalfHeight + capsuleSolidBottomHalfHeight)));
    // Remove air gap cone
    G4VSolid *capsule = new G4SubtractionSolid("capsule", capsuleHalf2, airGapCon, 0,
                                              G4ThreeVector(0.0, 0.0, -(capsuleHalfHeight + capsuleTipAirHalfHeight)));

    capsule_log = new G4LogicalVolume(capsule, SS316L, "capsule_log");
    G4VisAttributes *capsule_att = new G4VisAttributes();
    capsule_att->SetColor(0.5, 0.5, 0.5);
    capsule_att->SetVisibility(true);
    capsule_att->SetForceSolid(false);
    capsule_log->SetVisAttributes(capsule_att);

    // Steel Cable
    G4Tubs *cable = new G4Tubs("cable", 0.0 * mm, cableRadius, halfCableHeight, startPhi, spanPhi);
    
    cable_log = new G4LogicalVolume(cable, SS316L, "cable_log");
    G4VisAttributes *cable_att = new G4VisAttributes();
    cable_att->SetColor(0, 0, 1);
    cable_att->SetVisibility(true);
    cable_att->SetForceSolid(true);
    cable_log->SetVisAttributes(cable_att);
}

void BebigHdrIr::placeSource(ControlPoint cpt, G4VPhysicalVolume *mother)
{
    for (size_t i = 0; i < cpt.dwellPositions.size(); i++)
    {
        G4ThreeVector shift = cpt.dwellPositions[i].pos;
        G4double angle = cpt.dwellPositions[i].angle;
    // G4cout << "Source: " << "i: " <<i<< "  Position: " << shift << " angle: " << angle << G4endl;

        G4ThreeVector cableOffset = G4ThreeVector(0, 0, -halfSourceHeight - 2 * capsuleSolidBottomHalfHeight - halfCableHeight);

        // Direction Vector of Dwell position
        G4ThreeVector Zunit = G4ThreeVector(0.0,0.0,1.0); //Source Is Originally defined pointing in +Z
        G4ThreeVector V = G4ThreeVector(cpt.dwellPositions[i].rot[0],
                                        cpt.dwellPositions[i].rot[1],
                                        cpt.dwellPositions[i].rot[2]).unit();

        // Angle Between V and +Z Axis
        G4double Vangle = Zunit.angle(V);

        // Axis for which rotation (Vangle) will be used to define rotation matrix
        G4ThreeVector Vcross = G4ThreeVector(0,0,0);

        // Check if V Parallel to Z
        if (V.isParallel(Zunit, 1E-4)) // IF parallel to Z-axis
        {
          G4cout << "*** HowParallel: " << V.howParallel(Zunit) << G4endl;

          if (fabs(Vangle - 3.14159) < 1E-4) // If Angle = Pi then Vcross = -X
          {
            Vcross = G4ThreeVector(-1.0,0.0,0.0); // -X
            G4cout << "WARNING: Source Orientation Parallel with NEGATIVE Z axis" << G4endl;
          }
          else // ASSUMPTION! If Angle == 0
          {
            Vcross = G4ThreeVector(1.0,0.0,0.0); // +X
            G4cout << "WARNING: Source Orientation Parallel with POSITIVE Z axis" << G4endl;
          }
        }
        else // IF NOT parallel to Z-axis
        {
          Vcross = V.cross(Zunit).unit();
        }

        // DEFINE ROTATION MATRIX To line up source with direction vector V
        G4RotationMatrix * Vrot = new G4RotationMatrix();
        Vrot->rotate(Vangle, Vcross);
        Vrot->invert();

        // Placing Components ////
        G4cout << "Placing source at: " << shift  << G4endl;
        G4cout << "Source Orientation Vector:" << V << G4endl;

        // *** G4PVPplacement using G4Transform3D to apply translate/rotation in mother frame of reference
        // Source
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift), "source",
                                                        this->source_log, mother, false, i));
        // Capsule
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift), "capsule",
                                                        this->capsule_log, mother, false, i));
        // Air Gap
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift), "airGap",
                                                        this->airGap_log, mother, false, i));
        // Cable
        cableOffset.transform(*Vrot); // Transform cable shift in rotated frame of reference
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift+cableOffset), "cable",
                                                        this->cable_log, mother, false, i));
    }

    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}

void BebigHdrIr::cleanSource()
{
    for (size_t i = 0; i < this->placedObjects.size(); i++)
    {
        delete this->placedObjects[i];
    }
    this->placedObjects.clear();
    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}
