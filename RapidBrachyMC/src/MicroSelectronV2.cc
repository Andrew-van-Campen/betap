//////////////////////////////////////////////////////////////////////////
//
// Author: Shirin A. Enger <shirin@enger.se>
//
// License & Copyright
// ===================
//
// Copyright 2015 Shirin A Enger <shirin@enger.se>
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////

// Edited by Marc Morcos 2019-08-09 to add source orientation functionality
// Works with source orientation

#include <iostream>
#include <string>

#include "MicroSelectronV2.hh"
#include "SourceMaterial.hh"
#include "DetectorConstruction.hh"
#include "PositionList.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"

#include "G4Sphere.hh"
#include "G4Orb.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"

#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

#include "G4VisAttributes.hh"

#include "ControlPoint.hh"

using namespace std;

MicroSelectronV2::MicroSelectronV2(G4String core)
{
    pMaterial = new SourceMaterial();
    SetupDimensions();
    fCore = core;
}

MicroSelectronV2::MicroSelectronV2()
{
    pMaterial = new SourceMaterial();

    SetupDimensions();
    fPositionFilename = "";
    fCore = "G4_Ir";
}

MicroSelectronV2::~MicroSelectronV2()
{
    delete pMaterial;
}

void MicroSelectronV2::SetCore(G4String core)
{
    fCore = core;
}

void MicroSelectronV2::SetupDimensions()
{
    // Active core
    sourceRadius = 0.325 * mm;
    halfSourceHeight = 1.74 * mm;

    // Tip of core
    sourceTipMinConRadius = 0.265 * mm;
    sourceTipMaxConRadius = 0.325 * mm;
    sourceTipHalfHeight = 0.03 * mm;

    // Active core parameters
    coreRadius = sourceRadius;
    coreHalfZ = halfSourceHeight + 2 * sourceTipHalfHeight;

    capsuleInnerRadius = sourceRadius;
    capsuleOuterRadius = 0.45 * mm;
    capsuleHalfHeight = 1.8 * mm;

    // Extra capsule cylinder between core and cable
    capsuleHalfExtraHeight = 0.275 * mm;

    // Conical capsule tip
    capsuleTipMinConRadius = 0.35 * mm;
    capsuleTipMaxConRadius = 0.45 * mm;
    capsuleTipHalfHeight = 0.075 * mm;

    cableRadius = 0.35 * mm;
    halfCableHeight = 1.0 * mm;

    startPhi = 0.;
    spanPhi = 2. * CLHEP::pi;
    spanTheta = CLHEP::pi / 2.;
}

void MicroSelectronV2::createSource()
{
    G4Material *steel = pMaterial->GetMat("Steel");
    G4Material *Pt = pMaterial->GetMat("G4_Pt");
    G4Material *coreMaterial = pMaterial->GetMat(fCore);

    // Source definition

    G4Tubs *sourceCore = new G4Tubs("sourceCore", 0.0 * mm, sourceRadius,
                                    halfSourceHeight, startPhi, spanPhi);

    G4Cons *sourceFrontTip = new G4Cons("sourceFrontTip", 0.0 * mm, sourceTipMaxConRadius,
                                        0.0 * mm, sourceTipMinConRadius,
                                        sourceTipHalfHeight, startPhi, spanPhi);

    G4Cons *sourceBackTip = new G4Cons("sourceBackTip", 0.0 * mm, sourceTipMinConRadius,
                                       0.0 * mm, sourceTipMaxConRadius,
                                       sourceTipHalfHeight, startPhi, spanPhi);

    G4VSolid *source1 = new G4UnionSolid("source1", sourceCore, sourceFrontTip, 0, G4ThreeVector(0.0, 0.0, halfSourceHeight + sourceTipHalfHeight));
    G4VSolid *source = new G4UnionSolid("source", source1, sourceBackTip, 0, G4ThreeVector(0.0, 0.0, -(halfSourceHeight + sourceTipHalfHeight)));

    source_log = new G4LogicalVolume(source, coreMaterial, "source_log");
    G4VisAttributes *source_att = new G4VisAttributes();
    source_att->SetColor(1, 0, 0);
    source_att->SetVisibility(true);
    source_att->SetForceSolid(true);
    source_log->SetVisAttributes(source_att);

    // Capsule starts from core radius.
    G4Tubs *capsuleSolid = new G4Tubs("capsuleSolid", capsuleInnerRadius,
                                      capsuleOuterRadius, capsuleHalfHeight,
                                      startPhi, spanPhi);

    G4Tubs *capsuleExtraSolid = new G4Tubs("capsuleExtraSolid", 0.0 * mm,
                                           capsuleOuterRadius, capsuleHalfExtraHeight,
                                           startPhi, spanPhi);

    // Conical capsule tip near the cable
    G4Cons *capsuleBackTip = new G4Cons("capsuleBackTip", 0.0 * mm, capsuleTipMinConRadius,
                                        0.0 * mm, capsuleTipMaxConRadius, capsuleTipHalfHeight,
                                        startPhi, spanPhi);
    // Spherical capsule tip
    G4Sphere *capsuleFrontTip = new G4Sphere("capsuleFrontTip", 0.0 * mm, capsuleOuterRadius,
                                             startPhi, spanPhi, startPhi, spanTheta);

    // Join capsule cylinder with extra cylinder bit
    G4VSolid *capsuleHalf1 = new G4UnionSolid("capsuleHalf1", capsuleSolid, capsuleExtraSolid, 0,
                                              G4ThreeVector(0.0, 0.0, -(capsuleHalfHeight + capsuleHalfExtraHeight)));

    // Add in the conical back capsule tip
    G4VSolid *capsuleHalf2 = new G4UnionSolid("capsuleHalf2", capsuleHalf1, capsuleBackTip, 0,
                                              G4ThreeVector(0.0, 0.0, -(capsuleHalfHeight + 2 * capsuleHalfExtraHeight + capsuleTipHalfHeight)));

    // Add the front spherical capsule tip
    G4VSolid *capsule = new G4UnionSolid("capsule", capsuleHalf2, capsuleFrontTip, 0,
                                         G4ThreeVector(0.0, 0.0, capsuleHalfHeight));

    capsule_log = new G4LogicalVolume(capsule, steel, "capsule_log");
    G4VisAttributes *capsule_att = new G4VisAttributes();
    capsule_att->SetColor(0.5, 0.5, 0.5);
    capsule_att->SetVisibility(true);
    capsule_att->SetForceSolid(false);
    capsule_log->SetVisAttributes(capsule_att);

    // Steel Cable
    G4Tubs *cable_vol = new G4Tubs("cable_vol", 0.0 * mm, cableRadius, halfCableHeight, startPhi, spanPhi);
    cable_log = new G4LogicalVolume(cable_vol, steel, "cable_log");
    G4VisAttributes *cable_att = new G4VisAttributes();
    cable_att->SetColor(0, 0, 1);
    cable_att->SetVisibility(true);
    cable_att->SetForceSolid(true);
    cable_log->SetVisAttributes(cable_att);
}

void MicroSelectronV2::placeSource(ControlPoint cpt, G4VPhysicalVolume *mother)
{
    for (size_t i = 0; i < cpt.dwellPositions.size(); i++)
    {
        G4ThreeVector shift = cpt.dwellPositions[i].pos;
        G4double angle = cpt.dwellPositions[i].angle;
	// G4cout << "Source: " << "i: " <<i<< "  Position: " << shift << " angle: " << angle << G4endl;

        G4ThreeVector cableOffset = G4ThreeVector(0, 0, -halfSourceHeight - 2 * sourceTipHalfHeight - 2. * capsuleHalfExtraHeight - 2 * capsuleTipHalfHeight - halfCableHeight);

        // Direction Vector of Dwell position
        G4ThreeVector Zunit = G4ThreeVector(0.0,0.0,1.0); //Source Is Originally defined pointing in +Z
        G4ThreeVector V = G4ThreeVector(cpt.dwellPositions[i].rot[0],
                                        cpt.dwellPositions[i].rot[1],
                                        cpt.dwellPositions[i].rot[2]).unit();

        // Angle Between V and +Z Axis
        G4double Vangle = Zunit.angle(V);

        // Axis for which rotation (Vangle) will be used to define rotation matrix
        G4ThreeVector Vcross = G4ThreeVector(0,0,0);

        // Check if V Parallel to Z
        if (V.isParallel(Zunit, 1E-4)) // IF parallel to Z-axis
        {
          G4cout << "*** HowParallel: " << V.howParallel(Zunit) << G4endl;

          if (fabs(Vangle - 3.14159) < 1E-4) // If Angle = Pi then Vcross = -X
          {
            Vcross = G4ThreeVector(-1.0,0.0,0.0); // -X
            G4cout << "WARNING: Source Orientation Parallel with NEGATIVE Z axis" << G4endl;
          }
          else // ASSUMPTION! If Angle == 0
          {
            Vcross = G4ThreeVector(1.0,0.0,0.0); // +X
            G4cout << "WARNING: Source Orientation Parallel with POSITIVE Z axis" << G4endl;
          }
        }
        else // IF NOT parallel to Z-axis
        {
          Vcross = V.cross(Zunit).unit();
        }

        // DEFINE ROTATION MATRIX To line up source with direction vector V
        G4RotationMatrix * Vrot = new G4RotationMatrix();
        Vrot->rotate(Vangle, Vcross);
        Vrot->invert();

        // Placing Components ////
        G4cout << "Placing source at: " << shift  << G4endl;
        G4cout << "Source Orientation Vector:" << V << G4endl;

        // *** G4PVPplacement using G4Transform3D to apply translate/rotation in mother frame of reference
        // Source
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift), "source",
                                                        this->source_log, mother, false, i));
        // Capsule
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift), "capsule",
                                                        this->capsule_log, mother, false, i));
        // Cable
        cableOffset.transform(*Vrot); // Transform cable shift in rotated frame of reference
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift+cableOffset), "cable",
                                                        this->cable_log, mother, false, i));
    }

    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}

void MicroSelectronV2::cleanSource()
{
    for (size_t i = 0; i < this->placedObjects.size(); i++)
    {
        delete this->placedObjects[i];
    }
    this->placedObjects.clear();
    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}
