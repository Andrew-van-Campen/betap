#include <string>
#include <fstream>
#include <vector>

#include "globals.hh"
#include "string_utils.hh"
#include "G4ThreeVector.hh"
#include "G4SystemOfUnits.hh"
#include "ControlPoint.hh"
#include "TreatmentPlan.hh"

TreatmentPlan::TreatmentPlan(G4String planFile)
{
    this->inputFile = planFile;
    this->readPlanFile(planFile);
}

int TreatmentPlan::readPlanFile(G4String planFile)
{
    if (planFile.empty())
        return 0;

    std::vector<std::string> temp_vector;

    std::ifstream dataFile(planFile);
    std::string buffer;

    if (dataFile.good() != 1)
    {
        std::cerr << "Could not find file: " << planFile << std::endl;
        return 1;
    }

    std::getline(dataFile, buffer);
    if (buffer != "Treatment Plan")
        throw "Expected Treatment Plan, got:" + buffer;

    std::getline(dataFile, buffer);
    if (buffer.find("Control Points") == std::string::npos)
        throw "Expected Control Points, got:" + buffer;
    size_t num_cpt = std::stoi(split(buffer, ' ')[0]);
    std::cout << "Number of control points: " << num_cpt << std::endl;

    for (size_t i = 0; i < num_cpt; i++)
    {
        ControlPoint cpt;
        cpt.readCpt(dataFile);
        this->controlPoints.push_back(cpt);
    }

    dataFile.close();
    return 0;
}
