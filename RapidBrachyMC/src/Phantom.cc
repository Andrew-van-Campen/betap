#include <locale>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <cctype>
#include "G4SystemOfUnits.hh"
#include "globals.hh"
#include "Phantom.hh"
#include "string_utils.hh"
#include "PatientMaterial.hh"

Phantom::Phantom()
{
    pMaterial = new PatientMaterial();
}
Phantom::~Phantom() {
    delete densities;
    delete material_ids;
    delete real_ids;
}

void Phantom::loadFromEGSphant(G4String phant_filename)
{
    G4double dummy;
    size_t total_vox;
    std::vector<G4String> mats;
    std::string buffer, buffer2;
    std::stringstream convertor;

    G4cout << "Loading phantom: " << phant_filename << G4endl;

    std::ifstream phantom_file(phant_filename);
    if (!phantom_file)
    {
      G4cout << "Could not find " << phant_filename << G4endl;
        exit(-1);
    }
    this->p_filename = phant_filename;
    std::getline(phantom_file, buffer);
    convertor.str(buffer);

    convertor >> num_mats;
    for (int i = 0; i < num_mats; i++)
    {
        std::getline(phantom_file, buffer);
        material_names.push_back(trim(buffer));
    }
    std::getline(phantom_file, buffer); // dummy

    std::getline(phantom_file, buffer); // number of voxels
    sscanf(buffer.c_str(), "%i %i %i", &num_voxels[0], &num_voxels[1], &num_voxels[2]);
    for (int i = 0; i < 3; i++)
    {
        std::getline(phantom_file, buffer);
        sscanf(buffer.c_str(), "%lf %lf %*s", &topleft[i], &dummy);
        voxel_size[i] = dummy - topleft[i];
        topleft[i] *= cm;
        voxel_size[i] *= cm;
    }

    total_vox = num_voxels[0] * num_voxels[1] * num_voxels[2];
    material_ids = new size_t[total_vox];
    real_ids = new size_t[total_vox];
    int big_counter = 0;
    for (int k = 0; k < num_voxels[2]; k++)
    {
        for (int j = 0; j < num_voxels[1]; j++)
        {
            std::getline(phantom_file, buffer); // Material values
            const char *buf_dup = buffer.c_str();
            for (int i = 0; i < num_voxels[0]; i++)
            {
                // If statement handles EGSphant with more than 9 Materials
                // after Material #1-9 it goes A,B,C,.. etc
                if (buf_dup[i] > 64){
                  material_ids[big_counter] = buf_dup[i] - 'A' + 9;
                }else{
                  material_ids[big_counter] = buf_dup[i] - '0' - 1;
                }
                big_counter++;
            }
        }
        std::getline(phantom_file, buffer);
    }

    big_counter = 0;
    densities = new G4double[total_vox];

    for (int k = 0; k < num_voxels[2]; k++)
    {
        for (int j = 0; j < num_voxels[1]; j++)
        {
            std::getline(phantom_file, buffer); // Density values
            std::vector<std::string> densityValues = split(buffer, ' ');
            for (int i = 0; i < num_voxels[0]; i++)
            {
                densities[big_counter] = std::atof(densityValues[i].c_str()) * g / cm3;
                big_counter++;
            }
        }
        std::getline(phantom_file, buffer);
    }

    phantom_file.close();

    this->constructMaterials();
}

void Phantom::constructMaterials()
{
  G4cout << "Materials found in phantom: ";
    for (size_t i = 0; i < material_names.size(); i++)
    {
      G4cout << material_names[i] << " ";
        pMaterial->CreateFor(material_names[i]);
    }

    size_t total_vox = num_voxels[0] * num_voxels[1] * num_voxels[2];
    for (size_t i = 0; i < total_vox; i++)
    {
        size_t tissue_number = material_ids[i];
        G4double density = densities[i];
        real_ids[i] = pMaterial->GetMatIndex(tissue_number, density);
    }
}

G4double Phantom::getXmin()
{
    return topleft[0];
}

G4double Phantom::getYmin()
{
    return topleft[1];
}

G4double Phantom::getZmin()
{
    return topleft[2];
}

G4double Phantom::getXmax()
{
    return (topleft[0] + num_voxels[0] * voxel_size[0]);
}

G4double Phantom::getYmax()
{
    return (topleft[1] + num_voxels[1] * voxel_size[1]);
}

G4double Phantom::getZmax()
{
    return (topleft[2] + num_voxels[2] * voxel_size[2]);
}

G4double Phantom::getXsize()
{
    return voxel_size[0];
}

G4double Phantom::getYsize()
{
    return voxel_size[1];
}

G4double Phantom::getZsize()
{
    return voxel_size[2];
}

G4double Phantom::getNumVoxelsX()
{
    return num_voxels[0];
}

G4double Phantom::getNumVoxelsY()
{
    return num_voxels[1];
}

G4double Phantom::getNumVoxelsZ()
{
    return num_voxels[2];
}

std::vector<G4String> Phantom::getMatNames()
{
    return material_names;
}

size_t *Phantom::getMatNumbers()
{
    return real_ids;
}

size_t* Phantom::getUniqueMatNumbers()
{
    return material_ids;
}

G4double *Phantom::getDensities()
{
    return densities;
}

std::vector<G4Material *> Phantom::getAllMaterials()
{
    return this->pMaterial->GetFlatMatVector();
}
