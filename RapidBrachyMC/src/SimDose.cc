#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "DetectorConstruction.hh"
#include "SimDose.hh"

SimDose::SimDose(size_t total_voxels, bool include_temp)
{
    this->dose = std::vector<G4double>(total_voxels, 0.0);
    this->uncert = std::vector<G4double>(total_voxels, 0.0);

    if (include_temp)
    {
        this->dose_tmp = std::vector<G4double>(total_voxels, 0.0);
        this->current_hist = std::vector<G4long>(total_voxels, 0.0);
    }
}

SimDose::~SimDose()
{
}

void SimDose::deposit_dose(G4double dose_val, size_t vox, G4long hist)
{
    // Deposit energy in dose tally. This algorithm tracks which primary history
    // is depositing dose in each voxel. For proper uncertainty calculations, the
    // dose contribution to a voxel *for a given primary history* must be squared.
    // See:
    // Walters, B. R. B., I. Kawrakow, and D. W. O. Rogers.
    // "History by history statistical estimators in the BEAM code system."
    // Medical physics 29.12 (2002): 2745-2752.
    if (hist == this->current_hist[vox])
    {
        this->dose_tmp[vox] += dose_val;
    }
    else
    {
        this->dose[vox] += this->dose_tmp[vox];
        this->uncert[vox] += this->dose_tmp[vox] * this->dose_tmp[vox];
        this->dose_tmp[vox] = dose_val;
        this->current_hist[vox] = hist;
    }
}

void SimDose::deposit_dose_event(G4double dose_val, size_t vox)
{
    // Deposit energy into a voxel after a whole event has been simulated.
    this->dose[vox] += dose_val;
    this->uncert[vox] += dose_val * dose_val;
}

void SimDose::finalize_edep()
{
    // Method to be called at the end of a run. The leftover edeps in dose_hist will be
    // added to the dose and uncert tallies.
    for (size_t i = 0; i < this->dose.size(); i++)
    {
        this->dose[i] += this->dose_tmp[i];
        this->uncert[i] += this->dose_tmp[i] * this->dose_tmp[i];
    }
}

void SimDose::merge_with(SimDose* other_simdose)
{
    for (size_t i = 0; i < this->dose.size(); i++)
    {
        this->dose[i] += other_simdose->dose[i];
        this->uncert[i] += other_simdose->uncert[i];
    }
}

void SimDose::finalize_tally(G4long hist, G4double norm, DetectorConstruction *phant)
{
    G4double f_hist = hist;
    G4double vol = phant->getVoxelVolume();
    G4double *densities = phant->getDensities();

    // Normalize the dose tallies and calculate relative uncertainty in voxels.
    for (size_t i = 0; i < this->dose.size(); i++)
    {
        this->dose[i] /= f_hist;
        this->uncert[i] /= f_hist;

        if (this->dose[i] > 0.0)
        {
            G4double d_sq = (this->uncert[i] - (this->dose[i] * this->dose[i]));
            d_sq /= (f_hist - 1.0);
            d_sq = sqrt(d_sq) / this->dose[i];

            this->uncert[i] = d_sq;
            this->dose[i] = this->dose[i] / vol;
            this->dose[i] /= CLHEP::gray;
            this->dose[i] /= norm;
        }
        else
        {
            this->uncert[i] = 0.99;
        }
    }
}

void SimDose::as_3ddose(G4String filename, DetectorConstruction *phant, G4double uncert_threshold)
{
    int numVoxels[3];
    G4double topleft[3];
    G4double voxelSize[3];

    phant->getTopleft(topleft[0], topleft[1], topleft[2]);
    phant->getNumVoxels(numVoxels[0], numVoxels[1], numVoxels[2]);
    phant->getVoxelSize(voxelSize[0], voxelSize[1], voxelSize[2]);
    std::ofstream dose_file(filename);
    dose_file << numVoxels[0] << " " << numVoxels[1] << " " << numVoxels[2] << G4endl;

    for (G4int axis = 0; axis < 3; axis++)
    {
        for (G4int j = 0; j < numVoxels[axis] + 1; j++)
        {
            if (j != 0)
                dose_file << " ";
            dose_file << (topleft[axis] + j * voxelSize[axis]) / cm;
        }
        dose_file << G4endl;
    }

    dose_file << std::scientific;
    for (size_t i = 0; i < this->dose.size(); i++)
    {
        if (i != 0)
            dose_file << " ";
        if (this->dose[i] == 0 || this->uncert[i] > uncert_threshold)
        {
            dose_file << 0;
        }
        else
        {
            dose_file << this->dose[i];
        }
    }
    dose_file << std::endl;

    for (size_t i = 0; i < this->dose.size(); i++)
    {
        if (i != 0)
            dose_file << " ";
        if (this->dose[i] > 0.0)
        {
            dose_file << this->uncert[i];
        }
        else
        {
            dose_file << 1.0;
        }
    }

    dose_file.close();
    G4cout << "Dose exported to: " << filename << G4endl;
}

void SimDose::as_minidos(G4String filename, DetectorConstruction *phant, G4double uncert_threshold)
{
    int numVoxels[3];
    G4double topleft[3];
    G4double voxelSize[3];

    phant->getTopleft(topleft[0], topleft[1], topleft[2]);
    phant->getNumVoxels(numVoxels[0], numVoxels[1], numVoxels[2]);
    phant->getVoxelSize(voxelSize[0], voxelSize[1], voxelSize[2]);

    FILE *dose_file = fopen(filename.c_str(), "wb");
    if (dose_file == NULL)
    {
      //std::cerr << "Could not create dose file. Check write permissions" << std::endl;
    }

    float voxel_size[3];
    float f_topleft[3];
    // Convert doubles to floats for the binary file
    voxel_size[0] = voxelSize[0] / cm;
    voxel_size[1] = voxelSize[1] / cm;
    voxel_size[2] = voxelSize[2] / cm;

    f_topleft[0] = topleft[0] / cm;
    f_topleft[1] = topleft[1] / cm;
    f_topleft[2] = topleft[2] / cm;

    int num_nonzero = 0;
    for (size_t i = 0; i < this->dose.size(); i++)
    {
        if (this->dose[i] > 0.0 && this->uncert[i] < uncert_threshold)
        {
            num_nonzero += 1;
        }
    }

    fwrite(numVoxels, sizeof(int), 3, dose_file);
    fwrite(voxel_size, sizeof(float), 3, dose_file);
    fwrite(f_topleft, sizeof(float), 3, dose_file);
    fwrite(&num_nonzero, sizeof(int), 1, dose_file);

    int idx = 0;
    for (size_t i = 0; i < this->dose.size(); i++)
    {
        if (this->dose[i] > 0 && this->uncert[i] < uncert_threshold)
        {
            fwrite(&idx, sizeof(int), 1, dose_file);
            fwrite(&this->dose[i], sizeof(float), 1, dose_file);
        }
        idx += 1;
    }
    fclose(dose_file);
    G4cout << "Dose exported to: " << filename << G4endl;
}
