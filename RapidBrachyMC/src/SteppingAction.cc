#include "SteppingAction.hh"

SteppingAction::SteppingAction(){ }

void SteppingAction::UserSteppingAction(const G4Step *theStep)
{
  G4Material* preMat = theStep->GetPreStepPoint()->GetMaterial();
  G4Material* postMat = theStep->GetPostStepPoint()->GetMaterial();

   if (preMat != postMat) {
     G4cout << "PreStep " << theStep->GetPreStepPoint()->GetPosition()
      << " : " << preMat->GetName() << G4endl;
     G4cout << "PostStep " << theStep->GetPostStepPoint()->GetPosition();
      if(postMat) {
	 G4cout << " : " << postMat->GetName();
      }
      G4cout << G4endl;
      G4cout << "StepStatus : " << theStep->GetPostStepPoint()->GetStepStatus() << G4endl;
   }

}

