// This is an updated version of FlexiSource based on Granero et al
// Marc Morcos <marc.morcos@mail.mcgill.ca> - 2018-11-09
// Last Major edit 2019-08-09
// Works with source orientation

#include <iostream>
#include <string>

#include "FlexiNoShield.hh"
#include "SourceMaterial.hh"
#include "DetectorConstruction.hh"
#include "PositionList.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"

#include "G4Sphere.hh"
#include "G4Orb.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"

#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

#include "G4VisAttributes.hh"

#include "ControlPoint.hh"

using namespace std;

FlexiNoShield::FlexiNoShield(G4String core)
{
    pMaterial = new SourceMaterial();

    SetupDimensions();

    fCore = core;
}

FlexiNoShield::FlexiNoShield()
{
    pMaterial = new SourceMaterial();

    SetupDimensions();
    fPositionFilename = "";
    fCore = "G4_Ir";
}

FlexiNoShield::~FlexiNoShield()
{
    delete pMaterial;
}

void FlexiNoShield::SetCore(G4String core)
{
    fCore = core;
}

void FlexiNoShield::SetupDimensions()
{
  // Active core
  sourceRadius           = 0.3   * mm;
  halfSourceHeight       = 1.75  * mm;

  coreRadius = sourceRadius;
  coreHalfZ = halfSourceHeight;

  // Air Gap
  airInRad               = sourceRadius;
  airOutRad              = 0.335 * mm;
  air_halfHt             = 1.8   * mm;

  // Capsule - Main
  capsuleInnerRadius     = airOutRad;
  capsuleOuterRadius     = 0.425 * mm;
  capsuleHalfHeight      = air_halfHt;

  // Capsule - Right
  capsuleHalfExtraHeight = 0.325 * mm;

  // Capsule - Left
  capsuleTipMinConRadius = 0.25 * mm;
  capsuleTipHalfHeight   = 0.225 * mm; // 0.45mm in granero for diameter and 0.4 in brachy advanced example...

  // Cable
  cableRadius            = capsuleTipMinConRadius * mm;
  halfCableHeight        = 2.5 * mm;

  // Math
  startPhi  = 0.; // OK
  spanPhi   = 2. * CLHEP::pi; // OK
  //spanTheta = CLHEP::pi / 2.; //.... let's see if we need pi/2

  sourceOffsetY = 0 ;//*mm; // Just to make sure...
}

G4ThreeVector FlexiNoShield::getSourceOffset()
{
    return G4ThreeVector(0.0, -sourceOffsetY, 0.0);
}

void FlexiNoShield::createSource()
{
  // FlexiSource Model Adapted from:
  // Granero, D., et al. Medical physics 33.12 (2006): 4578-4582
  // "A dosimetric study on the high dose rate Flexisource."
  // Also adapted from GEANT4 Advanced Brachy Tutorial


  // Materials
  G4Material *steelMat = pMaterial->GetMat("Steel"); //Stainless Steel-304
  G4Material *airMat = pMaterial->GetMat("G4_AIR"); //Nist Air
  G4Material *coreMaterial = pMaterial->GetMat(fCore);

  ///////

  // Source Core
  ///////////////
  // Geometry
  G4Tubs *source = new G4Tubs("source", 0.0 * mm, sourceRadius,
                                  halfSourceHeight, startPhi, spanPhi);
  // Logical
  source_log = new G4LogicalVolume(source, coreMaterial, "source_log");

  // Visualization
  G4VisAttributes *source_att = new G4VisAttributes();
  source_att->SetColor(1, 0, 0); // Red
  source_att->SetVisibility(true);
  source_att->SetForceSolid(true);
  source_log->SetVisAttributes(source_att);

  // Air Gap
  ////////////////
  // Geometry
  // Capsule starts from core radius.
  G4Tubs *airGap = new G4Tubs("airGap", airInRad,
                                    airOutRad, air_halfHt,
                                    startPhi, spanPhi);
  // Logical
  airGap_log = new G4LogicalVolume(airGap, airMat, "airGap_log");

  // Visualization
  // ** No Visualization Needed for Air Gap

  // Capsule (3 Parts Combined - Main, Left/Cable End, Right/Closed End)
  //////////////////////////////////////////////////////////////////////
  // Geometry
  // center/main capsule
  G4Tubs *capsuleMain = new G4Tubs("capsuleMain", capsuleInnerRadius,
                                    capsuleOuterRadius, capsuleHalfHeight,
                                    startPhi, spanPhi);
  // right/closed end of capsule
  G4Tubs *capsuleRight = new G4Tubs("capsuleRight", 0.0 * mm,
                                         capsuleOuterRadius, capsuleHalfExtraHeight,
                                         startPhi, spanPhi);
  // left/cable end of capsule
  G4Cons *capsuleLeft = new G4Cons("capsuleLeft", 0.0 * mm, capsuleTipMinConRadius,
                                      0.0 * mm, capsuleOuterRadius, capsuleTipHalfHeight,
                                      startPhi, spanPhi);

  // Logical
  // Join Main & Right Tip --> New Coordinate System is inhereted from capsuleMain (so origin is at center of capsuleMain)
  G4VSolid *capsuleMainANDRight = new G4UnionSolid("capsuleMainANDRight", capsuleMain, capsuleRight, 0,
                                            G4ThreeVector(0.0, 0.0, (capsuleHalfHeight + capsuleHalfExtraHeight)));

  // Join Main+Right with Left Tip --> New Coordinate System is inhereted from Main+Right (so origin is at center of capsuleMain)
  G4VSolid *capsule = new G4UnionSolid("capsule", capsuleMainANDRight, capsuleLeft, 0,
                                            G4ThreeVector(0.0, 0.0, -(capsuleHalfHeight + capsuleTipHalfHeight)));

  // Visualization
  capsule_log = new G4LogicalVolume(capsule, steelMat, "capsule_log");
  G4VisAttributes *capsule_att = new G4VisAttributes();
  capsule_att->SetColor(0, 0, 1); //Blue
  capsule_att->SetVisibility(true);
  capsule_att->SetForceSolid(false);
  capsule_log->SetVisAttributes(capsule_att);


  // Cable
  ////////////////
  // Geometry
  G4Tubs *cable_vol = new G4Tubs("cable_vol", 0.0 * mm, cableRadius, halfCableHeight, startPhi, spanPhi);
  // Logical
  cable_log = new G4LogicalVolume(cable_vol, steelMat, "cable_log");

  // Visualization
  cable_log = new G4LogicalVolume(cable_vol, steelMat, "cable_log");
  G4VisAttributes *cable_att = new G4VisAttributes();
  cable_att->SetColor(0.2, 0, 0.8); //Blue-Purple
  cable_att->SetVisibility(true);
  cable_att->SetForceSolid(true);
  cable_log->SetVisAttributes(cable_att);

}

void FlexiNoShield::placeSource(ControlPoint cpt, G4VPhysicalVolume *mother)
{

    for (size_t i = 0; i < cpt.dwellPositions.size(); i++)
    {
        // Dwell position
        G4ThreeVector shift = cpt.dwellPositions[i].pos;

        // Source Offset
        // Used when IMBT is part of source model and there's a shift to offset source
        //G4ThreeVector sourceOffset = this->getSourceOffset();

        // Shield Angle
        // Similar to Source Offset, only used when shield is part of source model
        // G4double angle = cpt.dwellPositions[i].angle;

        // Cable Offset
        G4ThreeVector cableOffset = G4ThreeVector(0.0, 0.0,
                        -(halfCableHeight + 2. * capsuleTipHalfHeight + capsuleHalfHeight));


        // Direction Vector of Dwell position
        G4ThreeVector Zunit = G4ThreeVector(0.0,0.0,1.0); //Source Is Originally defined pointing in +Z
        G4ThreeVector V = G4ThreeVector(cpt.dwellPositions[i].rot[0],
                                        cpt.dwellPositions[i].rot[1],
                                        cpt.dwellPositions[i].rot[2]).unit();

        // Angle Between V and +Z Axis
        G4double Vangle = Zunit.angle(V);

        // Axis for which rotation (Vangle) will be used to define rotation matrix
        G4ThreeVector Vcross = G4ThreeVector(0,0,0);

        // Check if V Parallel to Z
        if (V.isParallel(Zunit, 1E-4)) // IF parallel to Z-axis
        {
          G4cout << "*** HowParallel: " << V.howParallel(Zunit) << G4endl;

          if (fabs(Vangle - 3.14159) < 1E-4) // If Angle = Pi then Vcross = -X
          {
            Vcross = G4ThreeVector(-1.0,0.0,0.0); // -X
            G4cout << "WARNING: Source Orientation Parallel with NEGATIVE Z axis" << G4endl;
          }
          else // ASSUMPTION! If Angle == 0
          {
            Vcross = G4ThreeVector(1.0,0.0,0.0); // +X
            G4cout << "WARNING: Source Orientation Parallel with POSITIVE Z axis" << G4endl;
          }
        }
        else // IF NOT parallel to Z-axis
        {
          Vcross = V.cross(Zunit).unit();
        }

        // DEFINE ROTATION MATRIX To line up source with direction vector V
        G4RotationMatrix * Vrot = new G4RotationMatrix();
        Vrot->rotate(Vangle, Vcross);
        Vrot->invert();


        // Placing Components ////
        G4cout << "Placing source at: " << shift  << G4endl;
        G4cout << "Source Orientation Vector:" << V << G4endl;

        // *** G4PVPplacement using G4Transform3D to apply translate/rotation in mother frame of reference
        // Source
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift), "source",
                                                        this->source_log, mother, false, i));
        // Air Gap
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift), "airGap",
                                                        this->airGap_log, mother, false, i));
        // Capsule
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift), "capsule",
                                                        this->capsule_log, mother, false, i));
        // Cable
        // Remeber: Cable is offset from the source-capsule construction
        cableOffset.transform(*Vrot); // Transform cable shift in rotated frame of reference
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift+cableOffset), "cable",
                                                        this->cable_log, mother, false, i));
    }

    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}

void FlexiNoShield::cleanSource()
{
    for (size_t i = 0; i < this->placedObjects.size(); i++)
    {
        delete this->placedObjects[i];
    }
    this->placedObjects.clear();
    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}
