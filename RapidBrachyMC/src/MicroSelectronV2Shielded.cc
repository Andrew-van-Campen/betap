// !!!!
// NEEDS TO BE UPDATED TO WORK WITH SOURCE ORIENTATION (See FLexiNoShield or MicroSelectronV2)
// !!!

#include <iostream>
#include <string>

#include "MicroSelectronV2Shielded.hh"
#include "SourceMaterial.hh"
#include "DetectorConstruction.hh"
#include "PositionList.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"

#include "G4Sphere.hh"
#include "G4Orb.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"

#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

#include "G4VisAttributes.hh"

#include "ControlPoint.hh"

using namespace std;

MicroSelectronV2Shielded::MicroSelectronV2Shielded(G4String core, G4double shieldRad)
{
    pMaterial = new SourceMaterial();

    shieldRadius = shieldRad;

    SetupDimensions();

    fCore = core;
}

MicroSelectronV2Shielded::MicroSelectronV2Shielded()
{
    pMaterial = new SourceMaterial();

    shieldRadius = 2.0 * mm;

    SetupDimensions();
    fPositionFilename = "";
    fCore = "G4_Ir";
}

MicroSelectronV2Shielded::~MicroSelectronV2Shielded()
{
    delete pMaterial;
}

void MicroSelectronV2Shielded::SetCore(G4String core)
{
    fCore = core;
}

void MicroSelectronV2Shielded::SetupDimensions()
{
    // Active core
    sourceRadius = 0.325 * mm;
    halfSourceHeight = 1.74 * mm;

    // Tip of core
    sourceTipMinConRadius = 0.265 * mm;
    sourceTipMaxConRadius = 0.325 * mm;
    sourceTipHalfHeight = 0.03 * mm;

    // Active core parameters
    coreRadius = sourceRadius;
    coreHalfZ = halfSourceHeight + 2 * sourceTipHalfHeight;

    capsuleInnerRadius = sourceRadius;
    capsuleOuterRadius = 0.45 * mm;
    capsuleHalfHeight = 1.8 * mm;

    // Extra capsule cylinder between core and cable
    capsuleHalfExtraHeight = 0.275 * mm;

    // Conical capsule tip
    capsuleTipMinConRadius = 0.35 * mm;
    capsuleTipMaxConRadius = 0.45 * mm;
    capsuleTipHalfHeight = 0.075 * mm;

    cableRadius = 0.35 * mm;
    halfCableHeight = 1.0 * mm;

    //shieldRadius = 0.6525*mm;
    //shieldRadius = 0.8 * mm;
    //shieldRadius = 2.0 * mm;

    halfShieldHeight = 1.8 * mm;
    halfExtraShieldHeight = 0.35 * mm;

    rectangleWidth = 0.5 * capsuleOuterRadius;
    rectangleHeight = halfShieldHeight;
    rectangleLength = shieldRadius;

    startPhi = 0.;
    spanPhi = 2. * CLHEP::pi;
    spanTheta = CLHEP::pi / 2.;

    sourceOffsetY = (shieldRadius - capsuleOuterRadius - 0.05 * mm);
}

G4ThreeVector MicroSelectronV2Shielded::getSourceOffset()
{
    return G4ThreeVector(0.0, -sourceOffsetY, 0.0);
}

void MicroSelectronV2Shielded::createSource()
{
    G4Material *steel = pMaterial->GetMat("Steel");
    G4Material *Pt = pMaterial->GetMat("G4_Pt");
    G4Material *coreMaterial = pMaterial->GetMat(fCore);

    // Source definition

    G4Tubs *sourceCore = new G4Tubs("sourceCore", 0.0 * mm, sourceRadius,
                                    halfSourceHeight, startPhi, spanPhi);

    G4Cons *sourceFrontTip = new G4Cons("sourceFrontTip", 0.0 * mm, sourceTipMaxConRadius,
                                        0.0 * mm, sourceTipMinConRadius,
                                        sourceTipHalfHeight, startPhi, spanPhi);

    G4Cons *sourceBackTip = new G4Cons("sourceBackTip", 0.0 * mm, sourceTipMinConRadius,
                                       0.0 * mm, sourceTipMaxConRadius,
                                       sourceTipHalfHeight, startPhi, spanPhi);

    G4VSolid *source1 = new G4UnionSolid("source1", sourceCore, sourceFrontTip, 0, G4ThreeVector(0.0, 0.0, halfSourceHeight + sourceTipHalfHeight));
    G4VSolid *source = new G4UnionSolid("source", source1, sourceBackTip, 0, G4ThreeVector(0.0, 0.0, -(halfSourceHeight + sourceTipHalfHeight)));

    source_log = new G4LogicalVolume(source, coreMaterial, "source_log");
    G4VisAttributes *source_att = new G4VisAttributes();
    source_att->SetColor(1, 0, 0);
    source_att->SetVisibility(true);
    source_att->SetForceSolid(true);
    source_log->SetVisAttributes(source_att);

    // Capsule starts from core radius.
    G4Tubs *capsuleSolid = new G4Tubs("capsuleSolid", capsuleInnerRadius,
                                      capsuleOuterRadius, capsuleHalfHeight,
                                      startPhi, spanPhi);

    G4Tubs *capsuleExtraSolid = new G4Tubs("capsuleExtraSolid", 0.0 * mm,
                                           capsuleOuterRadius, capsuleHalfExtraHeight,
                                           startPhi, spanPhi);

    // Conical capsule tip near the cable
    G4Cons *capsuleBackTip = new G4Cons("capsuleBackTip", 0.0 * mm, capsuleTipMinConRadius,
                                        0.0 * mm, capsuleTipMaxConRadius, capsuleTipHalfHeight,
                                        startPhi, spanPhi);
    // Spherical capsule tip
    G4Sphere *capsuleFrontTip = new G4Sphere("capsuleFrontTip", 0.0 * mm, capsuleOuterRadius,
                                             startPhi, spanPhi, startPhi, spanTheta);

    // Join capsule cylinder with extra cylinder bit
    G4VSolid *capsuleHalf1 = new G4UnionSolid("capsuleHalf1", capsuleSolid, capsuleExtraSolid, 0,
                                              G4ThreeVector(0.0, 0.0, -(capsuleHalfHeight + capsuleHalfExtraHeight)));

    // Add in the conical back capsule tip
    G4VSolid *capsuleHalf2 = new G4UnionSolid("capsuleHalf2", capsuleHalf1, capsuleBackTip, 0,
                                              G4ThreeVector(0.0, 0.0, -(capsuleHalfHeight + 2 * capsuleHalfExtraHeight + capsuleTipHalfHeight)));

    // Add the front spherical capsule tip
    G4VSolid *capsule = new G4UnionSolid("capsule", capsuleHalf2, capsuleFrontTip, 0,
                                         G4ThreeVector(0.0, 0.0, capsuleHalfHeight));

    capsule_log = new G4LogicalVolume(capsule, steel, "capsule_log");
    G4VisAttributes *capsule_att = new G4VisAttributes();
    capsule_att->SetColor(0.5, 0.5, 0.5);
    capsule_att->SetVisibility(true);
    capsule_att->SetForceSolid(false);
    capsule_log->SetVisAttributes(capsule_att);

    // Steel Cable
    G4Tubs *cable_vol = new G4Tubs("cable_vol", 0.0 * mm, cableRadius, halfCableHeight, startPhi, spanPhi);
    cable_log = new G4LogicalVolume(cable_vol, steel, "cable_log");
    G4VisAttributes *cable_att = new G4VisAttributes();
    cable_att->SetColor(0, 0, 1);
    cable_att->SetVisibility(true);
    cable_att->SetForceSolid(true);
    cable_log->SetVisAttributes(cable_att);

    ///////////////////////////////////////////////

    G4double shieldOffsetY = (shieldRadius - capsuleOuterRadius - 0.05 * mm);

    // Shield window solid
    G4Tubs *shieldWindow = new G4Tubs("shieldWindow", 0.0 * mm,
                                      shieldRadius, halfShieldHeight,
                                      startPhi, spanPhi);

    G4Box *removalRect = new G4Box("shieldRect", rectangleLength, rectangleWidth, rectangleHeight);

    G4VSolid *windowInterm = new G4SubtractionSolid("windowInterm", shieldWindow, removalRect, 0,
                                                    G4ThreeVector(0.0, -(shieldRadius - 0.5 * capsuleOuterRadius), 0.0));

    // Short cylinder covering the capsule between active source and cable.
    G4Tubs *shieldExtra = new G4Tubs("shieldExtra", 0.0 * mm,
                                     shieldRadius, halfExtraShieldHeight,
                                     startPhi, spanPhi);

    // Tip of the shield cylinder
    G4Sphere *shieldTip = new G4Sphere("shieldTip", 0.0 * mm,
                                       shieldRadius, startPhi, spanPhi,
                                       startPhi, spanTheta);

    // Subtraction solid to dig a hole the size of the capsule tip into the shield tip
    // so that they fit inside eachother
    G4Sphere *capsuleTipRemoval = new G4Sphere("capsuleTipRemoval", 0.0 * mm,
                                               capsuleOuterRadius, startPhi, spanPhi,
                                               startPhi, spanTheta);

    // Subtraction solid to dig a cylindrical hole the size of the capsule
    // so that they fit inside eachother.
    G4Tubs *capsuleRemoval = new G4Tubs("capsuleRemoval", 0, capsuleOuterRadius,
                                        halfShieldHeight + halfExtraShieldHeight,
                                        startPhi, spanPhi);

    // Join shield window and extra cylinder part
    G4VSolid *ShieldWS = new G4UnionSolid("ShieldWS", windowInterm, shieldExtra, 0,
                                          G4ThreeVector(0.0, 0.0, -(halfShieldHeight + halfExtraShieldHeight)));

    // Add the spherical tip
    G4VSolid *ShieldIntermediate = new G4UnionSolid("ShieldInterm", ShieldWS, shieldTip, 0,
                                                    G4ThreeVector(0.0, 0.0, halfShieldHeight));

    // Subtract the capsule cylinder from the solid shield
    ShieldIntermediate = new G4SubtractionSolid("ShieldInterm", ShieldIntermediate, capsuleRemoval, 0,
                                                G4ThreeVector(0.0, -sourceOffsetY, -halfExtraShieldHeight));

    // Subtract the capsule tip
    G4VSolid *Shield = new G4SubtractionSolid("Shield", ShieldIntermediate, capsuleTipRemoval, 0,
                                              G4ThreeVector(0.0, -sourceOffsetY, halfShieldHeight));

    shield_log = new G4LogicalVolume(Shield, Pt, "Shield_log");
    G4VisAttributes *Shield_att = new G4VisAttributes();
    Shield_att->SetColor(1, 0, 1);
    Shield_att->SetVisibility(true);
    Shield_att->SetForceSolid(false);
    shield_log->SetVisAttributes(Shield_att);
}

void MicroSelectronV2Shielded::placeSource(ControlPoint cpt, G4VPhysicalVolume *mother)
{

    for (size_t i = 0; i < cpt.dwellPositions.size(); i++)
    {
        G4ThreeVector sourceOffset = this->getSourceOffset();
        G4ThreeVector shift = cpt.dwellPositions[i].pos;
        G4double angle = cpt.dwellPositions[i].angle;
        //G4cout << "Placing source at " << shift << " with shield angle " << angle << G4endl;

        G4RotationMatrix *shieldRotation = new G4RotationMatrix();
        G4RotationMatrix *invertRotation = new G4RotationMatrix();
        shieldRotation->rotateZ(angle);
        sourceOffset.transform(*shieldRotation);
        *invertRotation = shieldRotation->invert();
        G4ThreeVector cableOffset = G4ThreeVector(0, 0, -halfSourceHeight - 2 * sourceTipHalfHeight - 2. * capsuleHalfExtraHeight - 2 * capsuleTipHalfHeight - halfCableHeight) + sourceOffset;

        this->placedObjects.push_back(new G4PVPlacement(0, shift + sourceOffset, "source",
                                                        this->source_log, mother, false, i));
        this->placedObjects.push_back(new G4PVPlacement(0, shift + sourceOffset, "capsule",
                                                        this->capsule_log, mother, false, i));

        this->placedObjects.push_back(new G4PVPlacement(0, shift + cableOffset, "cable",
                                                        this->cable_log, mother, false, i));

        this->placedObjects.push_back(new G4PVPlacement(invertRotation, shift, "Shield",
                                                        this->shield_log, mother, false, i));
    }

    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}

void MicroSelectronV2Shielded::cleanSource()
{
    for (size_t i = 0; i < this->placedObjects.size(); i++)
    {
        delete this->placedObjects[i];
    }
    this->placedObjects.clear();
    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}
