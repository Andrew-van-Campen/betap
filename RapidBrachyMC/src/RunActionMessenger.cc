#include "RunActionMessenger.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"

RunActionMessenger::RunActionMessenger(RunAction *runAct) : runAction(runAct)
{
    fDoseFormatcmd = new G4UIcmdWithAString("/dose/format", this);
    fDoseFormatcmd->SetGuidance("Select dose output format");
    fDoseFormatcmd->SetParameterName("tFormat", false);
    fDoseFormatcmd->AvailableForStates(G4State_PreInit, G4State_Idle);
    fDoseFormatcmd->SetToBeBroadcasted(false);

    treatmentPlanCmd = new G4UIcmdWithAString("/sim/plan", this);
    treatmentPlanCmd->SetGuidance("Provide the plan filename");
    treatmentPlanCmd->SetParameterName("choice", true);
    treatmentPlanCmd->SetDefaultValue("");
    treatmentPlanCmd->AvailableForStates(G4State_PreInit);

    simCmd = new G4UIcmdWithAnInteger("/sim/beamOn", this);
    simCmd->SetGuidance("Start the simulation with N histories");
    simCmd->SetParameterName("nhist", true);
    simCmd->SetDefaultValue(1000);
    simCmd->AvailableForStates(G4State_Idle);
}

RunActionMessenger::~RunActionMessenger()
{
    delete runActionDir;
    delete fDoseFormatcmd;
}

void RunActionMessenger::SetNewValue(G4UIcommand *command, G4String newValue)
{
    if (command == fDoseFormatcmd)
    {
        runAction->setDoseFormat(newValue);
    }
    else if(command == treatmentPlanCmd)
    {
        runAction->loadTreatmentPlan(newValue);
    }
    else if (command == simCmd)
    {
        runAction->startSimulation(simCmd->GetNewIntValue(newValue));
    }

}
