// BRACHYSOURCE //
#include "ApplicatorWorldMessenger.hh"
#include "ApplicatorWorldConstruction.hh"

// GEANT4 //
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithoutParameter.hh"

ApplicatorWorldMessenger::ApplicatorWorldMessenger(
        ApplicatorWorldConstruction* applicator_world)
    : G4UImessenger(), _applicator_world(applicator_world)
{
    _applicator_world_directory = new G4UIdirectory("/applicator/");
    _applicator_world_directory->SetGuidance(
            "Applicator geometry configuration options.");

    _material_command = new G4UIcmdWithAString("/applicator/material", this);
    _material_command->SetParameterName("material", true);
    _material_command->AvailableForStates(G4State_PreInit);

    _density_command = new G4UIcmdWithADouble("/applicator/density", this);
    _density_command->SetParameterName("density", true);
    _density_command->AvailableForStates(G4State_PreInit);

    _x_position_command = new G4UIcmdWithADoubleAndUnit("/applicator/xPosition", this);
    _x_position_command->SetGuidance("Set applicator x position.");
    _x_position_command->SetParameterName("position", true);
    _x_position_command->AvailableForStates(G4State_PreInit);

    _y_position_command = new G4UIcmdWithADoubleAndUnit("/applicator/yPosition", this);
    _y_position_command->SetGuidance("Set applicator y position.");
    _y_position_command->SetParameterName("position", true);
    _y_position_command->AvailableForStates(G4State_PreInit);

    _z_position_command = new G4UIcmdWithADoubleAndUnit("/applicator/zPosition", this);
    _z_position_command->SetGuidance("Set applicator z position.");
    _z_position_command->SetParameterName("position", true);
    _z_position_command->AvailableForStates(G4State_PreInit);

    _x_rotation_command = new G4UIcmdWithADoubleAndUnit("/applicator/xRotation", this);
    _x_rotation_command->SetGuidance("Set applicator x rotation.");
    _x_rotation_command->SetParameterName("rotation", true);
    _x_rotation_command->AvailableForStates(G4State_PreInit);

    _y_rotation_command = new G4UIcmdWithADoubleAndUnit("/applicator/yRotation", this);
    _y_rotation_command->SetGuidance("Set applicator y rotation.");
    _y_rotation_command->SetParameterName("rotation", true);
    _y_rotation_command->AvailableForStates(G4State_PreInit);

    _z_rotation_command = new G4UIcmdWithADoubleAndUnit("/applicator/zRotation", this);
    _z_rotation_command->SetGuidance("Set applicator z rotation.");
    _z_rotation_command->SetParameterName("rotation", true);
    _z_rotation_command->AvailableForStates(G4State_PreInit);

    _vertex_command = new G4UIcmdWith3VectorAndUnit("/applicator/vertex", this);
    _vertex_command->SetGuidance("Add a vertex to the tessellated solid.");
    _vertex_command->AvailableForStates(G4State_PreInit);

    _face_command = new G4UIcmdWith3Vector("/applicator/face", this);
    _face_command->SetGuidance("Add a face to the tessellated solid.");
    _face_command->AvailableForStates(G4State_PreInit);

    _done_command = new G4UIcmdWithoutParameter("/applicator/done", this);
    _done_command->SetGuidance("Finished adding this volume/mesh.");
    _done_command->AvailableForStates(G4State_PreInit);
}


ApplicatorWorldMessenger::~ApplicatorWorldMessenger()
{
    delete _applicator_world_directory;
    delete _material_command;
    delete _density_command;
    delete _x_position_command;
    delete _x_position_command;
    delete _z_position_command;
    delete _x_rotation_command;
    delete _y_rotation_command;
    delete _z_rotation_command;
    delete _vertex_command;
    delete _face_command;
    delete _done_command;
}


void ApplicatorWorldMessenger::SetNewValue(G4UIcommand* command, G4String value)
{
    if (command == _material_command)
    {
        _applicator_world->SetMaterial(value);
    }
        

    else if (command == _density_command)
        _applicator_world->SetDensity(
                _density_command->GetNewDoubleValue(value));

    else if (command == _x_position_command)
        _applicator_world->SetXPosition(
                _x_position_command->GetNewDoubleValue(value));

    else if (command == _y_position_command)
        _applicator_world->SetYPosition(
                _y_position_command->GetNewDoubleValue(value));

    else if (command == _z_position_command)
        _applicator_world->SetZPosition(
                _z_position_command->GetNewDoubleValue(value));

    else if (command == _x_rotation_command)
        _applicator_world->SetXRotation(
                _x_rotation_command->GetNewDoubleValue(value));

    else if (command == _y_rotation_command)
        _applicator_world->SetZRotation(
                _y_rotation_command->GetNewDoubleValue(value));

    else if (command == _z_rotation_command)
        _applicator_world->SetZRotation(
                _z_rotation_command->GetNewDoubleValue(value));

    else if (command == _vertex_command)
        _applicator_world->AddVertex(
                _vertex_command->GetNew3VectorValue(value));

    else if (command == _face_command)
        _applicator_world->AddFace(
                _face_command->GetNew3VectorValue(value));

    else if (command == _done_command)
        _applicator_world->DoneAddingVolume();
}

