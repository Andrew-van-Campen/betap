#include <fstream>
#include <sstream>
#include <iostream>
#include <string>

#include "RunAction.hh"
#include "G4RunManager.hh"
#include "DetectorConstruction.hh"
#include "ParallelWorldConstructionScoring.hh"
#include "SourceWorldConstruction.hh"
#include "PrimaryGeneratorAction.hh"
#include "G4UImanager.hh"
#include "G4VVisManager.hh"

#include "G4UnitsTable.hh"
#include "Run.hh"
#include "G4Run.hh"

#include "G4THitsMap.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "Phantom.hh"
#include "TreatmentPlan.hh"

#include "string_utils.hh"

G4int RunAction::numEvents = 0;
G4int RunAction::runIDcounter = 0;

RunAction::RunAction(G4String input_filename, DetectorConstruction *phant, ParallelWorldConstructionScoring *scoring, SourceWorldConstruction *source)
    : inputFilename(input_filename), egsphant(phant), scoringPar(scoring), sourceWorld(source)
{

    runActionMessenger = new RunActionMessenger(this);
    outputMode = "3ddose";
    uncert_threshold = 0.9;

    if (!input_filename.empty())
    {
        progressFilename = remove_extension(input_filename) + ".progress";
    }
}

RunAction::~RunAction()
{
    if (IsMaster())
    {
        //delete treatmentPlan;
    }
    //delete lRun;

    for (auto collection : doseCollections)
    {
        delete collection;
    }
}

void RunAction::loadTreatmentPlan(G4String fileName)
{
    if (IsMaster())
    {
        this->planFilename = fileName;
        treatmentPlan = new TreatmentPlan(fileName);
        G4cout << "Treatment plan successfully loaded." << G4endl;
    }
}

void RunAction::startSimulation(G4int nhist)
{
    // Dose.
    doseCollections.push_back(new SimDose(egsphant->getTotalVoxels()));
    
    // Dose to water.
    doseCollections.push_back(new SimDose(egsphant->getTotalVoxels()));


    if (IsMaster())
    {
        this->nGammas = 0;
        this->nHistories = nhist;
        G4int histsDone = 0;
        if (nhist > 0)
        {
            for (size_t i = 0; i < treatmentPlan->controlPoints.size(); i++)
            {
                this->sourceWorld->placeControlPoint(treatmentPlan->controlPoints[i]);
                G4int nhistForRun = treatmentPlan->controlPoints[i].weight * this->nHistories;
                if (nhistForRun < 1)
                    nhistForRun = 1;
                G4RunManager::GetRunManager()->BeamOn(nhistForRun);
                histsDone += nhistForRun;
                this->updateProgress(histsDone, nhist);
            }
            G4double norm = this->calculateNorm();
            
            for (auto collection : doseCollections)
            {
                collection->finalize_tally(nGammas, norm, egsphant);
            }

            this->outputDose();
            std::remove(progressFilename.c_str());
        }
        else
        {
            // If nhist == 0, user probably just wants to visualise things.
            // Place the first control point for visualisation purposes but don't do anything else.
            if (treatmentPlan->controlPoints.size() > 0)
            {
                this->sourceWorld->placeControlPoint(treatmentPlan->controlPoints[0]);
            }
            else
            {
	      //G4cout << "No control points to visualise" << G4endl;
            }
        }
    }
}

G4Run *RunAction::GenerateRun()
{
    // Accumulate energy depositions in a thread-local SimDose.
    // The global Run instance needs to know how many control points
    // will be simulated to know when to merge all the local runs into
    // itself.
    if (IsMaster())
    {
        lRun = new Run(this->doseCollections, treatmentPlan->controlPoints.size());
    }
    else
    {
        lRun = new Run(this->doseCollections, -1);
    }

    lRun->SetRunID(runIDcounter);

    return lRun;
}

void RunAction::setDoseFormat(G4String format)
{
    outputMode = format;
}

void RunAction::BeginOfRunAction(const G4Run *aRun)
{
    numEvents = aRun->GetNumberOfEventToBeProcessed();
}

void RunAction::EndOfRunAction(const G4Run *aRun)
{
    if (IsMaster())
    {
        Run *theRun = (Run *)aRun;
        nGammas += theRun->fNGammasCreated;
        size_t runNumber = runIDcounter + 1;
        if (runNumber == treatmentPlan->controlPoints.size())
        {
            for (int i=0; i<doseCollections.size(); i++)
            {
                doseCollections[i]->merge_with(theRun->doseCollections[i]);
            }
        }
    }
    runIDcounter += 1;
}

void RunAction::updateProgress(G4int histsDone, G4int totalHist)
{
    std::ofstream progressFile(this->progressFilename);
    progressFile << histsDone << "/" << totalHist << G4endl;
    progressFile.close();
    G4cout << histsDone << " / " << totalHist << " events done" << G4endl;
}

void RunAction::outputDose()
{
    if (outputMode == "3ddose")
    {
        G4String filename = remove_extension(inputFilename) + ".3ddose";
        doseCollections[0]->as_3ddose(filename, egsphant, uncert_threshold);

        filename = remove_extension(inputFilename) + "DoseToWaterInMedium.3ddose";
        doseCollections[1]->as_3ddose(filename, egsphant, uncert_threshold);

    }
    else if (outputMode == "minidos")
    {
        G4String filename = remove_extension(inputFilename) + ".minidos";
        doseCollections[0]->as_minidos(filename, egsphant, uncert_threshold);

        filename = remove_extension(inputFilename) + "DoseToWaterInMedium.minidos";
        doseCollections[1]->as_minidos(filename, egsphant, uncert_threshold);
    }
}

G4double RunAction::calculateNorm()
{
    G4double akPerHistory;
    G4double refAk;
    G4double totalTime;
    G4double H;
     
    akPerHistory = scoringPar->getAkPerHistory();
    refAk = scoringPar->getRefAk();
    totalTime = scoringPar->getTotalTime();
    H = scoringPar->getH();
    //G4cout <<" Get akPerHistory: " << akPerHistory<<" refAk  " <<refAk<<" totTime: "<<totalTime<<" H "<<H << G4endl;
    G4double normalization = akPerHistory * H/ (refAk * totalTime/3600);

    return normalization;
}
