#include "PhysicsList.hh"
#include "globals.hh"

#include "G4EmStandardPhysics_option4.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "G4EmLivermorePhysics.hh"
#include "G4EmPenelopePhysics.hh"

#include "G4DecayPhysics.hh"
#include "G4RadioactiveDecayPhysics.hh"
#include "G4VPhysicsConstructor.hh"
#include "G4SystemOfUnits.hh"

#include "G4ParallelWorldProcess.hh"
#include "G4ProcessManager.hh"
#include "G4ParticleDefinition.hh"
#include "ParallelWorldConstructionScoring.hh"

#include "G4ParallelWorldPhysics.hh"
#include "G4Version.hh"


PhysicsList::PhysicsList(ParallelWorldConstructionScoring *scoring) : G4VModularPhysicsList()
{
    this->scoringWorld = scoring;
    // EM physics: many alternatives
    //emPhysicsList = new G4EmLivermorePhysics();

    emPhysicsList = new G4EmPenelopePhysics();
    RegisterPhysics(emPhysicsList);

    decPhysicsList = new G4DecayPhysics();
    radDecayPhysicsList = new G4RadioactiveDecayPhysics();

    RegisterPhysics(decPhysicsList);
    RegisterPhysics(radDecayPhysicsList);

    // ORDER MATTERS (Source -> Applicator -> Patient/Scoring)
    RegisterPhysics(new G4ParallelWorldPhysics("SourceWorld", true));
    RegisterPhysics(new G4ParallelWorldPhysics("ApplicatorWorld", true));
    RegisterPhysics(new G4ParallelWorldPhysics("ScoringWorld", false));
}

PhysicsList::~PhysicsList()
{
}

void PhysicsList::ConstructParticle()
{
    decPhysicsList->ConstructParticle();
}

void PhysicsList::ConstructProcess()
{
    G4VModularPhysicsList::ConstructProcess();
}

void PhysicsList::SetCuts()
{
    SetCutsWithDefault();
    return;

    // Definition of  threshold of production
    // of secondary particles
    // This is defined in range.
    defaultCutValue = 0.1 * mm;

    G4double cutForElectron = 0.5 * mm;
    if (this->scoringWorld->getTrackLengthStatus())
    {
        cutForElectron = 1000.0 * mm;
    }

    SetCutValue(defaultCutValue, "gamma");
    SetCutValue(cutForElectron, "e-");
    SetCutValue(cutForElectron, "e+");

    // By default the low energy limit to produce
    // secondary particles is 990 eV.
    // This value is correct when using the EM Standard Physics.
    // When using the Low Energy Livermore this value can be
    // changed to 250 eV corresponding to the limit
    // of validity of the physics models.
    // Comment out following three lines if the
    // Standard electromagnetic Package is adopted.
    // G4double lowLimit = 200. * keV;
    // G4double lowLimit = 250 * eV;
    // G4double highLimit = 100. * GeV;

    //G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(lowLimit,
    //                                                                highLimit);

    if (verboseLevel > 0)
        DumpCutValuesTable();
}
