// This is an updated version of CustomSourceShielded
// Gabriel Famulari <gabriel.famulari@mail.mcgill.ca> 
// Last major edit 2019-09-09
// Proper source dimensions and air gap
// Needs to be updated for proper source orientation!!

#include <iostream>
#include <string>

#include "CustomSourceShielded.hh"
#include "SourceMaterial.hh"
#include "DetectorConstruction.hh"
#include "PositionList.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"

#include "G4Sphere.hh"
#include "G4Orb.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Ellipsoid.hh"

#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

#include "G4VisAttributes.hh"

#include "ControlPoint.hh"

using namespace std;

CustomSourceShielded::CustomSourceShielded(G4String core, G4double shieldRad) {
    pMaterial = new SourceMaterial();

    shieldRadius = shieldRad;

    SetupDimensions();

    fCore = core;
}

CustomSourceShielded::CustomSourceShielded() {
    pMaterial = new SourceMaterial();

    shieldRadius = 0.725 * mm;

    SetupDimensions();
    fPositionFilename = "";
    fCore = "G4_Yb";
}
CustomSourceShielded::~CustomSourceShielded() {
    delete pMaterial;
}

void CustomSourceShielded::SetCore(G4String core) {
    fCore = core;
}

void CustomSourceShielded::SetupDimensions() {
  // Active core
  sourceRadius = 0.2*mm;
  halfSourceHeight = 1.6*mm;

  // Active core parameters
  coreRadius = sourceRadius;
  coreHalfZ = halfSourceHeight;

  // Capsule
  innerCyl1CapsuleRadius = 0.2*mm;
  outerCyl1CapsuleRadius = 0.3*mm;
  heightCyl1CapsuleHalf = 1.6*mm;

  // Extra capsule cylinder near the cable
  innerCyl2CapsuleRadius = 0.0*mm;
  outerCyl2CapsuleRadius = 0.3*mm;
  heightCyl2CapsuleHalf = 0.05*mm;
 
  // Capsule cone near cable
  heightCapsuleTop = 0.2*mm;

  // Spherical capsule tip
  innerMinConCapsuleRadius = 0.0*mm;
  outerMinConCapsuleRadius = 0.2*mm;
  innerMaxConCapsuleRadius = 0.0*mm;
  outerMaxConCapsuleRadius = 0.3*mm;
  heightConCapsuleHalf = 0.1*mm;

  // Cable
  cableRadius = 0.23*mm;
  halfCableHeight = 1.0*mm;
  
  // AirGap
  airGap = 0.050*mm;

  // Shield
  halfShieldHeight = 5.0*mm;

  rectangleWidth = 0.1625*mm;
  rectangleHeight = halfShieldHeight;
  rectangleLength = shieldRadius;

  startPhi = 0.;
  spanPhi = 2. * CLHEP::pi;
  spanTheta = CLHEP::pi/2.;

  sourceOffsetY = (shieldRadius - outerCyl1CapsuleRadius - 0.5 * airGap);
}

G4ThreeVector CustomSourceShielded::getSourceOffset() {
    return G4ThreeVector(0.0, -sourceOffsetY, 0.0);
}

void CustomSourceShielded::createSource() {
  G4Material* steel = pMaterial->GetMat("Steel");
  G4Material* Pt = pMaterial->GetMat("G4_Pt");
  G4Material* coreMaterial = pMaterial->GetMat(fCore);

  //Core
  G4Tubs* source = new G4Tubs("source", 0.0*mm, sourceRadius, halfSourceHeight, startPhi, spanPhi);

  source_log = new G4LogicalVolume(source, coreMaterial, "source_log");
  G4VisAttributes* source_att = new G4VisAttributes();
  source_att->SetColor(1, 0, 0);
  source_att->SetVisibility(true);
  source_att->SetForceSolid(true);
  source_log->SetVisAttributes(source_att);

  // Steel Capsule
  G4Tubs* capsuleCyl1 = new G4Tubs("capsuleCyl1", innerCyl1CapsuleRadius, outerCyl1CapsuleRadius, heightCyl1CapsuleHalf, startPhi, spanPhi);
  G4Tubs* capsuleCyl2 = new G4Tubs("capsuleCyl2", innerCyl2CapsuleRadius, outerCyl2CapsuleRadius, heightCyl2CapsuleHalf, startPhi, spanPhi);
  G4Cons* capsuleCone  = new G4Cons("capsuleCone", innerMinConCapsuleRadius, outerMinConCapsuleRadius,
            innerMaxConCapsuleRadius, outerMaxConCapsuleRadius, heightConCapsuleHalf, startPhi, spanPhi);
  G4Ellipsoid* capsEllipsoid = new G4Ellipsoid("capsEllipsoid", outerCyl1CapsuleRadius, outerCyl1CapsuleRadius, heightCapsuleTop, 0.0*mm, heightCapsuleTop);
  G4VSolid* capsuleHalf1 = new G4UnionSolid("capsuleHalf1", capsuleCyl1, capsuleCyl2, 0, G4ThreeVector(0.0, 0.0, -(heightCyl1CapsuleHalf + heightCyl2CapsuleHalf)));
  G4VSolid* capsuleHalf2 = new G4UnionSolid("capsuleHalf2", capsuleHalf1, capsuleCone, 0, G4ThreeVector(0.0, 0.0,
                              -(heightCyl1CapsuleHalf + 2*heightCyl2CapsuleHalf + heightConCapsuleHalf)));
  G4VSolid* capsule = new G4UnionSolid("capsule", capsuleHalf2, capsEllipsoid, 0, G4ThreeVector(0.0, 0.0, heightCyl1CapsuleHalf));

  capsule_log = new G4LogicalVolume(capsule, steel, "capsule_log");
  G4VisAttributes* capsule_att = new G4VisAttributes();
  capsule_att->SetColor(0.5, 0.5, 0.5);
  capsule_att->SetVisibility(true);
  capsule_att->SetForceSolid(false);
  capsule_log->SetVisAttributes(capsule_att);

  // Steel Cable
  G4Tubs* cable_vol = new G4Tubs("cable_vol", 0.0*mm , cableRadius, halfCableHeight, startPhi, spanPhi);
  
  cable_log = new G4LogicalVolume(cable_vol, steel, "cable_log");
  G4VisAttributes* cable_att = new G4VisAttributes();
  cable_att->SetColor(0, 0, 1);
  cable_att->SetVisibility(true);
  cable_att->SetForceSolid(true);
  cable_log->SetVisAttributes(cable_att);

  ///////////////////////////////////////////////

  // Shield window solid
  G4Tubs* shieldWindow = new G4Tubs("shieldWindow", 0.0*mm,
                                    shieldRadius, halfShieldHeight,
                                    startPhi, spanPhi);

  G4Box *removalRect = new G4Box("shieldRect", rectangleLength, rectangleWidth, rectangleHeight);

  G4VSolid *windowInterm = new G4SubtractionSolid("windowInterm", shieldWindow, removalRect, 0,
                                                  G4ThreeVector(0.0, -(shieldRadius - rectangleWidth), 0.0));

  G4Tubs* shieldGroove = new G4Tubs("shieldGroove", 0.0*mm,
                                    outerCyl1CapsuleRadius+0.5*airGap, halfShieldHeight,
                                    startPhi, spanPhi);

  G4VSolid* Shield= new G4SubtractionSolid("Shield",windowInterm,shieldGroove, 0,
                                           G4ThreeVector(0.0, -sourceOffsetY, 0.0));

  shield_log = new G4LogicalVolume(Shield, Pt, "Shield_log");
  G4VisAttributes* Shield_att = new G4VisAttributes();
  Shield_att->SetColor(1, 0, 1);
  Shield_att->SetVisibility(true);
  Shield_att->SetForceSolid(false);
  shield_log->SetVisAttributes(Shield_att);
  G4cout << "Source has been created" << G4endl;
}

void CustomSourceShielded::placeSource(ControlPoint cpt, G4VPhysicalVolume *mother) {

    for (size_t i = 0; i < cpt.dwellPositions.size(); i++) {
        G4ThreeVector sourceOffset = this->getSourceOffset();
        G4ThreeVector shift = cpt.dwellPositions[i].pos;
        G4double angle = cpt.dwellPositions[i].angle;
        G4cout << "Placing source at " << shift + sourceOffset << " with shield angle " << angle << G4endl;

        G4RotationMatrix *shieldRotation = new G4RotationMatrix();
        G4RotationMatrix *invertRotation = new G4RotationMatrix();
        shieldRotation->rotateZ(angle);
        sourceOffset.transform(*shieldRotation);
        *invertRotation = shieldRotation->invert();
        G4ThreeVector cableOffset = G4ThreeVector(0,0,-halfSourceHeight-2*heightCyl2CapsuleHalf-2.*heightConCapsuleHalf-halfCableHeight) + sourceOffset;

        this->placedObjects.push_back(new G4PVPlacement(0, shift + sourceOffset, "source",
    						  this->source_log, mother, false, i));
        this->placedObjects.push_back(new G4PVPlacement(0, shift + sourceOffset, "capsule",
    						  this->capsule_log, mother, false, i));
        this->placedObjects.push_back(new G4PVPlacement(0, shift + cableOffset, "cable",
    						  this->cable_log, mother, false, i));
        this->placedObjects.push_back(new G4PVPlacement(invertRotation, shift, "Shield",
    					          this->shield_log, mother, false, i,true));
        G4cout << "Placing shield at " << shift << G4endl;
    }

    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}


void CustomSourceShielded::cleanSource() {
    for (size_t i = 0; i < this->placedObjects.size(); i++) {
        delete this->placedObjects[i];
    }
    this->placedObjects.clear();
    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}
