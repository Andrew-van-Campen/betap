#include "PatientMaterial.hh"
#include <sstream>
#include "G4NistManager.hh"
#include "G4UserLimits.hh"
#include "G4SystemOfUnits.hh"

using namespace std;

PatientMaterial::PatientMaterial()
{
    NumberOfMaterials = 0;
}

PatientMaterial::~PatientMaterial() {}

size_t PatientMaterial::GetMatIndex(size_t TissueNumber, G4double density)
{
    if (TissueNumber >= MatList.size())
    {
        cerr << "Invalid Tissue number: " << TissueNumber << endl;
        exit(1);
    }

    // Only return the index associated with the flat vector array.
    // TissueNumber should begin with 0 (in contrast with the egsphant definition... it begins with 1)
    size_t matIndex;
    int localMatIndex;
    // Determining if it is a single or multiple density material
    if (NbIncrementList[TissueNumber] == 1)
    {
        localMatIndex = 0;
    }
    else
    {
        localMatIndex = (density - MinDensityList[TissueNumber]) / (MaxDensityList[TissueNumber] - MinDensityList[TissueNumber]) * NbIncrementList[TissueNumber];
    }

    if (localMatIndex > NbIncrementList[TissueNumber])
    {
        localMatIndex = NbIncrementList[TissueNumber] - 1;
    }

    if (localMatIndex < 0) localMatIndex = 0;

    size_t TissueIndex = 0;

    // First, we must determine the proper material :
    for (size_t i = 0; i < TissueNumber; i++)
    {
        TissueIndex += NbIncrementList[i];
    }

    matIndex = localMatIndex + TissueIndex;

    return matIndex;
}

std::vector<G4Material *> PatientMaterial::GetFlatMatVector()
{
    vector<G4Material *> FlatMatVect;
    size_t numMaterials = 0;
    for (size_t i = 0; i < NbIncrementList.size(); i++)
    {
        numMaterials += NbIncrementList[i];
    }

    FlatMatVect.reserve(numMaterials);

    for (size_t i = 0; i < MatList.size(); i++)
    {
        for (size_t j = 0; j < MatList[i].size(); j++)
        {
            FlatMatVect.push_back(MatList[i][j]);
        }
    }

    if (NbIncrementList.size() != MatList.size())
    {
        cerr << "The Number of NbIncrement recorded doesn't match the number of Materials registered... tcheck your materials " << NbIncrementList.size() << " " << MatList.size() << endl;
        exit(1);
    }

    // Sanity check :
    if (numMaterials != FlatMatVect.size())
    {
        cerr << "Error in FlatMatVector calculation.... abort" << numMaterials << " " << FlatMatVect.size() << endl;
        exit(1);
    }

    return FlatMatVect;
}

G4int PatientMaterial::CreateFor(G4String TissueName)
{
    G4String name, symbol;
    G4int nel;
    G4int defaultIncrement = 100;
    G4NistManager *man = G4NistManager::Instance();

    G4Element *elH = man->FindOrBuildElement(1);
    G4Element *elC = man->FindOrBuildElement(6);
    G4Element *elN = man->FindOrBuildElement(7);
    G4Element *elO = man->FindOrBuildElement(8);
    G4Element *elNa = man->FindOrBuildElement(11);
    G4Element *elMg = man->FindOrBuildElement(12);
    G4Element *elP = man->FindOrBuildElement(15);
    G4Element *elS = man->FindOrBuildElement(16);
    G4Element *elCl = man->FindOrBuildElement(17);
    G4Element *elAr = man->FindOrBuildElement(18);
    G4Element *elK = man->FindOrBuildElement(19);
    G4Element *elCa = man->FindOrBuildElement(20);
    G4Element *elZn = man->FindOrBuildElement(30);
    G4Element *elW = man->FindOrBuildElement(74);
    G4Element *elMn = man->FindOrBuildElement(25);
    G4Element *elSi = man->FindOrBuildElement(14);
    G4Element *elFe = man->FindOrBuildElement(26);
    G4Element *elNi = man->FindOrBuildElement(28);
    G4Element *elCr = man->FindOrBuildElement(24);
    G4Element *elI  = man->FindOrBuildElement(53);

    if (TissueName == "Adipose")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material* localMat = new G4Material(fullName, density=density, nel=7);
            localMat->AddElement(elH, 0.114);
            localMat->AddElement(elC, 0.598);
            localMat->AddElement(elN, 0.007);
            localMat->AddElement(elO, 0.278);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elS, 0.001);
            localMat->AddElement(elCl, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Adipose_High_Z")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material* localMat = new G4Material(fullName, density=density, nel=7);
            localMat->AddElement(elH, 0.112);
            localMat->AddElement(elC, 0.517);
            localMat->AddElement(elN, 0.013);
            localMat->AddElement(elO, 0.355);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elS, 0.001);
            localMat->AddElement(elCl, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Adipose_Low_Z")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material* localMat = new G4Material(fullName, density=density, nel=7);
            localMat->AddElement(elH, 0.116);
            localMat->AddElement(elC, 0.681);
            localMat->AddElement(elN, 0.002);
            localMat->AddElement(elO, 0.198);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elS, 0.001);
            localMat->AddElement(elCl, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Air")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.0012 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density, man->FindOrBuildMaterial("G4_AIR"));
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Bladder")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material* localMat = new G4Material(fullName, density=density, nel=9);
            localMat->AddElement(elH, 0.108);
            localMat->AddElement(elC, 0.035);
            localMat->AddElement(elN, 0.015);
            localMat->AddElement(elO, 0.830);
            localMat->AddElement(elNa, 0.003);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.001);
            localMat->AddElement(elCl, 0.005);
            localMat->AddElement(elK, 0.002);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Bladder_Empty")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material* localMat = new G4Material(fullName, density=density, nel=9);
            localMat->AddElement(elH, 0.105);
            localMat->AddElement(elC, 0.096);
            localMat->AddElement(elN, 0.026);
            localMat->AddElement(elO, 0.761);
            localMat->AddElement(elNa, 0.002);
            localMat->AddElement(elP, 0.002);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.003);
            localMat->AddElement(elK, 0.003);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Bladder_Filled")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material* localMat = new G4Material(fullName, density=density, nel=9);
            localMat->AddElement(elH, 0.108);
            localMat->AddElement(elC, 0.035);
            localMat->AddElement(elN, 0.015);
            localMat->AddElement(elO, 0.830);
            localMat->AddElement(elNa, 0.003);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.001);
            localMat->AddElement(elCl, 0.005);
            localMat->AddElement(elK, 0.002);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Bone")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.034);
            localMat->AddElement(elC, 0.155);
            localMat->AddElement(elN, 0.042);
            localMat->AddElement(elO, 0.435);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elMg, 0.002);
            localMat->AddElement(elP, 0.103);
            localMat->AddElement(elS, 0.003);
            localMat->AddElement(elCa, 0.225);

            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Bone_Mineral")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 4);
            localMat->AddElement(elH, 0.002);
            localMat->AddElement(elO, 0.414);
            localMat->AddElement(elP, 0.185);
            localMat->AddElement(elCa, 0.399);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Breast")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 7);
            localMat->AddElement(elH, 0.108);
            localMat->AddElement(elC, 0.356);
            localMat->AddElement(elN, 0.022);
            localMat->AddElement(elO, 0.509);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.002);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Breast_3070")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 8);
            localMat->AddElement(elH, 0.108);
            localMat->AddElement(elC, 0.412);
            localMat->AddElement(elN, 0.023);
            localMat->AddElement(elO, 0.452);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Breast_5050")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 8);
            localMat->AddElement(elH, 0.110);
            localMat->AddElement(elC, 0.465);
            localMat->AddElement(elN, 0.019);
            localMat->AddElement(elO, 0.403);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Breast_8020")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 8);
            localMat->AddElement(elH, 0.110);
            localMat->AddElement(elC, 0.465);
            localMat->AddElement(elN, 0.019);
            localMat->AddElement(elO, 0.403);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Breast_Calcification")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 6);
            localMat->AddElement(elH, 0.003);
            localMat->AddElement(elC, 0.016);
            localMat->AddElement(elN, 0.005);
            localMat->AddElement(elO, 0.407);
            localMat->AddElement(elP, 0.187);
            localMat->AddElement(elCa, 0.382);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Cartilage")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 8);
            localMat->AddElement(elH, 0.096);
            localMat->AddElement(elC, 0.099);
            localMat->AddElement(elN, 0.022);
            localMat->AddElement(elO, 0.744);
            localMat->AddElement(elNa, 0.005);
            localMat->AddElement(elP, 0.022);
            localMat->AddElement(elS, 0.009);
            localMat->AddElement(elCl, 0.003);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Bone_Cortical")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.034);
            localMat->AddElement(elC, 0.155);
            localMat->AddElement(elN, 0.042);
            localMat->AddElement(elO, 0.435);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elMg, 0.002);
            localMat->AddElement(elP, 0.103);
            localMat->AddElement(elS, 0.003);
            localMat->AddElement(elCa, 0.225);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if( TissueName == "Densimet")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001*g/cm3;
        G4double maxDensity = 20.0*g/cm3;
        G4int nbIncrements = 250;

        for (G4int i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "_" << density/g*cm3 << " g/cm3" ;
            G4String fullName = namestream.str();
            G4Material* localMat = new G4Material(fullName, density=density, nel=3);
            localMat->AddElement(elW, 0.95);
            localMat->AddElement(elNi, 0.016);
            localMat->AddElement(elFe, 0.034);
            localMatList.push_back(localMat);
        }

      MinDensityList.push_back(minDensity);
      MaxDensityList.push_back(maxDensity);
      NbIncrementList.push_back(nbIncrements);
      MatList.push_back(localMatList);
    }
    else if (TissueName == "SoftTissue_Female")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.106);
            localMat->AddElement(elC, 0.315);
            localMat->AddElement(elN, 0.024);
            localMat->AddElement(elO, 0.547);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.002);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.001);
            localMat->AddElement(elK, 0.002);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Femur")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 10);
            localMat->AddElement(elH, 0.070);
            localMat->AddElement(elC, 0.345);
            localMat->AddElement(elN, 0.028);
            localMat->AddElement(elO, 0.368);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elMg, 0.001);
            localMat->AddElement(elP, 0.051);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.001);
            localMat->AddElement(elCa, 0.110);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Heart_Bloody")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 10);
            localMat->AddElement(elH, 0.103);
            localMat->AddElement(elC, 0.121);
            localMat->AddElement(elN, 0.032);
            localMat->AddElement(elO, 0.734);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.003);
            localMat->AddElement(elK, 0.002);
            localMat->AddElement(elCa, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if( TissueName == "Heart")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        G4int nbIncrements = 250;

        for (G4int i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "_" << density/g*cm3 << " g/cm3" ;
            G4String fullName = namestream.str();
            G4Material* localMat = new G4Material(fullName, density=density, nel=10);
            localMat->AddElement(elCa, 0.00023);
            localMat->AddElement(elC , 0.0911);
            localMat->AddElement(elH , 0.0976);
            localMat->AddElement(elMg, 0.00019);
            localMat->AddElement(elN , 0.0247);
            localMat->AddElement(elO , 0.7810);
            localMat->AddElement(elP , 0.0010);
            localMat->AddElement(elK , 0.0020);
            localMat->AddElement(elNa, 0.0021);
            localMat->AddElement(elZn, 0.00008);
            localMatList.push_back(localMat);
        }

      MinDensityList.push_back(minDensity);
      MaxDensityList.push_back(maxDensity);
      NbIncrementList.push_back(nbIncrements);
      MatList.push_back(localMatList);
    }
    else if (TissueName == "Heart_Empty")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.104);
            localMat->AddElement(elC, 0.139);
            localMat->AddElement(elN, 0.029);
            localMat->AddElement(elO, 0.718);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.002);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.002);
            localMat->AddElement(elK, 0.003);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Heart_Fatty")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.103);
            localMat->AddElement(elC, 0.182);
            localMat->AddElement(elN, 0.031);
            localMat->AddElement(elO, 0.674);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.002);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.002);
            localMat->AddElement(elK, 0.003);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if( TissueName == "IodineSolution")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001*g/cm3;
        G4double maxDensity = 4.0*g/cm3;
        G4int nbIncrements = 250;

        for (G4int i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "_" << density/g*cm3 << " g/cm3" ;
            G4String fullName = namestream.str();
            G4Material* localMat = new G4Material(fullName, density=density, nel=4);
            localMat->AddElement(elH, 0.106);
            localMat->AddElement(elO, 0.844);
            localMat->AddElement(elI, 0.042);
            localMat->AddElement(elNa, 0.008);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Lung")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.103);
            localMat->AddElement(elC, 0.105);
            localMat->AddElement(elN, 0.031);
            localMat->AddElement(elO, 0.749);
            localMat->AddElement(elNa, 0.002);
            localMat->AddElement(elP, 0.002);
            localMat->AddElement(elS, 0.003);
            localMat->AddElement(elCl, 0.003);
            localMat->AddElement(elK, 0.002);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Lung_Congested")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.105);
            localMat->AddElement(elC, 0.083);
            localMat->AddElement(elN, 0.023);
            localMat->AddElement(elO, 0.779);
            localMat->AddElement(elNa, 0.002);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.003);
            localMat->AddElement(elK, 0.002);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Lung_Healthy")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.103);
            localMat->AddElement(elC, 0.105);
            localMat->AddElement(elN, 0.031);
            localMat->AddElement(elO, 0.749);
            localMat->AddElement(elNa, 0.002);
            localMat->AddElement(elP, 0.002);
            localMat->AddElement(elS, 0.003);
            localMat->AddElement(elCl, 0.003);
            localMat->AddElement(elK, 0.002);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Lung_Parenchyma")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.103);
            localMat->AddElement(elC, 0.101);
            localMat->AddElement(elN, 0.029);
            localMat->AddElement(elO, 0.755);
            localMat->AddElement(elNa, 0.002);
            localMat->AddElement(elP, 0.002);
            localMat->AddElement(elS, 0.003);
            localMat->AddElement(elCl, 0.003);
            localMat->AddElement(elK, 0.002);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "SoftTissue_Male")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.105);
            localMat->AddElement(elC, 0.256);
            localMat->AddElement(elN, 0.027);
            localMat->AddElement(elO, 0.602);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.002);
            localMat->AddElement(elS, 0.003);
            localMat->AddElement(elCl, 0.002);
            localMat->AddElement(elK, 0.002);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Mammary_High")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 8);
            localMat->AddElement(elH, 0.102);
            localMat->AddElement(elC, 0.158);
            localMat->AddElement(elN, 0.037);
            localMat->AddElement(elO, 0.698);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Mammary_Low")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 8);
            localMat->AddElement(elH, 0.109);
            localMat->AddElement(elC, 0.506);
            localMat->AddElement(elN, 0.023);
            localMat->AddElement(elO, 0.358);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.001);
            localMat->AddElement(elCl, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Mammary")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 8);
            localMat->AddElement(elH, 0.106);
            localMat->AddElement(elC, 0.332);
            localMat->AddElement(elN, 0.030);
            localMat->AddElement(elO, 0.527);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if( TissueName == "Metal")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001*g/cm3;
        G4double maxDensity = 20.0*g/cm3;
        G4int nbIncrements = 250;

        for (G4int i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "_" << density/g*cm3 << " g/cm3" ;
            G4String fullName = namestream.str();
            G4Material* localMat = new G4Material(fullName, density=density, nel=6);
            localMat->AddElement(elFe, 0.6792);
            localMat->AddElement(elCr, 0.1900);
            localMat->AddElement(elNi, 0.1000);
            localMat->AddElement(elMn, 0.0200);
            localMat->AddElement(elSi, 0.0100);
            localMat->AddElement(elC, 0.0008);
            localMatList.push_back(localMat);
        }

      MinDensityList.push_back(minDensity);
      MaxDensityList.push_back(maxDensity);
      NbIncrementList.push_back(nbIncrements);
      MatList.push_back(localMatList);
    }
    else if (TissueName == "Muscle")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.102);
            localMat->AddElement(elC, 0.143);
            localMat->AddElement(elN, 0.034);
            localMat->AddElement(elO, 0.710);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.002);
            localMat->AddElement(elS, 0.003);
            localMat->AddElement(elCl, 0.001);
            localMat->AddElement(elK, 0.004);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Prostate")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 10);
            localMat->AddElement(elCa, 0.00023);
            localMat->AddElement(elC, 0.0911);
            localMat->AddElement(elH, 0.0976);
            localMat->AddElement(elMg, 0.00019);
            localMat->AddElement(elN, 0.0247);
            localMat->AddElement(elO, 0.7810);
            localMat->AddElement(elP, 0.0010);
            localMat->AddElement(elK, 0.0020);
            localMat->AddElement(elNa, 0.0021);
            localMat->AddElement(elZn, 0.00008);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Rectum")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.106);
            localMat->AddElement(elC, 0.115);
            localMat->AddElement(elN, 0.022);
            localMat->AddElement(elO, 0.751);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.001);
            localMat->AddElement(elCl, 0.002);
            localMat->AddElement(elK, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Red_Marrow")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 9);
            localMat->AddElement(elH, 0.105);
            localMat->AddElement(elC, 0.414);
            localMat->AddElement(elN, 0.034);
            localMat->AddElement(elO, 0.439);
            localMat->AddElement(elP, 0.001);
            localMat->AddElement(elS, 0.002);
            localMat->AddElement(elCl, 0.002);
            localMat->AddElement(elK, 0.002);
            localMat->AddElement(elFe, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if( TissueName == "SiliconRubber")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001*g/cm3;
        G4double maxDensity = 4.0*g/cm3;
        G4int nbIncrements = 250;

        for (G4int i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "_" << density/g*cm3 << " g/cm3" ;
            G4String fullName = namestream.str();
            G4Material* localMat = new G4Material(fullName, density=density, nel=4);
            localMat->AddElement(elH, 0.082);
            localMat->AddElement(elC, 0.324);
            localMat->AddElement(elO, 0.216);
            localMat->AddElement(elSi, 0.379);
            localMatList.push_back(localMat);
        }

      MinDensityList.push_back(minDensity);
      MaxDensityList.push_back(maxDensity);
      NbIncrementList.push_back(nbIncrements);
      MatList.push_back(localMatList);
    }
    else if( TissueName == "Skin")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        G4int nbIncrements = 250;

        for (G4int i = 0; i < nbIncrements; i++)
        {
        	stringstream namestream;
        	G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
        	namestream << TissueName << "_" << density/g*cm3 << " g/cm3" ;
        	G4String fullName = namestream.str();
        	G4Material* localMat = new G4Material(fullName, density=density, nel=10);
        	localMat->AddElement(elH, 0.095);
        	localMat->AddElement(elC, 0.455);
        	localMat->AddElement(elN, 0.025);
        	localMat->AddElement(elO, 0.355);
        	localMat->AddElement(elNa, 0.001);
        	localMat->AddElement(elP, 0.021);
        	localMat->AddElement(elS, 0.001);
        	localMat->AddElement(elCl, 0.001);
        	localMat->AddElement(elK, 0.001);
        	localMat->AddElement(elCa, 0.045);
        	localMatList.push_back(localMat);
        }

      MinDensityList.push_back(minDensity);
      MaxDensityList.push_back(maxDensity);
      NbIncrementList.push_back(nbIncrements);
      MatList.push_back(localMatList);
    }
    else if (TissueName == "SoftTissue")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density, man->FindOrBuildMaterial("G4_TISSUE_SOFT_ICRP"));
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Water")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density, man->FindOrBuildMaterial("G4_WATER"));
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else if (TissueName == "Yellow_Marrow")
    {
        std::vector<G4Material *> localMatList;
        G4double minDensity = 0.001 * g / cm3;
        G4double maxDensity = 4.0 * g / cm3;
        size_t nbIncrements = 250;

        for (size_t i = 0; i < nbIncrements; i++)
        {
            stringstream namestream;
            G4double density = minDensity + (maxDensity - minDensity) * i / nbIncrements;
            namestream << TissueName << "__" << density / g * cm3 << " g/cm3";
            G4String fullName = namestream.str();
            G4Material *localMat = new G4Material(fullName, density = density, nel = 7);
            localMat->AddElement(elH, 0.115);
            localMat->AddElement(elC, 0.644);
            localMat->AddElement(elN, 0.007);
            localMat->AddElement(elO, 0.231);
            localMat->AddElement(elNa, 0.001);
            localMat->AddElement(elS, 0.001);
            localMat->AddElement(elCl, 0.001);
            localMatList.push_back(localMat);
        }

        MinDensityList.push_back(minDensity);
        MaxDensityList.push_back(maxDensity);
        NbIncrementList.push_back(nbIncrements);
        MatList.push_back(localMatList);
    }
    else
    {
        cerr << "Sorry... but the PatientMaterial wasn't able to find a matching for : " << TissueName << endl;
    }

    return 0;
}

G4int PatientMaterial::GetNumberOfMaterials()
{
    return NumberOfMaterials;
}

G4Material *PatientMaterial::GetMat(G4String material)
{
    G4NistManager *man = G4NistManager::Instance();
    return man->FindOrBuildMaterial(material);
}
