//////////////////////////////////////////////////////////////////////////
//
// Author: Shirin A. Enger <shirin@enger.se>
//
// License & Copyright
// ===================
//
// Copyright 2015 Shirin A Enger <shirin@enger.se>
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////

#include "SourceMaterial.hh"

#include "globals.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"
#include "G4Element.hh"
#include "G4NistManager.hh"

    #include <stdio.h>  /* defines FILENAME_MAX */
    #include <unistd.h>
    #define GetCurrentDir getcwd

SourceMaterial::SourceMaterial() {
    DefineMaterials();
    G4cout << "IN SOURCE MATERIAL CONSTRUCTOR " << std::endl;
}

SourceMaterial::~SourceMaterial() {}

void SourceMaterial::DefineMaterials() {
    G4NistManager* nist_manager = G4NistManager::Instance();
    G4Element*  H  = nist_manager->FindOrBuildElement("H");
    G4Element*  Fe = nist_manager->FindOrBuildElement("Fe");
    G4Element*  Cr =nist_manager->FindOrBuildElement("Cr");
    G4Element*  Ni = nist_manager->FindOrBuildElement("Ni");
    G4Element*  Mn = nist_manager->FindOrBuildElement("Mn");
    G4Element*  Si = nist_manager->FindOrBuildElement("Si");
    G4Element*  C  = nist_manager->FindOrBuildElement("C");
    G4Element*  O  = nist_manager->FindOrBuildElement("O");
    G4Element*  Al  = nist_manager->FindOrBuildElement("Al");
    G4Element*  yb  = nist_manager->FindOrBuildElement("Yb");


    Pt = nist_manager->FindOrBuildMaterial("G4_Pt");
    Ir = nist_manager->FindOrBuildMaterial("G4_Ir");
    Se = nist_manager->FindOrBuildMaterial("G4_Se");
    Gd = nist_manager->FindOrBuildMaterial("G4_Gd");
    Yb = nist_manager->FindOrBuildMaterial("G4_Yb");
    Co = nist_manager->FindOrBuildMaterial("G4_Co");
    I = nist_manager->FindOrBuildMaterial("G4_I");
    W = nist_manager->FindOrBuildMaterial("G4_W");
    Pb = nist_manager->FindOrBuildMaterial("G4_Pb");
    Sn = nist_manager->FindOrBuildMaterial("G4_Sn");
    Air  = nist_manager->FindOrBuildMaterial("G4_AIR");
    H2O =nist_manager->FindOrBuildMaterial("G4_WATER");
    Be = nist_manager->FindOrBuildMaterial("G4_Be");

    // Plexiglass
    plexiglass = new G4Material("Plexiglass",1.19*g/cm3,3);
    plexiglass->AddElement(H,0.08);
    plexiglass->AddElement(C,0.60);
    plexiglass->AddElement(O,0.32);

    // Stainless steel 304
    steel = new G4Material( "Steel", 8.0*g/cm3, 6 );
    steel->AddElement( Fe, 0.6792 );
    steel->AddElement( Cr, 0.1900 );
    steel->AddElement( Ni, 0.1000 );
    steel->AddElement( Mn, 0.0200 );
    steel->AddElement( Si, 0.0100 );
    steel->AddElement( C , 0.0008 );    

    // Stainless steel 316L 8 g/cm3
    SS316L = new G4Material( "SS316L", 8.02*g/cm3, 5 );
    SS316L->AddElement( Fe, 0.6800 );
    SS316L->AddElement( Cr, 0.1700 );
    SS316L->AddElement( Ni, 0.1200 );
    SS316L->AddElement( Mn, 0.0200 );
    SS316L->AddElement( Si, 0.0100 );

    // Stainless steel 316L 5 g/cm3
    SS316L5 = new G4Material( "SS316L5", 5.0*g/cm3, 5 );
    SS316L5->AddElement( Fe, 0.6800 );
    SS316L5->AddElement( Cr, 0.1700 );
    SS316L5->AddElement( Ni, 0.1200 );
    SS316L5->AddElement( Mn, 0.0200 );
    SS316L5->AddElement( Si, 0.0100 );


    gold = new G4Material("gold",79.,196.97*g/mole,19.32*g/cm3);

    // Ytterbium oxide
    ytterbiumoxide = new G4Material("Ytterbium oxide",6.9*g/cm3,2);
    ytterbiumoxide->AddElement(yb,0.878);
    ytterbiumoxide->AddElement(O,0.122);

    ceramic = new G4Material("allumina",2.88*g/cm3,2);
    ceramic->AddElement(Al,2);
    ceramic->AddElement(O,3);

    G4double density = universe_mean_density;
    G4double pressure = 3.e-18*pascal;
    G4double temperature = 2.73*kelvin;

    Vacuum = new G4Material("Galactic", 1., 1.01*g/mole, density, kStateGas, temperature, pressure);

    LoadMaterialsTable();


    // //iterate through and add the materials
    std::map<std::pair<std::string, double>, std::vector<std::pair<std::string, double>>>::iterator it;
    for (it = matTable.begin(); it != matTable.end(); it++)
    {
            // G4cout<< " adding material " << it->first.first << "  with elements: " << it -> second.size() <<  std::endl;  
            G4Material* mat = new G4Material(it->first.first, it->first.second*g/cm3, it->second.size());

            for (int i = 0; i < it->second.size(); i++)
            {
                // std::cout << "    here " << it->second[i].first << " " << it -> second[i].second << std::endl;
                mat -> AddElement(nist_manager->FindOrBuildElement(it -> second[i].first), it->second[i].second);
            }
    }

}

G4Material* SourceMaterial::GetMat(G4String material) {
    G4Material* mat = G4Material::GetMaterial(material);

    if (mat)
        return mat;

    // Try the nist database.
    G4NistManager* nist_manager = G4NistManager::Instance();
    mat = nist_manager->FindOrBuildMaterial(material);

    if (mat)
        return mat;




    return nullptr;
}

void SourceMaterial::LoadMaterialsTable()
{
    std::ifstream input("./ApplicatorMaterials");

    std::string line;
    bool firstLine = true;

    std::string currentMat;
    double currentDen;
     std::vector<std::pair<std::string, double>> compositionVec;

    while (std::getline(input, line))
    {
        // G4cout << "    reading a line " << std::endl;
        std::istringstream s(line);

        if (firstLine)
        {
            if (currentMat != "")
            {
                matTable[std::make_pair(currentMat, currentDen)] = compositionVec;
            }
            
            compositionVec.clear();
            std::string field;
            getline(s,field,',');
            currentMat = field;
            // G4cout << "    material is  " << field << std::endl;
            // names.push_back(field);
            getline(s,field,',');
            // densities.push_back(::atof(field.c_str()));
            currentDen = ::atof(field.c_str());
            firstLine = false;
            continue;
        }

        std::string field;
        std::string fieldTwo;;

        getline(s,field,',');
        getline(s,fieldTwo,',');

        if (field == "")
        {
            firstLine = true;
            continue;
        }

        compositionVec.push_back(std::make_pair(field, std::stod(fieldTwo)));
    }
    matTable[std::make_pair(currentMat, currentDen)] = compositionVec;

}
