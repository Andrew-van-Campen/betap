// GEANT4 //
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4VUserParallelWorld.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4VisAttributes.hh"
#include "G4TessellatedSolid.hh"
#include "G4PVPlacement.hh"
#include "G4Box.hh"

// BRACHYSOURCE //
#include "ApplicatorWorldConstruction.hh"
#include "ApplicatorWorldMessenger.hh"
#include "SourceMaterial.hh"


ApplicatorWorldConstruction::ApplicatorWorldConstruction(G4String name)
    : G4VUserParallelWorld(name)
{
    _materials_manager = new SourceMaterial();

    _messenger = new ApplicatorWorldMessenger(this);
}

ApplicatorWorldConstruction::~ApplicatorWorldConstruction()
{
}

void ApplicatorWorldConstruction::Construct()
{
    // if (_faces.size() == 0) return;

    G4VPhysicalVolume *ghostWorld = GetWorld();
    
    // TODO: Assuming that the logicals, positions, and rotations are in order,
    //       and the vectors are the same length.
       
    for (size_t i=0; i<_logicals.size(); i++)
    {
        auto logical = _logicals[i];
        auto position = _positions[i];
        auto rotation = _rotations[i];

        // Geant4 will remove this from the store later.
        new G4PVPlacement( rotation
                         , position
                         , logical
                         , "applicator_physical"
                         , ghostWorld->GetLogicalVolume()
                         , false, 0);
    }

    // TODO: Assuming no overlaps etc.
}


void ApplicatorWorldConstruction::DoneAddingVolume()
{
    // Construct the solid, material, and logical volume.
    auto solid = new G4TessellatedSolid("applicator_solid");

    for (auto face : _faces)
    {
        solid->AddFacet(face);
        
    }

    solid->SetSolidClosed(true);

    G4Material* component = _materials_manager->GetMat(_material);

    if (!component)
    {
        std::cerr << "The applicator material is invalid. G4_* materials only." << std::endl;
        exit(1);
    }

    G4Material* material = new G4Material( "applicator_material"
                                         , _density
                                         , 1);
    material->AddMaterial(component, 1.0);

    auto logical = new G4LogicalVolume( solid
                                  , material
                                  , "applicator_solid"
    );
    logical -> SetMaterial(NULL);
    logical ->SetVisAttributes(G4VisAttributes::Invisible);
    logical -> SetMaterial(material);
    

    _logicals.push_back(logical);

    auto rotation = new G4RotationMatrix();
    rotation->rotateX(_x_rotation);
    rotation->rotateY(_y_rotation);
    rotation->rotateZ(_z_rotation);

    _rotations.push_back(rotation);

    auto position = G4ThreeVector( _x_position
                                 , _y_position
                                 , _z_position);

    _positions.push_back(position);


    // Clear the verts and facts vectors, for the addition of the next volume.
    _vertices.clear();
    _faces.clear();

    
}
