#include "G4Event.hh"
#include "G4GeneralParticleSource.hh"
#include "PrimaryGeneratorAction.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4IonTable.hh"
#include "G4RotationMatrix.hh"
#include "G4SystemOfUnits.hh"
#include "TreatmentPlan.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
{
    particleGun = new G4GeneralParticleSource();
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
    delete particleGun;
}

void PrimaryGeneratorAction::setControlPoint(ControlPoint cpt, SourceModel *source)
{
    this->particleGun->ClearAll();
    G4double length = source->getActiveCoreHalfZ();
    G4double radius = source->getActiveCoreRadius();

    // When either core length or radius are not well defined,
    // the emmissions will come from a point source
    // Best to kill simulation.
    if (length <= 0.0) {
      std::cerr << "Source Core Length <= 0 or coreHalfZ variable not set: " << std::endl;
      exit(1);
    }
    if (radius <= 0.0){
      std::cerr << "Source Core Radius <= 0 or coreRadius variable not set: " << std::endl;
      exit(1);
    }

    G4int coreA = source->getCoreA();
    G4int coreZ = source->getCoreZ();

    G4ParticleDefinition *ion;
    ion = G4IonTable::GetIonTable()->GetIon(coreZ, coreA, 0.0);

    for (size_t i = 0; i < cpt.dwellPositions.size(); i++)
    {
        particleGun->AddaSource(1.0);
        G4SingleParticleSource *sps = particleGun->GetCurrentSource();
        sps->SetParticleDefinition(ion);
        sps->SetParticleCharge(0);
        sps->GetPosDist()->SetPosDisType("Volume");
        sps->GetPosDist()->SetPosDisShape("Cylinder");
	      sps->GetAngDist()->SetAngDistType("iso");
        sps->GetPosDist()->SetRadius(radius);
        sps->GetPosDist()->SetHalfZ(length);
        sps->GetEneDist()->SetMonoEnergy(0);

        //////// ORIENTATION
        // V is direction vector
        G4ThreeVector V = G4ThreeVector(cpt.dwellPositions[i].rot[0],
                                        cpt.dwellPositions[i].rot[1],
                                        cpt.dwellPositions[i].rot[2]).unit();
        G4ThreeVector Xprime = V.orthogonal().unit();
        G4ThreeVector Yprime = V.cross(Xprime).unit();
        G4cout << "V: "  << V << G4endl;
        G4cout << "X': " << Xprime << G4endl;
        G4cout << "Y': " << Yprime << G4endl;
        G4cout << "ResCross: " << Yprime.cross(Xprime) << G4endl; // Maybe flip X/Y but doesn't matter since cylinder core is symmetric
        sps->GetPosDist()->SetPosRot1(Xprime);
        sps->GetPosDist()->SetPosRot2(Yprime);

        /////// POSITION
        sps->GetPosDist()->SetCentreCoords(cpt.dwellPositions[i].pos);
        //sps->GetPosDist()->SetCentreCoords(cpt.dwellPositions[i].pos + sourceOffset); // If using IMBT shield as part of source model
    }
    //particleGun->ListSource();
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event *anEvent)
{
    particleGun->GeneratePrimaryVertex(anEvent);
}
