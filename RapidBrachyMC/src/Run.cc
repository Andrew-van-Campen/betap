#include "Run.hh"
#include "G4SDManager.hh"

#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4THitsMap.hh"
#include "SimDose.hh"
#include <chrono>

Run::Run(std::vector<SimDose*> doseCollections, int numRuns) : G4Run()
{
    fNGammasCreated = 0;
    lnumRuns = numRuns;
    this->doseCollections = doseCollections;
}

Run::~Run()
{
}


void Run::Merge(const G4Run *aRun)
{
    // Merge is called from the global Run instance once per worker thread.
    // The purpose is to merge worker thread Run instance results into the global
    // instance.

    const Run *localRun = static_cast<const Run *>(aRun);
    fNGammasCreated += localRun->fNGammasCreated;

    // We only want to merge local runs into the global run after all control points
    // have been simulated to avoid overhead.
    if (this->runID + 1 == this->lnumRuns)
    {
        for (int i=0; i<doseCollections.size(); i++)
        {
            doseCollections[i]->merge_with(localRun->doseCollections[i]);
        }
    }
    G4Run::Merge(aRun);
}

void Run::RecordEvent(const G4Event *aEvent)
{
    G4HCofThisEvent *HCE = aEvent->GetHCofThisEvent();
    if (!HCE)
        return;
    G4THitsMap<G4double> *EvtMap = 0;
        auto start = std::chrono::high_resolution_clock::now();

    for (int i=0; i<HCE->GetNumberOfCollections(); i++)
    { 
        EvtMap = static_cast<G4THitsMap<G4double> *>(HCE->GetHC(i));
        if (EvtMap)
        {
            std::map<G4int, G4double *>::iterator itr = EvtMap->GetMap()->begin();
            for (; itr != EvtMap->GetMap()->end(); itr++)
            {
                doseCollections[i]->deposit_dose_event(*(itr->second), (itr->first));
            }
        }
    }
    

    G4Run::RecordEvent(aEvent);
        auto elapsed = std::chrono::high_resolution_clock::now() - start;
    long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
            bla+=microseconds;

    // std::cout << "Time Elapse: " << microseconds << "ms" << std::endl;
        // std::cout << "Time Elapse acc: " << bla << "ms" << std::endl;

}
