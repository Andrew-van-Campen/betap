#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdlib>

#include "G4VProcess.hh"
#include "TrackLengthEstimator.hh"
#include "G4VSolid.hh"
#include "G4Gamma.hh"
#include "G4VPhysicalVolume.hh"
#include "G4VPVParameterisation.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "DetectorConstruction.hh"

TrackLengthEstimator::TrackLengthEstimator(G4String name, G4int depth, DetectorConstruction *phant)
    : G4VPrimitiveScorer(name, depth), HCID(-1)
{
    SetUnit("Gy");
    this->egsphant = phant;
}

TrackLengthEstimator::~TrackLengthEstimator()
{
}

void TrackLengthEstimator::loadMuens(std::vector<G4String> matNames)
{
    this->numMaterials = matNames.size();
    this->matList = matNames;
    for (G4int i = 0; i < this->numMaterials; i++)
    {
        G4String matName = matList[i];
        if (std::getenv("BSOURCE_DIR") == NULL)
        {
	          G4cerr<<"BSOURCE_DIR environment variable not set!"<<G4endl;
            exit(1);
        }

        // Filename is mat name in lowercase
	std::transform(matName.begin(), matName.end(), matName.begin(), ::tolower);
        G4String filename = std::string(std::getenv("BSOURCE_DIR")) + "/muen_dats/" + matName + ".dat";
        //std::transform(filename.begin(), filename.end(), filename.begin(), ::tolower);
        std::cout << filename << std::endl;

        std::ifstream muFile(filename);
        if (!muFile)
        {
	          G4cerr<<"Cannot open "<<filename <<"."<<G4endl;
            exit(1);
            return;
        }

        int binsForMat;
        muFile >> binsForMat;
        std::vector<G4double> energies;
        std::vector<G4double> mooValues;
        for (G4int j = 0; j < binsForMat; j++)
        {
            G4double energy, moo;
            muFile >> energy >> moo;

            energy *= keV;
            moo *= cm2 / g;

            energies.push_back(energy);
            mooValues.push_back(moo);
        }
        muEnergies.push_back(energies);
        muValues.push_back(mooValues);

        muFile.close();
    }
}

G4bool TrackLengthEstimator::ProcessHits(G4Step *aStep, G4TouchableHistory *)
{
    G4double stepLength = aStep->GetStepLength();
    if (stepLength == 0.)
        return false;

    G4String current_material = aStep->GetTrack()->GetMaterial()->GetName();

    if (current_material.find("g/cm3") == std::string::npos)
        return false;

    //G4String particleName = aStep->GetTrack()->GetDefinition()->GetParticleName();
    G4ParticleDefinition *particle = aStep->GetTrack()->GetDefinition();
    if (particle == G4Gamma::GammaDefinition())
      //if (particleName == "gamma")
    {
        G4int idx = GetIndex(aStep);
        if (idx >= this->egsphant->getTotalVoxels())
            return false;

        G4int mat_id = this->egsphant->getUniqueMatNumbers()[idx];

        G4double energy = aStep->GetPreStepPoint()->GetKineticEnergy();

        G4int binNumber = FindBinLocation(energy, muEnergies[mat_id]);
        G4double muen = linLogLogInterpolate(energy,binNumber,muEnergies[mat_id],muValues[mat_id]);

        G4double dose = stepLength * muen * energy;
        dose *= aStep->GetPreStepPoint()->GetWeight();

        EvtMap->add(idx, dose);
    }

    return true;
}

void TrackLengthEstimator::Initialize(G4HCofThisEvent *HCE)
{
    EvtMap = new G4THitsMap<G4double>(GetMultiFunctionalDetector()->GetName(), GetName());
    if (HCID < 0)
    {
        HCID = GetCollectionID(0);
    }
    HCE->AddHitsCollection(HCID, (G4VHitsCollection *)EvtMap);
}


void TrackLengthEstimator::clear()
{
    EvtMap->clear();
}

void TrackLengthEstimator::SetUnit(const G4String &unit)
{
    CheckAndSetUnit(unit, "Dose");
}

G4int TrackLengthEstimator::FindBinLocation(G4double energy, std::vector<G4double>& points) {
  G4double e0 = points[0];
  G4int numBins = points.size();
  if (energy < e0) {
      energy = e0;
  }

  G4int lowerBound = 0;
  G4int upperBound = numBins - 1;

  // Binary search
  while (lowerBound <= upperBound) {
      G4int midBin = (lowerBound + upperBound) / 2;

      if ( energy < points[midBin] ) {
          upperBound = midBin-1;
      } else {
          lowerBound = midBin+1;
      }
    }

  return upperBound;
}



// This function is taken from G4LinLogLogInterpolation
G4double TrackLengthEstimator::linLogLogInterpolate(G4double x, G4int bin,
                std::vector<G4double>& points,
                std::vector<G4double>& data) {
  G4int nBins = data.size() - 1;
  G4double value = 0.;
  if (x < points[0]) {
      value = 0.;
  }
  else if (bin < nBins) {
      G4double e1 = points[bin];
      G4double e2 = points[bin+1];
      G4double d1 = data[bin];
      G4double d2 = data[bin+1];
      if(d1 > 0.0 && d2 > 0.0) {
          value = (std::log10(d1)*std::log10(e2/x) + std::log10(d2)*std::log10(x/e1)) / std::log10(e2/e1);
          value = std::pow(10.,value);
      } else {
          value = (d1*std::log10(e2/x) + d2*std::log10(x/e1)) / std::log10(e2/e1);
      }
  } else {
      value = data[nBins];
  }

  return value;
}
