#include "StackingAction.hh"
#include "G4Event.hh"
#include "G4Track.hh"
#include "G4VProcess.hh"
#include "G4Electron.hh"
#include "G4Gamma.hh"

G4ClassificationOfNewTrack StackingAction::ClassifyNewTrack(const G4Track *aTrack)
{
    G4ClassificationOfNewTrack classification = fUrgent;

    if (aTrack->GetCreatorProcess() != NULL)
    {
        if (aTrack->GetDefinition() == G4Electron::ElectronDefinition())
        {
            classification = fKill;
            return classification;
        }

        if (aTrack->GetCreatorProcess()->GetProcessName() == "RadioactiveDecay")
        {
            if (aTrack->GetDefinition() == G4Gamma::GammaDefinition())
            {
                runAction->lRun->fNGammasCreated++;
            }
        }
    }

    return classification;
}
