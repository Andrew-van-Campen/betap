#include "ActionInitialization.hh"
#include "PrimaryGeneratorAction.hh"
#include "SteppingAction.hh"
#include "StackingAction.hh"
#include "RunAction.hh"
#include "SourceWorldConstruction.hh"
#include "DetectorConstruction.hh"
#include "ApplicatorWorldConstruction.hh"

ActionInitialization::ActionInitialization(G4String fileN,
                                           ParallelWorldConstructionScoring *scoringW,
                                           SourceWorldConstruction *sourceW,
                                           DetectorConstruction *phant)
    : G4VUserActionInitialization(), fileName(fileN), scoringWorld(scoringW), sourceWorld(sourceW), egsphant(phant)
{
}

ActionInitialization::~ActionInitialization() {}

void ActionInitialization::BuildForMaster() const
{
    RunAction *run = new RunAction(fileName, egsphant, scoringWorld, sourceWorld);
    SetUserAction(run);
}

void ActionInitialization::Build() const
{
    PrimaryGeneratorAction *primGen = new PrimaryGeneratorAction();
    sourceWorld->addParticleGenerator(primGen);
    SetUserAction(primGen);

    RunAction *run = new RunAction(fileName, egsphant, scoringWorld, sourceWorld);
    SetUserAction(run);
    SetUserAction(new StackingAction(run));
}
