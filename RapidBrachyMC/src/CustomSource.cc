// Gabriel Famulari <gabriel.famulari@mail.mcgill.ca>

// Edited by Gabriel Famulari 2019-09-09 to add source orientation functionality
// Works with source orientation

#include <iostream>
#include <string>

#include "CustomSource.hh"
#include "SourceMaterial.hh"
#include "DetectorConstruction.hh"
#include "PositionList.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"

#include "G4Sphere.hh"
#include "G4Orb.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Ellipsoid.hh"

#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

#include "G4VisAttributes.hh"

#include "ControlPoint.hh"

using namespace std;

CustomSource::CustomSource(G4String core) 
{
  pMaterial = new SourceMaterial();
  SetupDimensions();
  fCore = core;
}

CustomSource::CustomSource() 
{
  pMaterial = new SourceMaterial();
  SetupDimensions();
  fPositionFilename = "";
  fCore = "G4_Yb";
}

CustomSource::~CustomSource() 
{
  delete pMaterial;
}

void CustomSource::SetCore(G4String core)
{
    fCore = core;
}

void CustomSource::SetupDimensions() 
{
  // Active core
  sourceRadius = 0.2*mm;
  halfSourceHeight = 1.6*mm;

  // Active core parameters
  coreRadius = sourceRadius;
  coreHalfZ = halfSourceHeight;

  // Capsule
  innerCyl1CapsuleRadius = 0.2*mm;
  outerCyl1CapsuleRadius = 0.3*mm;
  heightCyl1CapsuleHalf = 1.6*mm;

  // Extra capsule cylinder near the cable
  innerCyl2CapsuleRadius = 0.0*mm;
  outerCyl2CapsuleRadius = 0.3*mm;
  heightCyl2CapsuleHalf = 0.05*mm;
 
  // Capsule cone near cable
  heightCapsuleTop = 0.2*mm;

  // Spherical capsule tip
  innerMinConCapsuleRadius = 0.0*mm;
  outerMinConCapsuleRadius = 0.2*mm;
  innerMaxConCapsuleRadius = 0.0*mm;
  outerMaxConCapsuleRadius = 0.3*mm;
  heightConCapsuleHalf = 0.1*mm;

  // Cable
  cableRadius = 0.23*mm;
  halfCableHeight = 1.0*mm;

  startPhi = 0.;
  spanPhi = 2. * CLHEP::pi;
  spanTheta = CLHEP::pi/2.;
}

void CustomSource::createSource() 
{
  G4Material* steel = pMaterial->GetMat("Steel");
  G4Material* coreMaterial = pMaterial->GetMat("Ytterbium oxide");

  //Core
  G4Tubs* source = new G4Tubs("source", 0.0*mm, sourceRadius, halfSourceHeight, startPhi, spanPhi);

  source_log = new G4LogicalVolume(source, coreMaterial, "source_log");
  G4VisAttributes* source_att = new G4VisAttributes();
  source_att->SetColor(1, 0, 0);
  source_att->SetVisibility(true);
  source_att->SetForceSolid(true);
  source_log->SetVisAttributes(source_att);

  // Steel Capsule
  G4Tubs* capsuleCyl1 = new G4Tubs("capsuleCyl1", innerCyl1CapsuleRadius, outerCyl1CapsuleRadius, heightCyl1CapsuleHalf, startPhi, spanPhi);
  G4Tubs* capsuleCyl2 = new G4Tubs("capsuleCyl2", innerCyl2CapsuleRadius, outerCyl2CapsuleRadius, heightCyl2CapsuleHalf, startPhi, spanPhi);
  G4Cons* capsuleCone  = new G4Cons("capsuleCone", innerMinConCapsuleRadius, outerMinConCapsuleRadius,
            innerMaxConCapsuleRadius, outerMaxConCapsuleRadius, heightConCapsuleHalf, startPhi, spanPhi);
  G4Ellipsoid* capsEllipsoid = new G4Ellipsoid("capsEllipsoid", outerCyl1CapsuleRadius, outerCyl1CapsuleRadius, heightCapsuleTop, 0.0*mm, heightCapsuleTop);
  G4VSolid* capsuleHalf1 = new G4UnionSolid("capsuleHalf1", capsuleCyl1, capsuleCyl2, 0, G4ThreeVector(0.0, 0.0, -(heightCyl1CapsuleHalf + heightCyl2CapsuleHalf)));
  G4VSolid* capsuleHalf2 = new G4UnionSolid("capsuleHalf2", capsuleHalf1, capsuleCone, 0, G4ThreeVector(0.0, 0.0,
                              -(heightCyl1CapsuleHalf + 2*heightCyl2CapsuleHalf + heightConCapsuleHalf)));
  G4VSolid* capsule = new G4UnionSolid("capsule", capsuleHalf2, capsEllipsoid, 0, G4ThreeVector(0.0, 0.0, heightCyl1CapsuleHalf));

  capsule_log = new G4LogicalVolume(capsule, steel, "capsule_log");
  G4VisAttributes* capsule_att = new G4VisAttributes();
  capsule_att->SetColor(0.5, 0.5, 0.5);
  capsule_att->SetVisibility(true);
  capsule_att->SetForceSolid(false);
  capsule_log->SetVisAttributes(capsule_att);

  // Steel Cable
  G4Tubs* cable = new G4Tubs("cable", 0.0*mm , cableRadius, halfCableHeight, startPhi, spanPhi);
  
  cable_log = new G4LogicalVolume(cable, steel, "cable_log");
  G4VisAttributes* cable_att = new G4VisAttributes();
  cable_att->SetColor(0, 0, 1);
  cable_att->SetVisibility(true);
  cable_att->SetForceSolid(true);
  cable_log->SetVisAttributes(cable_att);
}

void CustomSource::placeSource(ControlPoint cpt, G4VPhysicalVolume *mother)
{
    for (size_t i = 0; i < cpt.dwellPositions.size(); i++)
    {
        G4ThreeVector shift = cpt.dwellPositions[i].pos;
        G4double angle = cpt.dwellPositions[i].angle;
    // G4cout << "Source: " << "i: " <<i<< "  Position: " << shift << " angle: " << angle << G4endl;

        G4ThreeVector cableOffset = G4ThreeVector(0, 0, -halfSourceHeight - 2 * heightCyl2CapsuleHalf - 2. * heightConCapsuleHalf - halfCableHeight);

        // Direction Vector of Dwell position
        G4ThreeVector Zunit = G4ThreeVector(0.0,0.0,1.0); //Source Is Originally defined pointing in +Z
        G4ThreeVector V = G4ThreeVector(cpt.dwellPositions[i].rot[0],
                                        cpt.dwellPositions[i].rot[1],
                                        cpt.dwellPositions[i].rot[2]).unit();

        // Angle Between V and +Z Axis
        G4double Vangle = Zunit.angle(V);

        // Axis for which rotation (Vangle) will be used to define rotation matrix
        G4ThreeVector Vcross = G4ThreeVector(0,0,0);

        // Check if V Parallel to Z
        if (V.isParallel(Zunit, 1E-4)) // IF parallel to Z-axis
        {
          G4cout << "*** HowParallel: " << V.howParallel(Zunit) << G4endl;

          if (fabs(Vangle - 3.14159) < 1E-4) // If Angle = Pi then Vcross = -X
          {
            Vcross = G4ThreeVector(-1.0,0.0,0.0); // -X
            G4cout << "WARNING: Source Orientation Parallel with NEGATIVE Z axis" << G4endl;
          }
          else // ASSUMPTION! If Angle == 0
          {
            Vcross = G4ThreeVector(1.0,0.0,0.0); // +X
            G4cout << "WARNING: Source Orientation Parallel with POSITIVE Z axis" << G4endl;
          }
        }
        else // IF NOT parallel to Z-axis
        {
          Vcross = V.cross(Zunit).unit();
        }

        // DEFINE ROTATION MATRIX To line up source with direction vector V
        G4RotationMatrix * Vrot = new G4RotationMatrix();
        Vrot->rotate(Vangle, Vcross);
        Vrot->invert();

        // Placing Components ////
        G4cout << "Placing source at: " << shift  << G4endl;
        G4cout << "Source Orientation Vector:" << V << G4endl;

        // *** G4PVPplacement using G4Transform3D to apply translate/rotation in mother frame of reference
        // Source
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift), "source",
                                                        this->source_log, mother, false, i));
        // Capsule
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift), "capsule",
                                                        this->capsule_log, mother, false, i));
        // Cable
        cableOffset.transform(*Vrot); // Transform cable shift in rotated frame of reference
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift+cableOffset), "cable",
                                                        this->cable_log, mother, false, i));
    }

    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}

void CustomSource::cleanSource() 
{
    for (size_t i = 0; i < this->placedObjects.size(); i++)
    {
        delete this->placedObjects[i];
    }
    this->placedObjects.clear();
    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}