#include "ParallelWorldMessenger.hh"
#include "ParallelWorldConstructionScoring.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"

ParallelWorldMessenger::ParallelWorldMessenger(ParallelWorldConstructionScoring *parWorld)
    : G4UImessenger(), fParWorld(parWorld)
{
    fParWorldDir = new G4UIdirectory("/parallel_world/");
    fParWorldDir->SetGuidance("Parallel World.");

    fphantCmd = new G4UIcmdWithAString("/parallel_world/phantom", this);
    fphantCmd->SetGuidance("Select egsphant file for material and density information");
    fphantCmd->SetParameterName("tPhantom", false);
    fphantCmd->AvailableForStates(G4State_PreInit);
    fphantCmd->SetToBeBroadcasted(false);

    fAkPerHistoryCmd = new G4UIcmdWithADouble("/parallel_world/ak_per_history", this);
    fAkPerHistoryCmd->SetGuidance("Specify the air kerma per history normalization constant");
    fAkPerHistoryCmd->SetParameterName("tAkPerHistory", false);
    fAkPerHistoryCmd->AvailableForStates(G4State_PreInit);
    fAkPerHistoryCmd->SetToBeBroadcasted(false);

    fRefAkCmd = new G4UIcmdWithADouble("/parallel_world/ref_ak", this);
    fRefAkCmd->SetGuidance("Specify the reference air kerma rate for the source.");
    fRefAkCmd->SetParameterName("tAkPerHistory", false);
    fRefAkCmd->AvailableForStates(G4State_PreInit);
    fRefAkCmd->SetToBeBroadcasted(false);

    fTotalTimeCmd = new G4UIcmdWithADouble("/parallel_world/total_time", this);
    fTotalTimeCmd->SetGuidance("Specify the total irradiation time of the sources.");
    fTotalTimeCmd->SetParameterName("tTotalTime", false);
    fTotalTimeCmd->AvailableForStates(G4State_PreInit);
    fTotalTimeCmd->SetToBeBroadcasted(false);

    fHCmd = new G4UIcmdWithADouble("/parallel_world/H", this);
    fHCmd->SetGuidance("Specify the H factor of the sources.");
    fHCmd->SetParameterName("tH", false);
    fHCmd->AvailableForStates(G4State_PreInit);
    fHCmd->SetToBeBroadcasted(false);

    fTrackLengthCmd = new G4UIcmdWithABool("/parallel_world/use_track_length_estimator", this);
    fTrackLengthCmd->SetGuidance("Specify between track length estimator or full MC.");
    fTrackLengthCmd->SetParameterName("tTrackLength", true);
    fTrackLengthCmd->AvailableForStates(G4State_PreInit);
    fTrackLengthCmd->SetToBeBroadcasted(false);
}

ParallelWorldMessenger::~ParallelWorldMessenger()
{
    delete fParWorldDir;
    delete fphantCmd;
    delete fAkPerHistoryCmd;
    delete fRefAkCmd;
    delete fTotalTimeCmd;
    delete fHCmd;
    delete fTrackLengthCmd;
}

void ParallelWorldMessenger::SetNewValue(G4UIcommand *command, G4String newValue)
{
    if (command == fphantCmd)
    {
        fParWorld->setPhantFilename(newValue);
    }

    if (command == fAkPerHistoryCmd)
    {
        fParWorld->setAkPerHistory(fAkPerHistoryCmd->GetNewDoubleValue(newValue));
    }

    if (command == fRefAkCmd)
    {
        fParWorld->setRefAk(fRefAkCmd->GetNewDoubleValue(newValue));
    }

    if (command == fTotalTimeCmd)
    {
        fParWorld->setTotalTime(fTotalTimeCmd->GetNewDoubleValue(newValue));
    }

    if (command == fHCmd)
    {
        fParWorld->setH(fHCmd->GetNewDoubleValue(newValue));
    }

    if (command == fTrackLengthCmd)
    {
        fParWorld->setTrackLength(fTrackLengthCmd->GetNewBoolValue(newValue));
    }
}
