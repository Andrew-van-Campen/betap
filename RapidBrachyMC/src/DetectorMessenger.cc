#include "DetectorMessenger.hh"
#include "DetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"

DetectorMessenger::DetectorMessenger(DetectorConstruction *Det) : detector(Det)
{
    detectorDir = new G4UIdirectory("/world/");
    detectorDir->SetGuidance(" world control.");

    fphantCmd = new G4UIcmdWithAString("/world/phantom", this);
    fphantCmd->SetGuidance("Select egsphant file for material and density information");
    fphantCmd->SetParameterName("tPhantom", false);
    fphantCmd->AvailableForStates(G4State_PreInit);
    fphantCmd->SetToBeBroadcasted(false);
}

DetectorMessenger::~DetectorMessenger()
{
    delete detectorDir;
    delete fphantCmd;
}

void DetectorMessenger::SetNewValue(G4UIcommand *command, G4String newValue)
{
    if (command == fphantCmd)
    {
        detector->setPhantFilename(newValue);
    }
}
