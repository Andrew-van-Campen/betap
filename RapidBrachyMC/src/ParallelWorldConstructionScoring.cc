#include <string>
#include <vector>

#include "G4Material.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"

#include "G4PVPlacement.hh"
#include "G4PVParameterised.hh"
#include "G4PhantomParameterisation.hh"

#include "G4VisAttributes.hh"

#include "G4SDManager.hh"
#include "G4MultiFunctionalDetector.hh"

#include "G4SystemOfUnits.hh"
#include "G4VUserParallelWorld.hh"
#include "G4VPrimitiveScorer.hh"

#include "DetectorConstruction.hh"
#include "ParallelWorldConstructionScoring.hh"
#include "ParallelWorldMessenger.hh"

#include "G4PSDoseDeposit.hh"
#include "TrackLengthEstimator.hh"
#include "TLEDoseToWaterInMedium.hh"
#include "Phantom.hh"

#include "G4Version.hh"


ParallelWorldConstructionScoring::ParallelWorldConstructionScoring(G4String &parallelWorldName, DetectorConstruction *phant)
    : G4VUserParallelWorld(parallelWorldName)
{
    fMessenger = new ParallelWorldMessenger(this);
    phantomFilename = "";
    fAkPerHistory = 1.0;
    fRefAk = 1.0;
    fTotalTime = 1.0;
    fH = 1.0;
    fUseTrackLength = true;

    egsphant = phant;
    scoringInitialized = false;
}

void ParallelWorldConstructionScoring::setPhantFilename(G4String filename)
{
    phantomFilename = filename;
}

void ParallelWorldConstructionScoring::setAkPerHistory(G4double akPerHistory)
{
    fAkPerHistory = akPerHistory;
}

void ParallelWorldConstructionScoring::setRefAk(G4double refAk)
{
    fRefAk = refAk;
}

void ParallelWorldConstructionScoring::setTotalTime(G4double totalTime)
{
    fTotalTime = totalTime;
}

void ParallelWorldConstructionScoring::setH(G4double H)
{
    fH = H;
}

void ParallelWorldConstructionScoring::setTrackLength(G4bool trackStatus)
{
    fUseTrackLength = trackStatus;
}

void ParallelWorldConstructionScoring::Construct()
{
    if (egsphant->getPhantomFilename().size() == 0)
        return;

    G4VPhysicalVolume *ghostWorldS = GetWorld();
    G4LogicalVolume *worldLogicalS = ghostWorldS->GetLogicalVolume();
    worldLogicalS->SetMaterial(NULL);

    XMin = egsphant->getXMin();
    YMin = egsphant->getYMin();
    ZMin = egsphant->getZMin();

    XMax = egsphant->getXMax();
    YMax = egsphant->getYMax();
    ZMax = egsphant->getZMax();

    NumBinX = egsphant->getNumVoxelsX();
    NumBinY = egsphant->getNumVoxelsY();
    NumBinZ = egsphant->getNumVoxelsZ();

    G4double X = XMax - XMin;
    G4double Y = YMax - YMin;
    G4double Z = ZMax - ZMin;

    G4double centerX = XMin + (X / 2.);
    G4double centerY = YMin + (Y / 2.);
    G4double centerZ = ZMin + (Z / 2.);;

    G4double halfSizeX = X / 2.;
    G4double halfSizeY = Y / 2.;
    G4double halfSizeZ = Z / 2.;

    halfVoxelDimensionX = halfSizeX / NumBinX;
    halfVoxelDimensionY = halfSizeY / NumBinY;
    halfVoxelDimensionZ = halfSizeZ / NumBinZ;

    std::vector<G4Material *> mats;
    // Since this is a massless world, set all voxels to point to a NULL
    // material pointer.
    mats.push_back(NULL);

    //Build a scoring Box as big as your patient, to later fill it with replicas of the same size
    G4Box *PatientScoringBox = new G4Box("PatientScoringBox", halfSizeX, halfSizeY, halfSizeZ);
    G4LogicalVolume *PatientScoringBoxLV = new G4LogicalVolume(PatientScoringBox, 0, "PatientScoringBoxLV");
    G4VPhysicalVolume *PatientScoringBoxPV = new G4PVPlacement(0,
                                                               G4ThreeVector(centerX, centerY, centerZ),
                                                               PatientScoringBoxLV,
                                                               "PatientScoringBoxPV",
                                                               worldLogicalS, false, 0);

    PatientScoringBoxLV->SetVisAttributes(G4VisAttributes::Invisible);

    G4Box *phantomVoxel_vol = new G4Box("phantomVoxel_vol", halfVoxelDimensionX, halfVoxelDimensionY, halfVoxelDimensionZ);
    G4LogicalVolume *phantomVoxel_log = new G4LogicalVolume(phantomVoxel_vol, 0, "phantomVoxel_log", 0, 0, 0);

    G4PhantomParameterisation *param = new G4PhantomParameterisation();
    param->SetVoxelDimensions(halfVoxelDimensionX, halfVoxelDimensionY, halfVoxelDimensionZ);
    param->SetNoVoxel(NumBinX, NumBinY, NumBinZ);
    param->SetMaterials(mats);

    // Massless world needs no material indices, G4PhantomParameterisation
    // defaults to index 0 of mats array if none are provided.
    // param->SetMaterialIndices(materialIds);
    param->SetSkipEqualMaterials(false);
    param->BuildContainerSolid(PatientScoringBoxPV);
    param->CheckVoxelsFillContainer(PatientScoringBox->GetXHalfLength(), PatientScoringBox->GetYHalfLength(), PatientScoringBox->GetZHalfLength());

    // If Geant4 version >= 10.02.p03  use kDefined
    // If 10.02.p02 or less use kXAxis
    #if G4VERSION_NUMBER >= 1023
      G4PVParameterised *phantom_phys = new G4PVParameterised("phantom_phys", phantomVoxel_log, PatientScoringBoxLV, kUndefined, param->GetNoVoxel(), param);
    #else
      G4PVParameterised *phantom_phys = new G4PVParameterised("phantom_phys", phantomVoxel_log, PatientScoringBoxLV, kXAxis, param->GetNoVoxel(), param);
      phantom_phys->SetRegularStructureId(1); // This needs to be used if using kXAxis or kYAxis
    #endif

    this->scoringInitialized = true;

    //ConstructSDandField();
}

void ParallelWorldConstructionScoring::ConstructSD()
{
    G4SDManager *SDman = G4SDManager::GetSDMpointer();
    G4String phantomSDname = "PhantomSD";

    G4MultiFunctionalDetector *MFDet = new G4MultiFunctionalDetector(phantomSDname);
    SDman->AddNewDetector(MFDet);

    TrackLengthEstimator *scorer;
    scorer = new TrackLengthEstimator("TotalDoseM", 0, egsphant);
    scorer->loadMuens(egsphant->getMatNames());
    MFDet->RegisterPrimitive(scorer);

    TLEDoseToWaterInMedium *DWMscorer;
    DWMscorer = new TLEDoseToWaterInMedium("DosetoWInM", 0, egsphant);
    MFDet->RegisterPrimitive(DWMscorer);

    if (this->scoringInitialized)
    {
        SetSensitiveDetector("phantomVoxel_log", MFDet);
    }
}
