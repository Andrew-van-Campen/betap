#include "SourceWorldMessenger.hh"
#include "SourceWorldConstruction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"

SourceWorldMessenger::SourceWorldMessenger(SourceWorldConstruction *sourceWorld)
    : G4UImessenger(), fSourceWorld(sourceWorld)
{
    sourceWorldDir = new G4UIdirectory("/source_world/");
    sourceWorldDir->SetGuidance("source World.");

    sourceCmd = new G4UIcmdWithAString("/source_world/switch", this);
    sourceCmd->SetGuidance("Choose among the following source geometries.");
    sourceCmd->SetParameterName("choice", true);
    sourceCmd->SetDefaultValue(" ");
    sourceCmd->SetCandidates("FlexiNoShield MicroSelectronV2 FlexiSource MicroSelectronV2Shielded Intrabeam BebigHdrIr GenericHDR CustomSource CustomSourceShielded GammaMedPlus");
    sourceCmd->AvailableForStates(G4State_PreInit);

    coreCmd = new G4UIcmdWithAString("/source_world/coreMaterial", this);
    coreCmd->SetGuidance("Choose among the selected source geometries.");
    coreCmd->SetParameterName("choice", true);
    coreCmd->SetDefaultValue(" ");
    coreCmd->SetCandidates("G4_Ir G4_Gd G4_Se G4_Pd G4_I G4_Yb");
    coreCmd->AvailableForStates(G4State_PreInit);

    coreACmd = new G4UIcmdWithAnInteger("/source_world/core/A", this);
    coreACmd->SetGuidance("Set the atomic mass");
    coreACmd->SetParameterName("nhist", true);
    coreACmd->SetDefaultValue(192);
    coreACmd->AvailableForStates(G4State_PreInit);

    coreZCmd = new G4UIcmdWithAnInteger("/source_world/core/Z", this);
    coreZCmd->SetGuidance("Set the atomic number");
    coreZCmd->SetParameterName("nhist", true);
    coreZCmd->SetDefaultValue(74);
    coreZCmd->AvailableForStates(G4State_PreInit);

    shieldRadiusCmd = new G4UIcmdWithADoubleAndUnit("/source_world/shieldRadius", this);
    shieldRadiusCmd->SetGuidance("Set shield radius.");
    shieldRadiusCmd->SetParameterName("tShieldRadius", true);
    shieldRadiusCmd->AvailableForStates(G4State_PreInit);
}

SourceWorldMessenger::~SourceWorldMessenger()
{
    delete sourceWorldDir;
    delete sourceCmd;
    delete coreCmd;
    delete shieldRadiusCmd;
    delete coreACmd;
    delete coreZCmd;
}

void SourceWorldMessenger::SetNewValue(G4UIcommand *command, G4String newValue)
{
    if (command == sourceCmd)
    {
        fSourceWorld->selectSourceModel(newValue);
    }

    if (command == coreCmd)
    {
        fSourceWorld->selectSourceCore(newValue);
    }

    if (command == shieldRadiusCmd)
    {
        fSourceWorld->setShieldRadius(shieldRadiusCmd->GetNewDoubleValue(newValue));
    }

    if (command == coreACmd)
    {
        fSourceWorld->setCoreA(coreACmd->GetNewIntValue(newValue));
    }

    if (command == coreZCmd)
    {
        fSourceWorld->setCoreZ(coreZCmd->GetNewIntValue(newValue));
    }
}
