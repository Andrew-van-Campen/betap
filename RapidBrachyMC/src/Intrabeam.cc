// !!!!
// NEEDS TO BE UPDATED TO WORK WITH SOURCE ORIENTATION (See FLexiNoShield or MicroSelectronV2)
// !!!

#include <algorithm>
#include <iomanip>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>
// USER //
#include "Intrabeam.hh"
#include "SourceMaterial.hh"
#include "DetectorConstruction.hh"
#include "PositionList.hh"
// GEANT4 //
#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4Sphere.hh"
#include "G4RunManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4TransportationManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"

using namespace std;

Intrabeam::Intrabeam(G4String fileName)
{
    pMaterial = new SourceMaterial();
    SetupDimensions();
    fPositionFilename = fileName;
}

Intrabeam::Intrabeam()
{
    pMaterial = new SourceMaterial();
    SetupDimensions();
    fPositionFilename = "";
}

void Intrabeam::SetupDimensions()
{
}

Intrabeam::~Intrabeam()
{
    delete pMaterial;
}

void Intrabeam::SetPositionFilename(G4String fileName)
{
    fPositionFilename = fileName;
}

void Intrabeam::CreateSource(G4VPhysicalVolume *mother)
{
    G4Material *steel = pMaterial->GetMat("Steel");
    G4Material *bery = pMaterial->GetMat("G4_Be");
    G4Material *vacuum = pMaterial->GetMat("Galactic");
    // G4Material* air = pMaterial->GetMat("G4_AIR");
    G4Material *gold = pMaterial->GetMat("gold");

    G4Colour red(1.0, 0.0, 0.0);
    G4Colour magenta(1.0, 0.0, 1.0);
    G4Colour lblue(0.0, 0.0, .75);

    // VACUUM TUB
    vactub = new G4Tubs("VacuumTub", 0. * mm, 1.1 * mm, 9. * cm, 0, 360. * deg);
    vacLog = new G4LogicalVolume(vactub, vacuum, "VacuumLog");
    // vacPhys = new G4PVPlacement(0, G4ThreeVector(), "vacuumTub", vacLog, mother, false, 0, true);

    // XRS TUBE
    xrstube = new G4Tubs("XrsTube", 1.1 * mm, 1.6 * mm, 7.4 * cm, 0, 360. * deg);
    tubeLog = new G4LogicalVolume(xrstube, steel, "TubeLog");
    // tubePhys = new G4PVPlacement(0, G4ThreeVector(), "TubePhys", tubeLog, vacPhys, false, 0, true);

    // BERYLIUM SHAFT
    beShaft = new G4Tubs("BeTip", 1.1 * mm, 1.6 * mm, 1.44 * cm, 0, 360. * deg);
    shaftLog = new G4LogicalVolume(beShaft, bery, "BeShaftLog");
    // shaftPhys = new G4PVPlacement(0, G4ThreeVector(0,0,7.4*cm), "BeShaftPhys", shaftLog, vacPhys, false, 0, true);

    // BERYLIUM TIP
    beTip = new G4Sphere("BeTip", 1.1 * mm, 1.6 * mm, 0. * deg, 180. * deg, 90. * deg, 270. * deg);
    tipLog = new G4LogicalVolume(beTip, bery, "BeTipLog");
    // tipPhys = new G4PVPlacement(0, G4ThreeVector(0,0,8.84*cm), "BeTipPhys", tipLog, mother, false, 0, true);

    // GOLD TARGET
    goldTarget = new G4Sphere("GoldTarget", 1099.5 * um, 1100 * um, 0. * deg, 180. * deg, 90. * deg, 270. * deg);
    goldLog = new G4LogicalVolume(goldTarget, gold, "GoldLog");
    // goldPhys = new G4PVPlacement(0, G4ThreeVector(0,0,8.84*cm), "GoldPhys", goldLog, tipPhys, false, 0, true);

    simpleiodiumVisAtt = new G4VisAttributes(magenta);
    simpleiodiumVisAtt->SetVisibility(true);
    simpleiodiumVisAtt->SetForceSolid(true);
    tubeLog->SetVisAttributes(simpleiodiumVisAtt);

    simpleCapsuleVisAtt = new G4VisAttributes(red);
    simpleCapsuleVisAtt->SetVisibility(true);
    simpleCapsuleVisAtt->SetForceWireframe(true);
    shaftLog->SetVisAttributes(simpleCapsuleVisAtt);

    simpleCapsuleTipVisAtt = new G4VisAttributes(red);
    simpleCapsuleTipVisAtt->SetVisibility(true);
    simpleCapsuleTipVisAtt->SetForceSolid(true);
    tipLog->SetVisAttributes(simpleCapsuleTipVisAtt);

    if (fPositionFilename != "")
    {
        PositionList readPositionList;
        readPositionList.ReadDataFile(fPositionFilename);
        G4int numberOfSources = readPositionList.GetNumberOfSources();
        //G4cout << "Total number of sources: " << numberOfSources << G4endl;
        for (G4int SourceNumber = 1; SourceNumber <= numberOfSources; SourceNumber++)
        {
            G4ThreeVector shift = readPositionList.GetPosition(SourceNumber - 1);
	    //  G4cout << "Source position: " << SourceNumber << "  Position: " << shift << G4endl;

            // There's a memory leak here since every G4PVPlacement object pointer is being overwritten by the next one,
            // so they won't be able to be freed since we lose our reference to them.
            vacPhys = new G4PVPlacement(0, shift + G4ThreeVector(), "vacuumTub", vacLog, mother, false, SourceNumber);
            tubePhys = new G4PVPlacement(0, shift + G4ThreeVector(), "TubePhys", tubeLog, vacPhys, false, SourceNumber);
            shaftPhys = new G4PVPlacement(0, shift + G4ThreeVector(0, 0, 7.4 * cm), "BeShaftPhys", shaftLog, vacPhys, false, SourceNumber);
            tipPhys = new G4PVPlacement(0, shift + G4ThreeVector(0, 0, 8.84 * cm), "BeTipPhys", tipLog, mother, false, SourceNumber);
            goldPhys = new G4PVPlacement(0, shift + G4ThreeVector(0, 0, 0), "GoldPhys", goldLog, tipPhys, false, SourceNumber);
        }
    }
    else
    {
        G4int SourceNumber = 1;
        tubePhys = new G4PVPlacement(0, G4ThreeVector(), "TubePhys", tubeLog, vacPhys, false, SourceNumber);
        vacPhys = new G4PVPlacement(0, G4ThreeVector(), "vacuumTub", vacLog, mother, false, SourceNumber);
        shaftPhys = new G4PVPlacement(0, G4ThreeVector(0, 0, 7.4 * cm), "BeShaftPhys", shaftLog, vacPhys, false, SourceNumber);
        tipPhys = new G4PVPlacement(0, G4ThreeVector(0, 0, 8.84 * cm), "BeTipPhys", tipLog, mother, false, SourceNumber);
        goldPhys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), "GoldPhys", goldLog, tipPhys, false, SourceNumber);
    }
}

void Intrabeam::CleanSource()
{
  //G4cout << " Intrabeam is cleaned" << G4endl;
    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}
