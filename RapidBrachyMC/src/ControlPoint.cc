#include <string>
#include <fstream>
#include <vector>

#include "globals.hh"
#include "string_utils.hh"
#include "G4ThreeVector.hh"
#include "G4SystemOfUnits.hh"
#include "ControlPoint.hh"

ControlPoint::ControlPoint() {}

int ControlPoint::readCpt(std::ifstream &planFile)
{
    std::string buffer;
    std::vector<std::string> temp_vector;

    std::getline(planFile, buffer);
    if (buffer.find("Control Point") == std::string::npos)
        throw "Expected Control Point";

    std::getline(planFile, buffer);
    if (buffer.find("weight") == std::string::npos)
        throw "Expected weight, got:" + buffer;
    this->weight = std::stod(split(buffer, '=')[1]);

    std::getline(planFile, buffer);
    if (buffer.find("Dwell Position") == std::string::npos)
        throw "Expected Dwell Position, got:" + buffer;
    int num_dwells = std::stoi(split(buffer, ' ')[0]);

    for (int i = 0; i < num_dwells; i++)
    {
        std::getline(planFile, buffer);
        temp_vector = split(buffer, ',');
        if (temp_vector.size() != 7)
            throw "Invalid format for dwell position";

        DwellPosition dwell;
        dwell.pos[0] = std::stod(temp_vector[0]) * mm;
        dwell.pos[1] = std::stod(temp_vector[1]) * mm;
        dwell.pos[2] = std::stod(temp_vector[2]) * mm;

        dwell.rot[0] = std::stod(temp_vector[3]);
        dwell.rot[1] = std::stod(temp_vector[4]);
        dwell.rot[2] = std::stod(temp_vector[5]);
        dwell.angle = std::stod(temp_vector[6]);
        this->dwellPositions.push_back(dwell);
    }

    return 0;
}
