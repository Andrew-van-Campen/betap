//////////////////////////////////////////////////////////////////////////
//
// Author: Shirin A. Enger <shirin@enger.se>
//
// License & Copyright
// ===================
//
// Copyright 2015 Shirin A Enger <shirin@enger.se>
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////



#include <iostream>
#include <string>

#include "GammaMedPlus.hh"
#include "SourceMaterial.hh"
#include "DetectorConstruction.hh"
#include "PositionList.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"

#include "G4Sphere.hh"
#include "G4Orb.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"

#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

#include "G4VisAttributes.hh"

#include "ControlPoint.hh"

using namespace std;

GammaMedPlus::GammaMedPlus(G4String core)
{
    pMaterial = new SourceMaterial();
    SetupDimensions();
    fCore = core;
}

GammaMedPlus::GammaMedPlus()
{
    pMaterial = new SourceMaterial();

    SetupDimensions();
    fPositionFilename = "";
    fCore = "G4_Ir";
}

GammaMedPlus::~GammaMedPlus()
{
    delete pMaterial;
}

void GammaMedPlus::SetCore(G4String core)
{
    fCore = core;
}

void GammaMedPlus::SetupDimensions()
{

  // Active core
  sourceRadius = 0.3 * mm;
  halfSourceHeight = 1.75 * mm;

  //NEEDED FOR PrimaryGeneratorAction (FOR ANY SOURCE!)
  coreRadius = sourceRadius;
  coreHalfZ = halfSourceHeight;

  //AirTube Above the Active Core
  airRadius = 0.35 * mm;
  halfAirHeight = 1.8 *mm;

  //Capsule part over the core region
  capsuleInnerRadius = airRadius;
  capsuleOuterRadius = 0.45 * mm;
  capsuleHalfHeight = halfSourceHeight;

  //Capsule part over the air only region
  //capsuleInnerAirRadius = airRadius;
  capsuleOuterAirRadius = capsuleOuterRadius;
  capsuleHalfAirHeight  = 0.05 * mm;

  // Extra capsule cylinder between core and cable
  //capsuleHalfFrontHeight = 0.25 * mm;
  capsuleBackRadius = capsuleOuterRadius;
  capsuleHalfBackHeight = 0.15 * mm;

  // Extra capsule cylinder between core and conical tip
  capsuleFrontRadius = capsuleOuterRadius;
  capsuleHalfFrontHeight = 0.25 * mm;

  // Conical capsule tip
  capsuleTipMinConRadius = 0.0 * mm;
  capsuleTipMaxConRadius = capsuleOuterRadius;//0.45 * mm;
  capsuleTipHalfHeight = 0.06 * mm;

  //Cable
  cableRadius = 0.45 * mm;
  halfCableHeight = 2.5 * mm;

  startPhi = 0.;
  spanPhi = 2. * CLHEP::pi;
  spanTheta = CLHEP::pi / 2.;

}

void GammaMedPlus::createSource()
{
  G4Material *steel = pMaterial->GetMat("Steel");
  G4Material *air = pMaterial->GetMat("G4_AIR");
  G4Material *Pt = pMaterial->GetMat("G4_Pt");
  G4Material *coreMaterial = pMaterial->GetMat(fCore);

  // Source definition
  G4Tubs *source = new G4Tubs("source", 0.0 * mm, sourceRadius,
				halfSourceHeight, startPhi, spanPhi);
  source_log = new G4LogicalVolume(source, coreMaterial, "source_log");
  G4VisAttributes *source_att = new G4VisAttributes();
  source_att->SetColor(1, 0, 0);
  source_att->SetVisibility(true);
  source_att->SetForceSolid(true);
  source_log->SetVisAttributes(source_att);

  //Air Tube above the core
  G4Tubs *airTubeTmp = new G4Tubs("airTubeTmp", 0.0 * mm, airRadius,
			       halfAirHeight, startPhi, spanPhi);
  // Air Tube with Core carved out
  G4Tubs *sourceCarve = new G4Tubs("sourceCarve", 0.0 * mm, sourceRadius,
        halfSourceHeight+0.01*mm, startPhi, spanPhi);
  G4VSolid *airTube = new G4SubtractionSolid("airTube", airTubeTmp, sourceCarve, 0,
              G4ThreeVector(0.0, 0.0, -(capsuleHalfAirHeight+0.01*mm)));
  airTube_log = new G4LogicalVolume(airTube, air, "airTube_log");


  // Capsule part over the core
  G4Tubs *capsuleSolid = new G4Tubs("capsuleSolid", capsuleInnerRadius,
				    capsuleOuterRadius, capsuleHalfHeight,
				    startPhi, spanPhi);

  //Front Part of the Capsule over air only region
  G4Tubs *capsuleFrontSeg = new G4Tubs("capsuleFrontSeg", capsuleInnerRadius,
					 capsuleOuterAirRadius, capsuleHalfAirHeight,
					 startPhi, spanPhi);

  //Front Part of the Capsule closer to the cone tip
  G4Tubs *capsuleFrontSolid = new G4Tubs("capsuleFrontSolid", 0.0 * mm,
					 capsuleFrontRadius, capsuleHalfFrontHeight,
					 startPhi, spanPhi);

  //Back Part of the Capsule closer to the cable
  G4Tubs *capsuleBackSolid = new G4Tubs("capsuleBackSolid", 0.0 * mm,
					capsuleBackRadius, capsuleHalfBackHeight,
					startPhi, spanPhi);

  // Conical capsule tip
  G4Cons *capsuleFrontTip = new G4Cons("capsuleBackTip",
                                0.0 * mm, capsuleTipMaxConRadius,
				                        0.0 * mm, capsuleTipMinConRadius,
                                capsuleTipHalfHeight, startPhi, spanPhi);

  // Join capsule parts bit for bit
  G4VSolid *capsuleHalf1 = new G4UnionSolid("capsuleHalf1", capsuleSolid, capsuleFrontSeg, 0,
              G4ThreeVector(0.0, 0.0, (capsuleHalfHeight + capsuleHalfAirHeight)));

  G4VSolid *capsuleHalf2 = new G4UnionSolid("capsuleHalf2", capsuleHalf1, capsuleFrontSolid, 0,
					    G4ThreeVector(0.0, 0.0, (capsuleHalfHeight + 2*capsuleHalfAirHeight +capsuleHalfFrontHeight)));

  G4VSolid *capsuleHalf3 = new G4UnionSolid("capsuleHalf3", capsuleHalf2, capsuleFrontTip, 0,
					    G4ThreeVector(0.0, 0.0, (capsuleHalfHeight + 2*capsuleHalfAirHeight + 2*capsuleHalfFrontHeight + capsuleTipHalfHeight)));

  G4VSolid *capsule = new G4UnionSolid("capsule_log", capsuleHalf3, capsuleBackSolid, 0,
					    G4ThreeVector(0.0, 0.0, -(capsuleHalfHeight + capsuleHalfBackHeight)));

  capsule_log = new G4LogicalVolume(capsule, steel, "capsule_log");
  G4VisAttributes *capsule_att = new G4VisAttributes();
  capsule_att->SetColor(0.5, 0.5, 0.5);
  capsule_att->SetVisibility(true);
  capsule_att->SetForceSolid(false);
  capsule_log->SetVisAttributes(capsule_att);

  // Steel Cable
  G4Tubs *cable_vol = new G4Tubs("cable_vol", 0.0 * mm, cableRadius, halfCableHeight, startPhi, spanPhi);
  cable_log = new G4LogicalVolume(cable_vol, steel, "cable_log");
  G4VisAttributes *cable_att = new G4VisAttributes();
  cable_att->SetColor(0, 0, 1);
  cable_att->SetVisibility(true);
  cable_att->SetForceSolid(true);
  cable_log->SetVisAttributes(cable_att);
}

void GammaMedPlus::placeSource(ControlPoint cpt, G4VPhysicalVolume *mother)
{
    for (size_t i = 0; i < cpt.dwellPositions.size(); i++)
    {
        G4ThreeVector shift = cpt.dwellPositions[i].pos;
        //G4double angle = cpt.dwellPositions[i].angle; THIS IS UNUSED SINCE APPLICATOR ROTATION SHOULD BE TREATED AS is in MARC BRANCH
        // std::cout << "Source: " << "i: " <<i<< "  Position: " << shift << " angle: " << angle << std::endl;

        //G4double cableOffset = capsuleHalfHeight + 2 * capsuleHalfBackHeight + halfCableHeight;
        G4ThreeVector cableOffset = G4ThreeVector(0, 0, -(halfSourceHeight + 2 * capsuleHalfBackHeight + halfCableHeight));
        G4ThreeVector airOffset = G4ThreeVector(0, 0, capsuleHalfAirHeight);

        // Direction Vector of Dwell position
        G4ThreeVector Zunit = G4ThreeVector(0.0,0.0,1.0); //Source Is Originally defined pointing in +Z
        G4ThreeVector V = G4ThreeVector(cpt.dwellPositions[i].rot[0],
                                        cpt.dwellPositions[i].rot[1],
                                        cpt.dwellPositions[i].rot[2]).unit();

        // Angle Between V and +Z Axis
        G4double Vangle = Zunit.angle(V);

        // Axis for which rotation (Vangle) will be used to define rotation matrix
        G4ThreeVector Vcross = G4ThreeVector(0,0,0);

        // Check if V Parallel to Z
        if (V.isParallel(Zunit, 1E-4)) // IF parallel to Z-axis
        {
          std::cout << "*** HowParallel: " << V.howParallel(Zunit) << std::endl;

          if (fabs(Vangle - 3.14159) < 1E-4) // If Angle = Pi then Vcross = -X
          {
            Vcross = G4ThreeVector(-1.0,0.0,0.0); // -X
            std::cout << "WARNING: Source Orientation Parallel with NEGATIVE Z axis" << std::endl;
          }
          else // ASSUMPTION! If Angle == 0
          {
            Vcross = G4ThreeVector(1.0,0.0,0.0); // +X
            std::cout << "WARNING: Source Orientation Parallel with POSITIVE Z axis" << std::endl;
          }
        }
        else // IF NOT parallel to Z-axis
        {
          Vcross = V.cross(Zunit).unit();
        }

        // DEFINE ROTATION MATRIX To line up source with direction vector V
        G4RotationMatrix * Vrot = new G4RotationMatrix();
        Vrot->rotate(Vangle, Vcross);
        Vrot->invert();

        // Placing Components ////
        std::cout << "Placing source at: " << shift  << std::endl;
        std::cout << "Source Orientation Vector:" << V << std::endl;

        // *** G4PVPplacement using G4Transform3D to apply translate/rotation in mother frame of reference
        // Source

	//

        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift), "source",this->source_log, mother, false, i));
        airOffset.transform(*Vrot); // Transform Airtube shift in rotated frame of reference
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift+airOffset),"airTube",this->airTube_log, mother, false, i));

        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift), "capsule",this->capsule_log, mother, false, i));

        cableOffset.transform(*Vrot); // Transform cable shift in rotated frame of reference
        this->placedObjects.push_back(new G4PVPlacement(G4Transform3D(*Vrot, shift+cableOffset), "cable",this->cable_log, mother, false, i));
    }

    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}

void GammaMedPlus::cleanSource()
{
    for (size_t i = 0; i < this->placedObjects.size(); i++)
    {
        delete this->placedObjects[i];
    }
    this->placedObjects.clear();
    G4RunManager::GetRunManager()->GeometryHasBeenModified();
}
