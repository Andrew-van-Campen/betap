#ifndef ActionInitialization_H
#define ActionInitialization_H 1
#include "G4VUserActionInitialization.hh"
#include "ParallelWorldConstructionScoring.hh"
#include "SourceWorldConstruction.hh"
#include "DetectorConstruction.hh"
class ParallelWorldConstructionScoring;

class ActionInitialization : public G4VUserActionInitialization
{
  public:
    ActionInitialization(G4String, ParallelWorldConstructionScoring *, SourceWorldConstruction *, DetectorConstruction *);
    virtual ~ActionInitialization();

    virtual void BuildForMaster() const;
    virtual void Build() const;

  private:
    G4String fileName;
    ParallelWorldConstructionScoring *scoringWorld;
    SourceWorldConstruction *sourceWorld;
    DetectorConstruction *egsphant;
};

#endif
