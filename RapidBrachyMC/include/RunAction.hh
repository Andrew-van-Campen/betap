#ifndef RunAction_h
#define RunAction_h 1

#include <vector>

#include "G4UserRunAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "ParallelWorldConstructionScoring.hh"
#include "SourceWorldConstruction.hh"
#include "DetectorConstruction.hh"
#include "RunActionMessenger.hh"
#include "Run.hh"
#include "G4THitsMap.hh"
#include "TreatmentPlan.hh"

class G4Run;
class RunActionMessenger;
class ParallelWorldConstructionScoring;

class RunAction : public G4UserRunAction
{
  public:
    RunAction(G4String, DetectorConstruction *, ParallelWorldConstructionScoring *scoring, SourceWorldConstruction *source);
    ~RunAction();

  public:
    virtual G4Run *GenerateRun();
    void BeginOfRunAction(const G4Run *);
    void EndOfRunAction(const G4Run *);
    void setDoseFormat(G4String);
    void outputDose();
    void startSimulation(G4int nhist);
    void updateProgress(G4int, G4int);
    void loadTreatmentPlan(G4String fileName);

    ParallelWorldConstructionScoring *GetPar() { return scoringPar; }

    static G4int GetNumEvents() { return numEvents; }
    static G4int getRunCounter() { return runIDcounter; }

    G4double calculateNorm();
    Run *lRun;

  private:
    RunActionMessenger *runActionMessenger;

    G4String inputFilename;
    G4String planFilename;
    G4String progressFilename;
    G4String ending;
    G4String outputMode;
    static G4int numEvents;
    static G4int runIDcounter;

    DetectorConstruction *egsphant;
    ParallelWorldConstructionScoring *scoringPar;
    SourceWorldConstruction *sourceWorld;

    TreatmentPlan *treatmentPlan;
    G4double uncert_threshold;

    G4int nGammas;
    std::vector<SimDose*> doseCollections;
    G4long nHistories;
};

#endif
