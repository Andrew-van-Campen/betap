// Copyright 2015 Marc-Andre Renaud <marc-andre.renaud@mail.mcgill.ca>

#ifndef MicroSelectronV2Shielded_H
#define MicroSelectronV2Shielded_H 1
#include "SourceMaterial.hh"

#include "G4Cons.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4TransportationManager.hh"
#include "G4VisAttributes.hh"
#include "SourceModel.hh"
#include "ControlPoint.hh"

class G4LogicalVolume;
class G4Tubs;
class G4Box;
class G4Sphere;
class G4VPhysicalVolume;
class G4VisAttributes;
class SourceModel;

class MicroSelectronV2Shielded : public SourceModel
{
  public:
    explicit MicroSelectronV2Shielded(G4String, G4double);
    MicroSelectronV2Shielded();
    ~MicroSelectronV2Shielded();

    void createSource();
    void placeSource(ControlPoint, G4VPhysicalVolume *);
    void cleanSource();

    void SetCore(G4String);
    void SetPositionFilename(G4String fileName);
    void SetupDimensions();
    void SetupPositions();

    G4ThreeVector getSourceOffset();

  private:
    G4double sourceRadius, halfSourceHeight;

    G4double sourceTipMinConRadius, sourceTipMaxConRadius, sourceTipHalfHeight;

    G4double capsuleInnerRadius, capsuleOuterRadius, capsuleHalfHeight;

    G4double capsuleHalfExtraHeight, capsuleTipMinConRadius;
    G4double capsuleTipMaxConRadius, capsuleTipHalfHeight;

    G4double cableRadius, halfCableHeight, startPhi, spanPhi, spanTheta;

    G4double shieldRadius, halfShieldHeight, halfExtraShieldHeight;
    G4double rectangleWidth, rectangleHeight, rectangleLength;

    G4double sourceOffsetY;

    SourceMaterial *pMaterial;

    G4int numberOfSources;

    G4String fPositionFilename;
    G4String fCore;

    G4LogicalVolume *source_log;
    G4LogicalVolume *shield_log;
    G4LogicalVolume *capsule_log;
    G4LogicalVolume *cable_log;
};
#endif
