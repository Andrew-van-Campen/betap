#ifndef Intrabeam_H
#define Intrabeam_H 1
#include "SourceMaterial.hh"
// GEANT4 //
#include "G4Cons.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4TransportationManager.hh"
#include "G4VisAttributes.hh"
#include "SourceModel.hh"

class G4LogicalVolume;
class G4Tubs;
class G4Box;
class G4Sphere;
class G4VPhysicalVolume;
class G4VisAttributes;
class SourceModel;

class Intrabeam : public SourceModel
{
  public:
    explicit Intrabeam(G4String);
    Intrabeam();
    ~Intrabeam();

    void CreateSource(G4VPhysicalVolume *);
    void CleanSource();

    void SetPositionFilename(G4String fileName);
    void SetupDimensions();

  private:
    G4Tubs *xrstube;
    G4LogicalVolume *tubeLog;
    G4VPhysicalVolume *tubePhys;
    G4Sphere *beTip;
    G4LogicalVolume *tipLog;
    G4VPhysicalVolume *tipPhys;
    G4Tubs *beShaft;
    G4LogicalVolume *shaftLog;
    G4VPhysicalVolume *shaftPhys;
    G4Sphere *goldTarget;
    G4LogicalVolume *goldLog;
    G4VPhysicalVolume *goldPhys;
    G4Tubs *vactub;
    G4LogicalVolume *vacLog;
    G4VPhysicalVolume *vacPhys;
    G4VisAttributes *simpleiodiumVisAtt;
    G4VisAttributes *simpleCapsuleVisAtt;
    G4VisAttributes *simpleCapsuleTipVisAtt;

    SourceMaterial *pMaterial;
    G4String fPositionFilename;
};
#endif
