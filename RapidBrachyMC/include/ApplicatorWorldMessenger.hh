#pragma once

// GEANT4 //
#include "globals.hh"
#include "G4UImessenger.hh"

class GUIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADouble;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3Vector;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWithoutParameter;

// BRACHYSOURCE //
class ApplicatorWorldConstruction;


class ApplicatorWorldMessenger : public G4UImessenger
{
  public:
    ApplicatorWorldMessenger(ApplicatorWorldConstruction* applicator_world);
    ~ApplicatorWorldMessenger();

    void SetNewValue(G4UIcommand* command, G4String value);

  private:
    ApplicatorWorldConstruction* _applicator_world;

    G4UIdirectory* _applicator_world_directory;

    G4UIcmdWithAString* _material_command;

    G4UIcmdWithADouble* _density_command;

    G4UIcmdWithADoubleAndUnit* _x_position_command;
    G4UIcmdWithADoubleAndUnit* _y_position_command;
    G4UIcmdWithADoubleAndUnit* _z_position_command;

    G4UIcmdWithADoubleAndUnit* _x_rotation_command;
    G4UIcmdWithADoubleAndUnit* _y_rotation_command;
    G4UIcmdWithADoubleAndUnit* _z_rotation_command;

    G4UIcmdWith3VectorAndUnit* _vertex_command;
    G4UIcmdWith3Vector* _face_command;

    G4UIcmdWithoutParameter* _done_command;
};

