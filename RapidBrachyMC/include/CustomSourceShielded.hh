// Copyright 2016 Gabriel Famulari <gabriel.famulari@mail.mcgill.ca>

#ifndef CustomSourceShielded_H
#define CustomSourceShielded_H 1
#include "SourceMaterial.hh"

#include "G4Cons.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4TransportationManager.hh"
#include "G4VisAttributes.hh"
#include "SourceModel.hh"
#include "ControlPoint.hh"

class G4LogicalVolume;
class G4Tubs;
class G4Box;
class G4Sphere;
class G4VPhysicalVolume;
class G4VisAttributes;
class SourceModel;

class CustomSourceShielded : public SourceModel {
public:
  explicit CustomSourceShielded(G4String, G4double);
  CustomSourceShielded();
  ~CustomSourceShielded();

  void createSource();
  void placeSource(ControlPoint, G4VPhysicalVolume*);
  void cleanSource();


  void SetCore(G4String);
  void SetPositionFilename(G4String fileName);
  void SetupDimensions();
  void SetupPositions();

  G4ThreeVector getSourceOffset();

private:
  G4double sourceRadius, halfSourceHeight;
  G4double coreRadius, coreHalfZ;

  G4double innerCyl1CapsuleRadius, outerCyl1CapsuleRadius, heightCyl1CapsuleHalf;

  G4double innerCyl2CapsuleRadius, outerCyl2CapsuleRadius, heightCyl2CapsuleHalf;

  G4double heightCapsuleTop;

  G4double innerMinConCapsuleRadius, outerMinConCapsuleRadius, innerMaxConCapsuleRadius, outerMaxConCapsuleRadius, heightConCapsuleHalf;

  G4double airGap;

  G4double cableRadius, halfCableHeight, startPhi, spanPhi, spanTheta;

  G4double shieldRadius, halfShieldHeight, halfExtraShieldHeight;
  G4double rectangleWidth, rectangleHeight, rectangleLength;

  G4double sourceOffsetY;

  SourceMaterial* pMaterial;

  G4int numberOfSources;

  G4String fPositionFilename;
  G4String fCore;

  G4LogicalVolume *source_log;
  G4LogicalVolume *shield_log;
  G4LogicalVolume *capsule_log;
  G4LogicalVolume *cable_log;
};
#endif
