// Gabriel Famulari <gabriel.famulari@mail.mcgill.ca>

// USER //
#ifndef GenericHDR_H
#define GenericHDR_H 1
#include "SourceMaterial.hh"
// GEANT4 //
#include "G4Cons.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4TransportationManager.hh"
#include "G4VisAttributes.hh"
#include "SourceModel.hh"
#include "ControlPoint.hh"

class G4LogicalVolume;
class G4Tubs;
class G4Box;
class G4Sphere;
class G4VPhysicalVolume;
class G4VisAttributes;
class SourceModel;

class GenericHDR : public SourceModel
{
  public:
    explicit GenericHDR(G4String);
    GenericHDR();
    ~GenericHDR();

    void createSource();
    void placeSource(ControlPoint, G4VPhysicalVolume *);
    void cleanSource();

    void SetCore(G4String);
    void SetPositionFilename(G4String fileName);
    void SetupDimensions();
    void SetupPositions();

  private:
    G4double sourceRadius, halfSourceHeight;

    G4double sourceTipMinConRadius, sourceTipMaxConRadius, sourceTipHalfHeight;

    G4double capsuleInnerRadius, capsuleOuterRadius, capsuleHalfHeight;

    G4double capsuleHalfExtraHeightFront, capsuleHalfExtraHeightBack;
    G4double capsuleTipMaxConRadius, capsuleTipHalfHeight;

    G4double cableRadius, halfCableHeight, startPhi, spanPhi, spanTheta;

    SourceMaterial *pMaterial;

    G4int numberOfSources;

    G4String fPositionFilename;
    G4String fCore;

    G4LogicalVolume *source_log;
    G4LogicalVolume *capsule_log;
    G4LogicalVolume *cable_log;
};
#endif
