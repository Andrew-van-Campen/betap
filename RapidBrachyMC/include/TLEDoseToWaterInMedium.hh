#ifndef TLEDoseToWaterInMedium_h
#define TLEDoseToWaterInMedium_h 1

#include "G4VPrimitiveScorer.hh"
#include "G4THitsMap.hh"
#include "DetectorConstruction.hh"

class TLEDoseToWaterInMedium : public G4VPrimitiveScorer
{
  public:
    TLEDoseToWaterInMedium(G4String name, G4int depth, DetectorConstruction *);
    ~TLEDoseToWaterInMedium();
    virtual void Initialize(G4HCofThisEvent *);
    virtual void clear();
    virtual void SetUnit(const G4String &unit);
  

  protected:
    virtual G4bool ProcessHits(G4Step *, G4TouchableHistory *);

  private:
    G4int FindBinLocation(G4double energy, std::vector<G4double>&);
    G4double linLogLogInterpolate(G4double x, G4int bin,
                                std::vector<G4double>& points,
                                std::vector<G4double>& data);
    G4int numMaterials;
    G4int firstevent;
  
    
    
    std::vector<G4double> energies;
    std::vector<G4double> mooValues;
    
    G4int HCID;
    G4THitsMap<G4double> *EvtMap;
  
    DetectorConstruction *egsphant;
};
#endif
