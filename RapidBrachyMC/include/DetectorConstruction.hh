#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"
#include "PatientMaterial.hh"
#include "Phantom.hh"

class DetectorMessenger;
class G4LogicalVolume;
class G4Material;
class G4Box;
class G4Colour;
class G4VPhysicalVolume;
class G4VPhysicalVolume;
class SourceMaterial;
class BrachySource;

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    explicit DetectorConstruction();
    ~DetectorConstruction();

    void setPhantFilename(G4String filename);
    G4VPhysicalVolume *Construct();
    void ConstructPhantom();
    void InitPhantom();
    void PrintDetectorParameters();

    G4String getPhantomFilename()
    {
        return phantomFilename;
    }

    G4double getXMin()
    {
        return XMin;
    };

    G4double getXMax()
    {
        return XMax;
    };

    G4double getYMin()
    {
        return YMin;
    };

    G4double getYMax()
    {
        return YMax;
    };

    G4double getZMin()
    {
        return ZMin;
    };

    G4double getZMax()
    {
        return ZMax;
    };

    G4int getNumVoxelsX()
    {
        return NumBinX;
    };

    G4int getNumVoxelsY()
    {
        return NumBinY;
    };

    G4int getNumVoxelsZ()
    {
        return NumBinZ;
    };

    G4double getPixelSpacingX()
    {
        return pixel_spacing_X;
    };

    G4double getPixelSpacingY()
    {
        return pixel_spacing_Y;
    };

    G4double getPixelSpacingZ()
    {
        return pixel_spacing_Z;
    };

    std::vector<G4String> getMatNames()
    {
        return matNames;
    }

    void getTopleft(G4double &x, G4double &y, G4double &z) const
    {
        x = XMin;
        y = YMin;
        z = ZMin;
    }

    void getNumVoxels(G4int &numX, G4int &numY, G4int &numZ) const
    {
        numX = NumBinX;
        numY = NumBinY;
        numZ = NumBinZ;
    }

    G4long getTotalVoxels()
    {
        return totalVox;
    }

    void getVoxelSize(G4double &sizeX, G4double &sizeY, G4double &sizeZ) const
    {
        sizeX = pixel_spacing_X;
        sizeY = pixel_spacing_Y;
        sizeZ = pixel_spacing_Z;
    }

    G4double getVoxelVolume() const
    {
        return pixel_spacing_X * pixel_spacing_Y * pixel_spacing_Z;
    }

    G4double* getDensities()
    {
        return phantom->getDensities();
    }

    size_t* getUniqueMatNumbers() {
        return phantom->getUniqueMatNumbers();
    }

  private:
    G4String phantomFilename;
    G4int SourceChoice;
    BrachySource *Source;
    Phantom *phantom;

    G4Box *World;
    G4LogicalVolume *WorldLog;
    G4VPhysicalVolume *WorldPhys;

    G4double worldSizeX;
    G4double worldSizeY;
    G4double worldSizeZ;

    DetectorMessenger *detectorMessenger;
    PatientMaterial *pMaterial;

    G4double pixel_spacing_X;
    G4double pixel_spacing_Y;
    G4double pixel_spacing_Z;
    PatientMaterial *mat;
    G4int NumBinX, NumBinY, NumBinZ;
    G4int totalVox;

    G4double XMax, XMin;
    G4double YMax, YMin;
    G4double ZMin, ZMax;

    std::vector<G4String> matNames;
    bool phantomLoaded;
};

#endif
