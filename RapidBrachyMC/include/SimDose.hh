#ifndef SimDose_H
#define SimDose_H 1

#include <vector>
#include "globals.hh"
#include "DetectorConstruction.hh"

class SimDose
{
    public:
      SimDose(size_t total_vox, bool include_temp = true);
      ~SimDose();

      void deposit_dose(G4double dose, size_t vox, G4long hist);
      void deposit_dose_event(G4double dose, size_t vox);
      void finalize_edep();
      void merge_with(SimDose* other_dose);
      void finalize_tally(G4long hist, G4double norm, DetectorConstruction *phant);
      void as_3ddose(G4String filename, DetectorConstruction *phant, G4double uncert_threshold);
      void as_minidos(G4String filename, DetectorConstruction *phant, G4double uncert_threshold);

      std::vector<G4double> dose;
      std::vector<G4double> dose_tmp;
      std::vector<G4double> uncert;
      std::vector<G4long> current_hist;
};

#endif
