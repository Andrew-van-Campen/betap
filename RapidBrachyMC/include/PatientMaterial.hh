#ifndef PatientMaterial_h
#define PatientMaterial_h 1

#include "globals.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4MaterialTable.hh"
#include "G4VisAttributes.hh"
#include "G4ios.hh"

class PatientMaterial
{
  public:
    PatientMaterial();
    ~PatientMaterial();

    size_t GetMatIndex(size_t TissueNumber, G4double density);
    G4int GetNumberOfMaterials();

    G4int CreateFor(G4String TissueName);
    std::vector<G4Material *> GetFlatMatVector();
    G4Material *GetMat(G4String);

  private:
    G4int NumberOfMaterials;
    std::vector<std::vector<G4Material *> > MatList;
    std::vector<G4double> MinDensityList;
    std::vector<G4double> MaxDensityList;
    std::vector<size_t> NbIncrementList;
};

#endif
