#ifndef Phantom_h
#define Phantom_h 1

#include "globals.hh"
#include "PatientMaterial.hh"
#include "G4Material.hh"
#include <vector>

class Phantom
{
  public:
    Phantom();
    ~Phantom();

    G4double getXmin();
    G4double getXmax();
    G4double getYmin();
    G4double getYmax();
    G4double getZmin();
    G4double getZmax();
    G4double getXsize();
    G4double getYsize();
    G4double getZsize();
    G4double getNumVoxelsX();
    G4double getNumVoxelsY();
    G4double getNumVoxelsZ();
    std::vector<G4String> getMatNames();
    size_t *getMatNumbers();
    size_t* getUniqueMatNumbers();
    G4double *getDensities();

    std::vector<G4Material *> getAllMaterials();

    G4String p_filename;
    int num_mats;

    void loadFromEGSphant(const G4String);

  private:
    void constructMaterials();
    G4int num_voxels[3];
    G4double voxel_size[3];
    G4double topleft[3];

    size_t *material_ids;
    size_t *real_ids;
    G4double *densities;
    std::vector<G4String> material_names;
    PatientMaterial *pMaterial;
};

#endif
