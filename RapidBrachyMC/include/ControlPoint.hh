#ifndef ControlPoint_H
#define ControlPoint_H 1

#include <string>
#include <fstream>
#include <vector>
#include "globals.hh"
#include "G4ThreeVector.hh"

typedef struct
{
    G4ThreeVector pos;
    G4ThreeVector rot;
    G4double angle;
} DwellPosition;

class ControlPoint
{
  public:
    ControlPoint();
    std::vector<DwellPosition> dwellPositions;
    G4double weight;

    int readCpt(std::ifstream &planFile);
};

#endif
