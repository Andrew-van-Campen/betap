
#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
//#include "PrimaryGeneratorActionMessenger.hh"
#include "ControlPoint.hh"
#include "SourceModel.hh"

class G4GeneralParticleSource;
class G4Event;

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    PrimaryGeneratorAction();
    ~PrimaryGeneratorAction();

    void GeneratePrimaries(G4Event *anEvent);
    void setControlPoint(ControlPoint, SourceModel *);

  private:
    G4GeneralParticleSource *particleGun;
};

#endif
