#ifndef SourceWorldMessenger_h
#define SourceWorldMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"
#include "SourceWorldConstruction.hh"

class G4UIdirectory;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;

class SourceWorldConstruction;

class SourceWorldMessenger : public G4UImessenger
{
  public:
    explicit SourceWorldMessenger(SourceWorldConstruction *);
    virtual ~SourceWorldMessenger();

    virtual void SetNewValue(G4UIcommand *, G4String);

  private:
    SourceWorldConstruction *fSourceWorld;
    G4UIdirectory *sourceWorldDir;
    G4UIcmdWithAString *sourceCmd;
    G4UIcmdWithAString *coreCmd;
    G4UIcmdWithADoubleAndUnit *shieldRadiusCmd;
    G4UIcmdWithAnInteger *coreACmd;
    G4UIcmdWithAnInteger *coreZCmd;
};

#endif
