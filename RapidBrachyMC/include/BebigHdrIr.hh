//////////////////////////////////////////////////////////////////////////
//
// Author: Shirin A. Enger <shirin@enger.se>
//
// License & Copyright
// ===================
//
// Copyright 2015 Shirin A Enger <shirin@enger.se>
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////

// USER //
#ifndef BebigHdrIr_H
#define BebigHdrIr_H 1
#include "SourceMaterial.hh"
// GEANT4 //
#include "G4Cons.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4TransportationManager.hh"
#include "G4VisAttributes.hh"
#include "SourceModel.hh"
#include "ControlPoint.hh"

class G4LogicalVolume;
class G4Tubs;
class G4Box;
class G4Sphere;
class G4VPhysicalVolume;
class G4VisAttributes;
class SourceModel;

class BebigHdrIr : public SourceModel
{
  public:
    explicit BebigHdrIr(G4String);
    BebigHdrIr();
    ~BebigHdrIr();

    void createSource();
    void placeSource(ControlPoint, G4VPhysicalVolume *);
    void cleanSource();

    void SetCore(G4String);
    void SetPositionFilename(G4String fileName);
    void SetupDimensions();
    void SetupPositions();

  private:
    //Source
    G4double sourceRadius, halfSourceHeight;
    //Capsule hollow part
    G4double capsuleInnerRadius,capsuleOuterRadius,capsuleHalfHeight;
    // Tip of capsule
    G4double capsuleTipMinConRadius,capsuleTipMaxConRadius,capsuleTipHalfHeight;
    //Extra capsule cylinder between the tip and where the source starts
    G4double capsuleSolidFrontInnerRadius,capsuleSolidFrontOuterRadius,capsuleSolidFrontHalfHeight;
    // Conical air capsule tip
    G4double capsuleTipAirMinConRadius,capsuleTipAirMaxConRadius,capsuleTipAirHalfHeight;
    // Extra capsule cylinder between core and cable
    G4double capsuleSolidBottomInnerRadius,capsuleSolidBottomOuterRadius,capsuleSolidBottomHalfHeight;
   

    G4double cableRadius, halfCableHeight, startPhi, spanPhi, spanTheta;

    SourceMaterial *pMaterial;

    G4int numberOfSources;

    G4String fPositionFilename;
    G4String fCore;

    G4LogicalVolume *source_log;
  G4LogicalVolume *capsule_log, *airGap_log;
    G4LogicalVolume *cable_log;
};
#endif
