#ifndef Run_h
#define Run_h 1

#include <vector>
#include "G4Run.hh"
#include "G4Event.hh"
#include "SimDose.hh"

class Run : public G4Run
{

  public:
    Run(std::vector<SimDose*> doseCollections, int numRuns);
    virtual ~Run();
    virtual void RecordEvent(const G4Event *);
    virtual void Merge(const G4Run *aRun);


    std::vector<SimDose*> doseCollections;

    int lnumRuns;
    G4int fNGammasCreated;
    long bla = 0;
};

#endif
