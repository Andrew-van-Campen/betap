
#ifndef SourceModel_h
#define SourceModel_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "ControlPoint.hh"
#include "G4PVPlacement.hh"

class G4VPhysicalVolume;

class SourceModel
{
  public:
    SourceModel();
    virtual ~SourceModel();

    virtual void createSource(){};
    virtual void placeSource(ControlPoint, G4VPhysicalVolume *){};
    virtual void cleanSource(){};

    virtual G4double getActiveCoreRadius()
    {
        return coreRadius;
    }

    virtual G4double getActiveCoreHalfZ()
    {
        return coreHalfZ;
    }

    virtual void setCoreA(G4int coreA)
    {
        isotopeA = coreA;
    }

    virtual void setCoreZ(G4int coreZ)
    {
        isotopeZ = coreZ;
    }

    virtual G4int getCoreA() { return isotopeA; }
    virtual G4int getCoreZ() { return isotopeZ; }

    virtual G4ThreeVector getSourceOffset() { return G4ThreeVector(); }

  protected:
    std::vector<G4PVPlacement *> placedObjects;
    G4int isotopeA;
    G4int isotopeZ;
    G4double coreRadius;
    G4double coreHalfZ;
};
#endif
