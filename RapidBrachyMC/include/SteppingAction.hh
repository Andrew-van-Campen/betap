#ifndef SteppingAction_h
#define SteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"

#include <fstream>

#include "G4Track.hh"
#include "G4Step.hh"
#include "G4StepStatus.hh"
#include "G4StepPoint.hh"
#include "G4TrackingManager.hh"

using namespace std;

class SteppingAction : public G4UserSteppingAction

{
  public:
    SteppingAction();

    ~SteppingAction(){};

    void UserSteppingAction(const G4Step *theStep);

  private:
};

#endif
