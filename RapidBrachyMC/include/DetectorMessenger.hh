#ifndef DetectorMessenger_h
#define DetectorMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class DetectorConstruction;
class G4UIdirectory;
class G4UIcmdWithAString;

class DetectorMessenger : public G4UImessenger
{
  public:
    explicit DetectorMessenger(DetectorConstruction *det);
    ~DetectorMessenger();

    void SetNewValue(G4UIcommand *, G4String);

  private:
    DetectorConstruction *detector;
    G4UIdirectory *detectorDir;
    G4UIcmdWithAString *fphantCmd;
};
#endif
