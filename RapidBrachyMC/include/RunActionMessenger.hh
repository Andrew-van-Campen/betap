#ifndef RunActionMessenger_h
#define RunActionMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"
#include "RunAction.hh"
#include "G4UIcmdWithAnInteger.hh"

class RunAction;
class G4UIdirectory;
class G4UIcmdWithAString;

class RunActionMessenger : public G4UImessenger
{
  public:
    explicit RunActionMessenger(RunAction *runAction);
    ~RunActionMessenger();

    void SetNewValue(G4UIcommand *, G4String);

  private:
    RunAction *runAction;
    G4UIdirectory *runActionDir;
    G4UIcmdWithAString *fDoseFormatcmd;
    G4UIcmdWithAString *treatmentPlanCmd;
    G4UIcmdWithAnInteger *simCmd;
};
#endif
