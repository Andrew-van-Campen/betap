#ifndef TreatmentPlan_H
#define TreatmentPlan_H 1

#include <string>
#include <fstream>
#include <vector>
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "ControlPoint.hh"

class TreatmentPlan
{
  public:
    TreatmentPlan(G4String);
    ~TreatmentPlan() {}

    int readPlanFile(G4String);
    std::vector<ControlPoint> controlPoints;

  private:
    G4String inputFile;
};

#endif
