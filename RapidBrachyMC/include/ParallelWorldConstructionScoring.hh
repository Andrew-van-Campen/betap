#ifndef ParallelWorldConstructionScoring_h
#define ParallelWorldConstructionScoring_h 1

#include <string>
#include <vector>

#include "globals.hh"

#include "G4VUserParallelWorld.hh"
#include "PatientMaterial.hh"
#include "SourceWorldConstruction.hh"
#include "ParallelWorldMessenger.hh"
#include "DetectorConstruction.hh"

class ParallelWorldMessenger;
class SourceWorldConstruction;

class ParallelWorldConstructionScoring : public G4VUserParallelWorld
{
  public:
    explicit ParallelWorldConstructionScoring(G4String &, DetectorConstruction *);

    ~ParallelWorldConstructionScoring() {}
    virtual void Construct();
    virtual void ConstructSD();

    void InitScoring(SourceWorldConstruction *);

    void setPhantFilename(G4String);

    void getTopleft(G4double &x, G4double &y, G4double &z) const
    {
        x = XMin;
        y = YMin;
        z = ZMin;
    }

    void getNumVoxels(G4int &numX, G4int &numY, G4int &numZ) const
    {
        numX = NumBinX;
        numY = NumBinY;
        numZ = NumBinZ;
    }

    G4long getTotalVoxels()
    {
        return NumBinX * NumBinY * NumBinZ;
    }

    void getVoxelSize(G4double &sizeX, G4double &sizeY, G4double &sizeZ) const
    {
        sizeX = halfVoxelDimensionX * 2.0;
        sizeY = halfVoxelDimensionY * 2.0;
        sizeZ = halfVoxelDimensionZ * 2.0;
    }

    G4double getTrackLengthStatus() { return fUseTrackLength; }

    void setRefAk(G4double);
    void setAkPerHistory(G4double);
    void setTotalTime(G4double);
    void setH(G4double);
    void setTrackLength(G4bool);

    G4double getTotalTime()
    {
        return fTotalTime;
    }

    G4double getRefAk()
    {
        return fRefAk;
    };

    G4double getAkPerHistory()
    {
        return fAkPerHistory;
    };

    G4double getH()
    {
        return fH;
    };

    G4double *GetDensities()
    {
        return densities;
    }

    size_t *GetMatNumbers()
    {
        return materialNumbers;
    }

    std::vector<G4String> GetMatNames()
    {
        return matNames;
    }

    G4bool UseTrackLength()
    {
        return fUseTrackLength;
    }

  private:
    G4String phantomFilename;
    G4double fAkPerHistory;
    G4double fRefAk;
    G4double fTotalTime;
    G4double fH;
    G4bool fUseTrackLength;
    ParallelWorldMessenger *fMessenger;

    G4double pixel_spacing_X;
    G4double pixel_spacing_Y;
    G4double pixel_spacing_Z;
    PatientMaterial *mat;
    G4int NumBinX, NumBinY, NumBinZ;
    G4double halfVoxelDimensionX, halfVoxelDimensionY, halfVoxelDimensionZ;
    G4double *densities;
    size_t *materialNumbers;
    std::vector<G4String> matNames;

    G4double XMax, XMin;
    G4double YMax, YMin;
    G4double ZMin, ZMax;
    DetectorConstruction *egsphant;
    bool scoringInitialized;
};

#endif
