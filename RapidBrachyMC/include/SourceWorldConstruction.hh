#ifndef SourceWorldConstruction_h
#define SourceWorldConstruction_h 1

#include "globals.hh"

#include "PrimaryGeneratorAction.hh"
#include "G4VUserParallelWorld.hh"
#include "PatientMaterial.hh"
#include "SourceWorldMessenger.hh"
#include "SourceMaterial.hh"
#include "SourceModel.hh"
#include "ControlPoint.hh"

class SourceWorldMessenger;
class RunAction;

class SourceWorldConstruction : public G4VUserParallelWorld
{
  public:
    SourceWorldConstruction(G4String &);

    ~SourceWorldConstruction();

    virtual void Construct();

    void loadSource();

    void selectSourceModel(G4String);
    void selectSourceCore(G4String);

    void setShieldRadius(G4double);

    void addParticleGenerator(PrimaryGeneratorAction *);

    void startSimulation(G4int nhist);

    void placeControlPoint(ControlPoint &cpt);

    void setCoreA(G4int);
    void setCoreZ(G4int);
    void setH(G4double);
    void setAkPerHistory(G4double);

    private:
    G4String sourceGeometry;
    G4String sourceActiveCore;

    G4int iCoreA, iCoreZ;

    SourceWorldMessenger *messenger;

    PatientMaterial *mat;
    SourceMaterial *pMaterial;

    G4double fShieldRadius;
    G4bool constructed;
    G4bool useTrackLength;

    SourceModel *source;
    std::vector<PrimaryGeneratorAction *> particleGenerators;
    bool sourceLoaded;
};

#endif
