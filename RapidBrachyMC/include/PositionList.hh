#ifndef PositionList_h
#define PositionList_h 1

#include <vector>
#include "globals.hh"
#include "G4ThreeVector.hh"

class PositionList
{
  public:
    PositionList(G4String);
    PositionList() {}
    ~PositionList() {}

    G4bool ReadMacFile(G4String);
    G4bool ReadDataFile(G4String);
    G4int GetNumberOfSources() { return listOfPositions.size(); }
    G4ThreeVector GetPosition(G4int SourceNumber);
    G4double GetAngle(G4int SourceNumber);

    void AddPosition(G4ThreeVector);
    void AddAngle(G4double);
    std::vector<G4ThreeVector> GetListOfPositions();

  private:
    G4int totalNumberOfSources;
    std::vector<G4ThreeVector> listOfPositions;
    std::vector<G4double> listOfAngles;
};

#endif
