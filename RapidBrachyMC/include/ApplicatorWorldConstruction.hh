#pragma once

// GEANT4 //
#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4VUserParallelWorld.hh"
#include "G4NistManager.hh"
#include "G4Material.hh"
#include "G4TriangularFacet.hh"
#include "G4PVPlacement.hh"


// BRACHYSOURCE //
class ApplicatorWorldMessenger;
class SourceMaterial;


class ApplicatorWorldConstruction : public G4VUserParallelWorld
{
  public:
    ApplicatorWorldConstruction(G4String name);
    ~ApplicatorWorldConstruction();

    void Construct();
    void DoneAddingVolume();

  public:
    void SetMaterial(G4String material)
    {
        _material = material;
    }

    void SetDensity(double density)
    {
        _density = density * g/cm3;
    }

    void SetXPosition(double x)
    {
        _x_position = x;
    }

    void SetYPosition(double y)
    {
        _y_position = y;
    }

    void SetZPosition(double z)
    {
        _z_position = z;
    }

    void SetXRotation(double x)
    {
        _x_rotation = x;
    }

    void SetYRotation(double y)
    {
        _y_rotation = y;
    }

    void SetZRotation(double z)
    {
        _z_rotation = z;
    }

    void AddVertex(G4ThreeVector vertex)
    {
        _vertices.push_back(vertex);
    }

    // TODO: Assuming vertices have been added first.
    void AddFace(G4ThreeVector points)
    {
        int a = (int) points.x();
        int b = (int) points.y();
        int c = (int) points.z();
        
        auto face = new G4TriangularFacet( _vertices[a]
                                         , _vertices[b]
                                         , _vertices[c]
                                         , ABSOLUTE);

        _faces.push_back(face);
    }    

  private:
    ApplicatorWorldMessenger* _messenger;
    
    SourceMaterial* _materials_manager;

    G4String _material;

    double _density;

    double _x_position;
    double _y_position;
    double _z_position;

    double _x_rotation;
    double _y_rotation;
    double _z_rotation;

    std::vector<G4RotationMatrix*> _rotations;
    std::vector<G4ThreeVector> _positions;
    std::vector<G4LogicalVolume*> _logicals;

    std::vector<G4ThreeVector> _vertices;
    std::vector<G4TriangularFacet*> _faces;
};

