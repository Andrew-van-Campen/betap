#ifndef ParallelWorldMessenger_h
#define ParallelWorldMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"
#include "ParallelWorldConstructionScoring.hh"

class ParallelWorldConstructionScoring;

class G4UIdirectory;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;

class ParallelWorldMessenger : public G4UImessenger
{
  public:
    explicit ParallelWorldMessenger(ParallelWorldConstructionScoring *);
    virtual ~ParallelWorldMessenger();

    virtual void SetNewValue(G4UIcommand *, G4String);

  private:
    ParallelWorldConstructionScoring *fParWorld;

    G4UIdirectory *fParWorldDir;
    G4UIcmdWithAString *fphantCmd;
    G4UIcmdWithAString *sourceCmd;
    G4UIcmdWithADouble *fAkPerHistoryCmd;
    G4UIcmdWithADouble *fRefAkCmd;
    G4UIcmdWithADouble *fTotalTimeCmd;
    G4UIcmdWithADouble *fHCmd;
    G4UIcmdWithABool *fTrackLengthCmd;
};

#endif
