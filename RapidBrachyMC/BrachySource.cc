#include <string>
#include "G4UImanager.hh"
#include "PhysicsList.hh"
#include "DetectorConstruction.hh"
#include "SourceWorldConstruction.hh"
#include "ApplicatorWorldConstruction.hh"
#include "ParallelWorldConstructionScoring.hh"
#include "ActionInitialization.hh"
#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#include "G4UIExecutive.hh"

#include "G4ParallelWorldPhysics.hh"

using namespace std;

int main(int argc, char **argv)
{
    G4String fileName;
    long int seed = 12378632LL;
    CLHEP::RanecuEngine *theRanGenerator = new CLHEP::RanecuEngine;
    theRanGenerator->setSeed(seed);
    G4Random::setTheEngine(theRanGenerator);

    if (argc > 1)
    {
        fileName = argv[1];
    }
    else
    {
        fileName = "";
    }

#ifdef G4MULTITHREADED
    G4MTRunManager *runManager = new G4MTRunManager;
#else
    G4RunManager *runManager = new G4RunManager;
#endif

    DetectorConstruction *detector = new DetectorConstruction();

    // ORDER MATTERS (Source -> Applicator -> Patient/Scoring)
    G4String sourceWorldName = "SourceWorld";
    SourceWorldConstruction *sourceWorld = new SourceWorldConstruction(sourceWorldName);
    detector->RegisterParallelWorld(sourceWorld);

    G4String applicatorWorldName = "ApplicatorWorld";
    ApplicatorWorldConstruction *applicatorWorld = new ApplicatorWorldConstruction(applicatorWorldName);
    detector->RegisterParallelWorld(applicatorWorld);

    G4String scoringWorldName = "ScoringWorld";
    ParallelWorldConstructionScoring *scoringWorld = new ParallelWorldConstructionScoring(scoringWorldName, detector);
    detector->RegisterParallelWorld(scoringWorld);

    runManager->SetUserInitialization(detector);

    PhysicsList *phys = new PhysicsList(scoringWorld);

    runManager->SetUserInitialization(phys);

    runManager->SetUserInitialization(new ActionInitialization(fileName,
                                                               scoringWorld,
                                                               sourceWorld,
                                                               detector));

    G4UImanager *UI = G4UImanager::GetUIpointer();

#ifdef G4VIS_USE
    G4VisManager *visManager = new G4VisExecutive;
    visManager->Initialize();
#endif

    if (argc != 1)
    {
        G4String command = "/control/execute ";
        UI->ApplyCommand(command + fileName);
    }
    else
    {
        std::cout << "BrachySource called with no arguments: starting GUI" << std::endl;
        G4UIExecutive *ui = new G4UIExecutive(argc, argv);
        ui->SessionStart();
        delete ui;
    }

    delete runManager;

#ifdef G4VIS_USE
    delete visManager;
#endif

    return 0;
}
