import os
import numpy as np
import pandas as pd
import math








# Function to read the input file for a series of runs
#.csv file. Each row is a separate run. 
#The columns are: depth of the artery | separation between artery and vein | atomic number of the isotope | atomic mass of the isotope | number of events to run
def import_values(filename):
	names = pd.Series(['depth','sep','Z','A','events','source'])
	frame = pd.read_csv(filename,header=None,names=names)
	return frame


# Function that writes a properly formatted macro for the ./BetaP Geant4 simulation
#inputs: name of directory to be created to save the data from the run, name of the Geant4 run, depth of the radial artery for the run, separation between artery and vein, atomic number of the isotope, atomic mass of the isotope, number of events to run
def create_macro(dir_name,name,depth,sep,Z,A,events,source):
#calculates out the geometry for the run. This is performed in the same manner as in the IsotopeDetectorConstruction.cc file.
	one = 31.65-depth
	angle = math.pi/2- math.acos(1-(sep*sep)/(2*(31.65-depth)*(31.65-depth)))
	two = (31.65-depth)*math.sin(angle)
	three = (31.65-depth)*math.cos(angle)
#rounds the floats two and three to 4 sig figs. 
	two = round(two,4)
	three = round(three,4)

# Creates a new .mac file to write the macro in. 
	f = open(name+".mac", "w")

# Initializes the Geant4 run
	f.write("/run/initialize\n")
# sets verbosity of Geant4 output to 1. This controls how detailed the output is. 
	f.write( "/control/verbose 1\n")
	f.write( "/run/verbose 1\n")

	if((source == 'vas') or (source=='both')):
	# Creates first source using the GPS package in Geant4
		f.write( "/gps/particle ion\n")
	# sets the element and isotope of the first source. 
		f.write( f"/gps/ion {Z} {A} 0 0\n" )
	# The next lines disperse the source through a CYLINDRICAL VOLUME with the dimensions shown.
	# At the beginning of each event, a source particle will be simulated in a randomly determined location within this volume
		f.write( "/gps/pos/type Volume\n")
		f.write( "/gps/pos/shape Cylinder\n")
		f.write( "/gps/pos/radius 0.99 mm\n")
		f.write( "/gps/pos/halfz 4.99 cm\n")
	# Gives the source particles zero momentum 
		f.write( "/gps/energy 0 keV\n")
	# Places the center of the center at the center of the radial artery and confines the source to the radial artery
		f.write( f"/gps/pos/centre {one} 0 0 mm\n")
		#f.write( "/gps/pos/confine arm_source_phys \n")

	#Adds a second source.
	#The following lines sets up the second source in the same manner as the first, except it is confined to the radial vein structure.
		f.write( "/gps/source/add 1\n")

		f.write( "/gps/particle ion\n")

		f.write( f"/gps/ion {Z} {A} 0 0\n")

		f.write( "/gps/pos/type Volume\n")
		f.write( "/gps/pos/shape Cylinder\n")
		f.write( "/gps/pos/radius 0.99 mm\n")
		f.write( "/gps/pos/halfz 4.99 cm\n")
		f.write( "/gps/energy 0 keV\n")
		f.write( f"/gps/pos/centre {two} {three} 0 mm\n")
	if((source == 'body') or (source=='both')):
		if ((source == 'body')):
			f.write( "/gps/source/add 1\n")
		f.write( "/gps/particle ion\n")

		f.write( f"/gps/ion {Z} {A} 0 0\n")

		f.write( "/gps/pos/type Surface\n")
		f.write( "/gps/pos/shape Cylinder\n")
		f.write( "/gps/pos/radius 60 mm\n")
		f.write( "/gps/pos/halfz 20 cm\n")
		f.write( "/gps/energy 0 keV\n")
		f.write( f"/gps/pos/centre 71.62 0 0 mm\n")

#Starts the simulation with the chosen number of events.
	f.write( f"/run/beamOn {events}\n")	
#Returns the macro file name
	return(name+".mac")


# Here starts the main part of the program
#Manually change the csv_path to point to the .csv with the run info and the folder where the final data files will be kept.
csv_path = "testcsv.csv"
dir_name = "body_test"
#df is a dataframe that holds the settings for the runs
df = import_values(csv_path)

# Make the data directory and change directories to it. 
os.system("mkdir "+dir_name)
os.chdir(dir_name)

# initialize variables
holder = "blank"
names = []

# assign variables that will 
skips = np.linspace(0,35,37) 
names_2 = pd.Series(['Edep', 'Edep_gamma', 'PosX', 'PosY','PosZ', 'tracker', 'E_Pos', 'PosXP', 'PosYP', 'PosZP', 'positron_name','photon_name','LYSOEdep', 'LYSOEdep_gamma', 'LYSOPosX', 'LYSOPosY','LYSOPosZ', 'LYSOPosXP', 'LYSOPosYP', 'LYSOPosZP','botEdep', 'botEdep_gamma', 'botPosX', 'botPosY','botPosZ', 'botPosXP', 'botPosYP', 'botPosZP','rightDet','leftDet','Total_Events' ]) 


# This loops through "df" to create a macro file and perform a geant4 run for each individual event. 
for index,row in df.iterrows():
	# holder variable used for naming the run
	holder = str(index)+"_"+str(row.depth)+"mmDepth_"+str(row.sep)+"mmSep_"+str(row.Z)+"_"+str(row.A)
	names.append(holder)
	# name of the macro, created from the variables in the run.
	macro_name = create_macro(dir_name,names[index],row.depth,row.sep,int(row.Z),int(row.A),int(row.events),row.source)
	# create a directory for each and
	os.system("mkdir "+holder)
	os.chdir(holder)
	# prints the name of the macro
	print(f"../.././BetaP -n {names[index]} -m ../{macro_name} -g linear_2 -s {row.sep} -d {row.depth}")

	# runs the ./BetaP program with the created macro, using the linear_2 setting and no optical photon tracking. 
	os.system(f"../.././BetaP -n {names[index]} -m ../{macro_name} -g linear_2 -s {row.sep} -d {row.depth}")
	# create an empty datafram to assemble the data. 
	# note that the ./BetaP program will output one .csv dataset per thread (default 8 threads)
	# the following code will combine the separate files. 
	data = pd.DataFrame()
	os.chdir('../')
	folder_path = f"{holder}/"
	#loops through all files in the data directory, appends the event data to "data" and deletes the old .csv files.
	for filename in os.listdir(folder_path):
		if filename.endswith('.csv'):
			data_path = folder_path + filename
			data_0 = pd.read_csv(data_path,skiprows=skips,header=None,names=names_2)   
			data = data.append(data_0)
			print(filename)
			os.system("pwd")
			os.remove(holder+"/"+filename)
	# saves the .csv files in a new csv
	data.to_csv(holder+".csv")
	

os.chdir('../')

	
