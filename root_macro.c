void format_h(TH1F* h, int linecolor){
	h->SetLineWidth(3);
	h->SetLineColor(linecolor);
    h->SetFillColor(linecolor);}
// Will format the histogramas for readabality 


void root_macro(){

format_h(PosPosy,kBlue);
format_h(PosArty,kRed);
format_h(PosVeny,kGreen);

  auto sum_h = new TH1F("Position","y-Position of Energy Deposition in the Scintillator (mm)",300,-100,100);
  sum_h->Add(PosVeny);
  sum_h->Add(PosPosy);
  sum_h->Add(PosArty);
format_h(sum_h,kOrange);


THStack *a =new THStack("a","Stacked Position Histograms");

 a->Add(PosArty);

 a->Add(PosVeny);

 a->Add(PosPosy);

TCanvas* c_sum = new TCanvas();
c_sum->Divide (2,1,0,0);
c_sum->cd(1);
sum_h->Draw();
c_sum->cd(2);
TVirtualPad* c2 = c_sum->GetPad(2);

PosVeny->Draw();
PosArty->Draw("Same");
PosPosy->Draw("Same");
TLegend* l = new TLegend(0.55,0.7,0.99,0.94);
l->SetHeader("Legend","C");
l->AddEntry(PosVeny,"Events originating from Vein","f");
l->AddEntry(PosArty,"Events originating from Artery","f");
l->AddEntry(PosPosy,"Energy deposited directly by Positrons","f");

//TLegend* l =  c2->BuildLegend(0.3,0.21,0.3,0.21,"y-Position","f");

l->Draw();







}

