import os
import numpy as np
import pandas as pd
import math








def import_values(filename):
	names = pd.Series(['depth','sep','Z','A','events'])
	frame = pd.read_csv(filename,header=None,names=names)
	return frame



def create_macro(dir_name,name,depth,sep,Z,A,events):
	one = 31.65-depth
	angle = math.pi/2- math.acos(1-(sep*sep)/(2*(31.65-depth)*(31.65-depth)))
	two = (31.65-depth)*math.sin(angle)
	three = (31.65-depth)*math.cos(angle)

	two = round(two,4)
	three = round(three,4)

	#f = open(dir_name+'/'+name+".amac", "w")
	f = open(name+".mac", "w")
	f.write("/run/initialize\n")

	f.write( "/control/verbose 1\n")
	f.write( "/run/verbose 1\n")



	f.write( "/gps/particle ion\n")

	f.write( f"/gps/ion {Z} {A} 0 0\n" )

	f.write( "/gps/pos/type Volume\n")
	f.write( "/gps/pos/shape Cylinder\n")
	f.write( "/gps/pos/radius 0.99 mm\n")
	f.write( "/gps/pos/halfz 9.99 cm\n")
	f.write( "/gps/energy 0 keV\n")
	f.write( f"/gps/pos/centre {one} 0 0 mm\n")
	#f.write( "/gps/pos/confine arm_source_phys \n")

	f.write( "/gps/source/add 1\n")

	f.write( "/gps/particle ion\n")

	f.write( f"/gps/ion {Z} {A} 0 0\n")

	f.write( "/gps/pos/type Volume\n")
	f.write( "/gps/pos/shape Cylinder\n")
	f.write( "/gps/pos/radius 0.99 mm\n")
	f.write( "/gps/pos/halfz 9.99 cm\n")
	f.write( "/gps/energy 0 keV\n")
	f.write( f"/gps/pos/centre {two} {three} 0 mm\n")
	#f.write( "/gps/pos/confine vein_source_phys\n")

	f.write( f"/run/beamOn {events}\n")	
	return(name+".mac")



csv_path = "variable_depth_G68.csv"

df = import_values(csv_path)
dir_name = "variable_depth_Ga68_linear"
os.system("mkdir "+dir_name)


os.chdir(dir_name)

holder = "blank"
names = []

skips = np.linspace(0,18,20) 
names_2 = pd.Series(['Edep', 'Edep_gamma', 'PosX', 'PosY','PosZ', 'tracker', 'E_Pos', 'PosXP', 'PosYP', 'PosZP', 'positron_name','photon_name', 'botHit','Total_Events' ]) 

#os.system("cd ../")

for index,row in df.iterrows():
	holder = "linear_"+str(index)+"_"+str(row.depth)+"mmDepth_"+str(row.sep)+"mmSep_"+str(row.Z)+"_"+str(row.A)
	names.append(holder)
	macro_name = create_macro(dir_name,names[index],row.depth,row.sep,int(row.Z),int(row.A),int(row.events))
	os.system("mkdir "+holder)
	os.chdir(holder)
	print(f"../.././BetaP -n {names[index]} -m ../{macro_name} -g linear -s {row.sep} -d {row.depth}")
	data = pd.DataFrame()
	os.system(f"../.././BetaP -n {names[index]} -m ../{macro_name} -g linear -s {row.sep} -d {row.depth}")
	data = pd.DataFrame()
	os.chdir('../')
	folder_path = f"{holder}/"
	for filename in os.listdir(folder_path):
		if filename.endswith('.csv'):
			data_path = folder_path + filename
			data_0 = pd.read_csv(data_path,skiprows=skips,header=None,names=names_2)   
			data = data.append(data_0)
			print(filename)
			os.system("pwd")
			os.remove(holder+"/"+filename)
	data.to_csv(holder+".csv")
	

os.chdir('../')

	
