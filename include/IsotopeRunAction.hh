

#ifndef IsotopeRunAction_h
#define IsotopeRunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "IsotopeDetectorConstruction.hh"




//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....



class IsotopeRunAction : public G4UserRunAction
{
  public:
  IsotopeRunAction(long int, G4String);

   ~IsotopeRunAction();

  public:
     void BeginOfRunAction(const G4Run*);
     void EndOfRunAction(const G4Run*);
     G4String fileName;

     std::vector<G4double>& get_times_1(){return times_1;};
     std::vector<G4double>& get_times_2(){return times_2;};
     std::vector<G4int>& get_photons(){return photons;};
     std::vector<G4int>& get_photons_2(){return photons;};

     void reset_photons();

     G4int Add_photons(G4String pmt);
     
private:
  std::vector<G4double> times_1;
  std::vector<G4double> times_2; 
  std::vector<G4int> photons;

  G4int runIDcounter;
  long int MyTime;
  
  
  G4String ending;
  G4String Name;
  
 

  

};

#endif

