
 class Run : public G4Run {
 public:
  Run();
  virtual ~Run() {};
  virtual void RecordEvent(const G4Event*);
  virtual void Merge(const G4Run*);
  G4double GetEmEnergy() const { return em_ene; }
  G4double GetHadEnergy() const { return had_ene; }
  G4double GetShowerShape() const { return shower_shape; }
 private:
  G4double em_ene; //accumulated energy in EM calo
  G4double had_ene;//accumulated energy in HAD calo
  G4double shower_shape;//accumulated shower shape ( f=EM/(EM+HAD) )
  G4int ECHCID; //ID for EM hits collection
  G4int HCHCID; //ID for HAD hits collection
 }; 