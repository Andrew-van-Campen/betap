#ifndef IsotopeActionInitialization_h
#define IsotopeActionInitialization_h 1

#include "G4VUserActionInitialization.hh"
#include "globals.hh"


class IsotopeDetectorConstruction;

/// Action initialization class.
///

class IsotopeActionInitialization : public G4VUserActionInitialization
{
  public:
    IsotopeActionInitialization(IsotopeDetectorConstruction*, long int, G4String );
    virtual ~IsotopeActionInitialization();

    virtual void BuildForMaster() const;
    virtual void Build() const;

  private:
    IsotopeDetectorConstruction* fDetConstruction;
    G4String Name;
    long int Tid;

};

#endif
