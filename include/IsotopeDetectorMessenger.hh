#ifndef IsotopeDetectorMessenger_h
#define IsotopeDetectorMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class IsotopeDetectorConstruction;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithABool;
class G4UIcmdWithAnInteger;

class IsotopeDetectorMessenger: public G4UImessenger
{
  public:
    IsotopeDetectorMessenger(IsotopeDetectorConstruction* );
    virtual ~IsotopeDetectorMessenger();
    
    virtual void SetNewValue(G4UIcommand*, G4String);
    
    
  private:
  

    IsotopeDetectorConstruction* fDetector;
    
    G4UIdirectory*             fDirectory;
  

    
};


#endif