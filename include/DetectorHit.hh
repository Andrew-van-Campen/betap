
#ifndef DetectorHit_h
#define DetectorHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "tls.hh"



class DetectorHit : public G4VHit
{
public:
  DetectorHit();
  DetectorHit(const DetectorHit &right);
  virtual ~DetectorHit();

//operators
  const DetectorHit& operator=(const DetectorHit &right);
  G4bool operator==(const DetectorHit &right) const;
 

  inline void* operator new(size_t);
  inline void  operator delete(void*hit);
 

 //methods from base class
  virtual void Draw();
  virtual void Print();

//setters
   void SetEdep(G4double de) { edep=de; };
   void SetPos(G4ThreeVector xyz) { pos=xyz;} ;
   void SetParticle(G4String str) {part_name = str;};
   void SetScint_name(G4String str) {scint_name = str;};
   void SetIs_annh(G4bool is){is_annh = is;};

//getters
   G4double GetEdep() const { return edep; } ;
   G4ThreeVector GetPos() const {return pos;};
   G4String GetParticle() const {return part_name;};
   G4bool GetIs_annh() {return is_annh;}
   G4String GetScint_name() {return scint_name;};

private:
  G4double edep;
  G4ThreeVector pos;
  G4String part_name, scint_name;
  G4bool is_annh;
  //G4int tracker;
};


using  DetectorHitsCollection = G4THitsCollection<DetectorHit> ;
extern G4ThreadLocal G4Allocator<DetectorHit>* DetectorHitAllocator;

inline void* DetectorHit::operator new(size_t)
{

  if(!DetectorHitAllocator){
    DetectorHitAllocator = new G4Allocator<DetectorHit>;
  }
  return (void*)DetectorHitAllocator->MallocSingle();
}


inline void DetectorHit::operator delete(void*hit)
{
  DetectorHitAllocator->FreeSingle((DetectorHit*) hit);
}


//inline G4ThreeVector DetectorHit::GetPos() const{return pos;}

#endif 








