
#ifndef PMT_hit_h
#define PMT_hit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "tls.hh"



class PMT_hit : public G4VHit
{
public:
  PMT_hit();
  PMT_hit(const PMT_hit &right);
  virtual ~PMT_hit();

//operators
  const PMT_hit& operator=(const PMT_hit &right);
  G4bool operator==(const PMT_hit &right) const;
 

  inline void* operator new(size_t);
  inline void  operator delete(void*hit);
 

 //methods from base class
  virtual void Draw();
  virtual void Print();

//setters
   
   void SetName(G4String str) {PMT_name = str;};
   

//getters
   
   G4String GetName() const {return PMT_name;};
  

   

private:
  G4String PMT_name;
};


using  PMTHitsCollection = G4THitsCollection<PMT_hit> ;
extern G4ThreadLocal G4Allocator<PMT_hit>* PMT_hitAllocator;

inline void* PMT_hit::operator new(size_t)
{

  if(!PMT_hitAllocator){
    PMT_hitAllocator = new G4Allocator<PMT_hit>;
  }
  return (void*)PMT_hitAllocator->MallocSingle();
}


inline void PMT_hit::operator delete(void*hit)
{
  PMT_hitAllocator->FreeSingle((PMT_hit*) hit);
}


//inline G4ThreeVector PMT_hit::GetPos() const{return pos;}

#endif 








