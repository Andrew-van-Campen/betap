#ifndef IsotopeEventAction_h
#define IsotopeEventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "IsotopeRunAction.hh"
#include "SensitiveDetector.hh"
#include "DetectorHit.hh"
#include "vector"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class IsotopeEventAction : public G4UserEventAction
{
  public:
    IsotopeEventAction(IsotopeRunAction* aRun);
    ~IsotopeEventAction();

  public:

    G4double 
    PosArtX, 
    PosArtY, 
    PosArtZ, 
    PosVenX, 
    PosVenY, 
    PosVenZ, 
    PosX, 
    PosY, 
    PosZ, 
    PosXP, 
    PosYP, 
    PosZP,
    LYSOPosX, 
    LYSOPosY, 
    LYSOPosZ, 
    LYSOPosXP, 
    LYSOPosYP, 
    LYSOPosZP,
    botDetPosX, 
    botDetPosY, 
    botDetPosZ, 
    botDetPosXP, 
    botDetPosYP, 
    botDetPosZP;
    void BeginOfEventAction(const G4Event* anEvent);
    void EndOfEventAction(const G4Event* anEvent);
    void SetDrawFlag(G4String val)  {drawFlag = val;};
 
std::vector<double>* times_1;
std::vector<double>* times_2;


//Getters
G4int GetTracker(){return tracker ;};
G4double GetEventEnergy() {return EventEnergy;};
G4ThreeVector GetArtPosition(){return ArtPosition;};
G4ThreeVector GetVenPosition(){return VenPosition;};
G4int getArtTracker(){return ArtTracker;};
G4int getVenTracker(){return VenTracker;};
G4bool GetAnnh(){return Annh;};
G4int GetIs_botHit(){return is_botHit;};
//G4int getPMT_1_photons(){return PMT_1_photons;};
//G4int getPMT_2_photons(){return PMT_2_photons;};
//G4double getDeltaT(){return DeltaT;};
//std::vector<G4double> get_times_1(std::vector<G4double> &vec){return vec;};
//std::vector<G4double> get_times_2(std::vector<G4double> &vec){return vec;};
//std::vector<G4int>& get_photons(std::vector<G4int>& vec){return vec;};


//Setters
void SetTracker(G4int t){tracker = t;};  
void SetEventEnergy(G4double e){EventEnergy = e;};
void SetPositron(G4double);
void SetGamma(G4double);
//void SetPMT_1_photons(G4int i){PMT_1_photons=i;};
//void SetPMT_2_photons(G4int i){PMT_2_photons=i;};
void SetArtTracker(G4int i){ArtTracker = i;};
void SetVenTracker(G4int i){VenTracker = i;}; 
void SetAnnh(G4bool b ){Annh = b;};
void SetIs_botHit(G4int b ){is_botHit = b;};
//void SetDeltaT();
//void ResetDeltaT(){DeltaT=0;};

 
//Adders
void AddEventEnergy(G4double e){EventEnergy = EventEnergy+e;};
void AddArtPosition(G4ThreeVector);
void AddVenPosition(G4ThreeVector);
void AddVenTracker(){VenTracker=VenTracker+1;};
void AddArtTracker(){ArtTracker=ArtTracker+1;};
//void AddPMT_1_photons(){PMT_1_photons=PMT_1_photons+1;};
//void AddPMT_2_photons(){PMT_2_photons=PMT_2_photons+1;};
//void Add_times_1(G4double d);
//void Add_times_2(G4double d);
//void Add_photons(G4String pmt);

//void clear_times();
//void sort_times();
  
  G4ThreeVector position;
  
private:
  
  DetectorHitsCollection* GetHitsCollection(G4int hcID, const G4Event* event) const;

  IsotopeRunAction* IsotopeRun;
  
  G4String drawFlag;
  G4bool Annh;
  //annh is a tracker that stacking action  uses to keep track of when it is running tracks after annihilation in an event. 
  G4double EventEnergy, DeltaT;

  G4int tracker=0, VenTracker=0, ArtTracker=0, PMT_1_photons=0, PMT_2_photons=0, is_botHit = 0;

  G4int fCollID,PMTID,LYSO_ID,botID;

  //std::vector<G4int> photons;

  G4ThreeVector ArtPosition, VenPosition;
 
 
};

#endif

    




