//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************
//

#ifndef IsotpeStackingAction_h
#define IsotopeStackingAction_h 1

#include "globals.hh"
#include "G4UserStackingAction.hh"
#include "G4EventManager.hh"
#include "IsotopeEventAction.hh"
#include "IsotopeRunAction.hh"
using namespace std;



class IsotopeStackingAction : public G4UserStackingAction
{
  public:
  IsotopeStackingAction();
  virtual    ~IsotopeStackingAction();






private:

 public:
    virtual G4ClassificationOfNewTrack  ClassifyNewTrack(const G4Track* aTrack);
  	virtual void NewStage( ){
  		IsotopeEventAction* evt = (IsotopeEventAction*)G4EventManager::GetEventManager()->GetUserEventAction();
  		evt->SetAnnh(true);};

  	
  	
};

#endif