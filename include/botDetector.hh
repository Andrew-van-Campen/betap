
#ifndef botDetector_h
#define botDetector_h 1

#include "G4VSensitiveDetector.hh"
#include "G4ThreeVector.hh"
#include "botDetectorHit.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"
#include <vector>


class G4HCofThisEvent; 

class G4Step;


class botDetector: public G4VSensitiveDetector
{
public:
  botDetector(const G4String& name,
  					        const G4String& Hcname);
  virtual ~botDetector();

  virtual void Initialize(G4HCofThisEvent* HCE);
  virtual G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* ROhist);
  void clear();
  void DrawAll();
  void PrintAll();
  virtual void EndOfEvent(G4HCofThisEvent* HCE);
private:
  botDetectorHitsCollection* Collection;
  G4int HCID;
};
#endif


