#ifndef IsotopeDetectorConstruction_H
#define IsotopeDetectorConstruction_H 1

#include "G4VUserDetectorConstruction.hh"
#include "IsotopeDetectorConstruction.hh"
#include "IsotopeDetectorMessenger.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4SubtractionSolid.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4SubtractionSolid.hh"
#include "G4VisAttributes.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"



#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
class IsotopeDetectorConstruction : public G4VUserDetectorConstruction
{
  public:

    IsotopeDetectorConstruction(G4String in, G4String sep, G4String depth);
    ~IsotopeDetectorConstruction();

   void ChangeShield(G4bool is_shield);

   virtual void ConstructSDandField();

    G4VPhysicalVolume* Construct();

  private:

  G4String geo;

  G4Material* theMaterial;
  G4VPhysicalVolume 
  *World_phys,
  *physPhantom,
  *arm_source_phys,
  *vein_source_phys,
  *botPhysical,
  *body_phys,
  *LYSOphysical;

  
  G4LogicalVolume* World_log;

  G4double WorldSize;

  G4double sourceRadius, halfSourceHeight;
  G4double startPhi, spanPhi, spanTheta;


  G4double phantomRadius, halfPhantomHeight;
  G4double cutInnerRad, cutOuterRad, halfCutHeight;

G4double 
  shellInnerRadius,
  shellOuterRadius,
  halfBodyHeight,
  bodyRadius,
  detectorInnerRadius, 
  detectorOuterRadius, 
  halfDetectorLength,
  depth,
  separation,
  angle,
  Al_x,
  Al_y,
  Al_z,
  LYSO_x,
  LYSO_y,
  LYSO_z,
  claddingInnerRadius, 
  CladdingOuterRadius, 
  halfCladdingLength, 
  wrapInnerRadius, 
  WrapOuterRadius, 
  halfWrapLength,
  PMT_inner,
  PMT_outer,
  PMT_z;
  
  G4VSolid 
  *scint_solid,
  *heat_shrink_solid,
  *cladding_solid,
  *PMT_1_solid,
  *PMT_2_solid,
  *Al_shield;
  
  G4VPhysicalVolume * scint_physical;

  IsotopeDetectorMessenger* fDetectorMessenger;
  
  G4LogicalVolume 
  *vein_source_log, 
  *source_log,
  *phantom_log,
  *arm_source_log,
  *cladding_logical,
  *heat_shrink_logical,
  *scint_logical,
  *PMT_1_logical,
  *PMT_2_logical,
  *PMT_logical,
  *topLYSO_log,
  *AlLog,
  *shellLog,
  *body_log;

  G4LogicalVolume
  *bot_log;

  G4MaterialPropertiesTable* fpst_mt;
  G4MaterialPropertiesTable* fpet_mt;
  G4MaterialPropertiesTable* facl_mt;
  G4MaterialPropertiesTable* fair_mt;

};

#endif

