
#ifndef PMT_SD_h
#define PMT_SD_h 1

#include "G4VSensitiveDetector.hh"
#include "G4ThreeVector.hh"
#include "PMT_hit.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"
#include <vector>


class G4HCofThisEvent; 

class G4Step;


class PMT_SD: public G4VSensitiveDetector
{
public:
  PMT_SD(const G4String& name,
  					        const G4String& Hcname);
  virtual ~PMT_SD();

  virtual void Initialize(G4HCofThisEvent* HCE);
  virtual G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* ROhist);
  void clear();
  void DrawAll();
  void PrintAll();
  virtual void EndOfEvent(G4HCofThisEvent* HCE);
private:
  PMTHitsCollection* Collection;
  G4int HCID;
};
#endif


