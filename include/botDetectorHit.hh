
#ifndef botDetectorHit_h
#define botDetectorHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "tls.hh"



class botDetectorHit : public G4VHit
{
public:
  botDetectorHit();
  botDetectorHit(const botDetectorHit &right);
  virtual ~botDetectorHit();

//operators
  const botDetectorHit& operator=(const botDetectorHit &right);
  G4bool operator==(const botDetectorHit &right) const;
 

  inline void* operator new(size_t);
  inline void  operator delete(void*hit);
 

 //methods from base class
  virtual void Draw();
  virtual void Print();

//setters
   void SetHit(G4bool h) { is_hit=h; };
   void SetEdep(G4double de) { edep=de; };
   void SetPos(G4ThreeVector xyz) { pos=xyz;} ;
   void SetIs_annh(G4bool is){is_annh = is;};
//getters
   G4bool GetHit() const { return is_hit; } ;
   G4double GetEdep() const { return edep; } ;
  G4ThreeVector GetPos() const {return pos;};
  G4bool GetIs_annh() {return is_annh;}

private:
  G4bool is_hit;
  G4double edep;
  G4ThreeVector pos;
  G4bool is_annh;
};


using  botDetectorHitsCollection = G4THitsCollection<botDetectorHit> ;
extern G4ThreadLocal G4Allocator<botDetectorHit>* botDetectorHitAllocator;

inline void* botDetectorHit::operator new(size_t)
{

  if(!botDetectorHitAllocator){
    botDetectorHitAllocator = new G4Allocator<botDetectorHit>;
  }
  return (void*)botDetectorHitAllocator->MallocSingle();
}


inline void botDetectorHit::operator delete(void*hit)
{
  botDetectorHitAllocator->FreeSingle((botDetectorHit*) hit);
}




#endif 








