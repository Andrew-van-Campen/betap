#include "IsotopeActionInitialization.hh"
#include "PrimaryGeneratorAction.hh"
#include "IsotopeRunAction.hh"
#include "IsotopeEventAction.hh"
#include "IsotopeSteppingAction.hh"
#include "IsotopeDetectorConstruction.hh"
#include "IsotopeStackingAction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

IsotopeActionInitialization::IsotopeActionInitialization
                            (IsotopeDetectorConstruction* detConstruction,long int Tid,G4String Name)
 : G4VUserActionInitialization(),
   fDetConstruction(detConstruction)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

IsotopeActionInitialization::~IsotopeActionInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void IsotopeActionInitialization::BuildForMaster() const
{

  SetUserAction(new IsotopeRunAction(Tid,Name));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void IsotopeActionInitialization::Build() const
{
	IsotopeRunAction* aRun = new IsotopeRunAction(Tid,Name);
  SetUserAction(aRun);
  
  SetUserAction(new PrimaryGeneratorAction);
  

  auto eventAction = new IsotopeEventAction(aRun);
  SetUserAction(eventAction);

  SetUserAction(new IsotopeStackingAction());

  SetUserAction(new IsotopeSteppingAction(eventAction));
   
 
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
