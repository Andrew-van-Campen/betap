#include "G4EmStandardPhysics_option4.hh"
#include "G4EmLivermorePhysics.hh"
#include "G4DecayPhysics.hh"
#include "G4RadioactiveDecayPhysics.hh"
#include "G4EmPenelopePhysics.hh"


#include "PhysicsList.hh"
#include "G4EmProcessOptions.hh"
#include "G4VPhysicsConstructor.hh"

#include "G4ParticleDefinition.hh"
#include "G4ProductionCutsTable.hh"
#include "G4ProcessManager.hh"
#include "G4ParticleTypes.hh"
#include "G4ios.hh"
#include "G4StepLimiter.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"


#include "G4ProcessVector.hh"

#include "G4ParticleTable.hh"
#include "G4Cerenkov.hh"
#include "G4OpticalPhysics.hh"

PhysicsList::PhysicsList(): G4VModularPhysicsList() {
  SetVerboseLevel(1);

  
  /// EM physics: 3 alternatives

  //emPhysicsList = new G4EmStandardPhysics_option4(1);

  // Alternatively you can substitute this physics list
  // with the LowEnergy Livermore or LowEnergy Penelope:
  //emPhysicsList = new G4EmLivermorePhysics();
  // Low Energy based on Livermore Evaluated Data Libraries

  // Penelope physics
  emPhysicsList = new G4EmPenelopePhysics();

  // Geant4DNA physics
  //emPhysicsList = new EmDNAPhysics();
  // emPhysicsList = new EmDNAPhysics_option1();

  // Add Decay
  decPhysicsList = new G4DecayPhysics();
  radDecayPhysicsList = new G4RadioactiveDecayPhysics();

  //optical = new G4OpticalPhysics();



  
}

PhysicsList::~PhysicsList() {
  delete decPhysicsList;
  delete radDecayPhysicsList;
  delete emPhysicsList;
  //delete optical;
}

void PhysicsList::ConstructParticle() {
  decPhysicsList->ConstructParticle();
 // optical->ConstructParticle();
}

void PhysicsList::ConstructProcess() {
  AddTransportation();
 
  emPhysicsList->ConstructProcess();

   G4EmProcessOptions emOptions;
   emOptions.SetFluo(false); // To activate deexcitation processes and fluorescence
   emOptions.SetAuger(true); // To activate Auger effect if deexcitation is activated
   emOptions.SetPIXE(true); // To activate Particle Induced X-Ray Emission (PIXE) 
   
  // decay physics list
  decPhysicsList->ConstructProcess();
  radDecayPhysicsList->ConstructProcess();
  //optical->ConstructProcess();
 

  


  G4cout << "### PhysicsList::ConstructProcess is done" << G4endl;
}

void PhysicsList::SetCuts() {
  // Definition of  threshold of production
  // of secondary particles
  // This is defined in range.
  defaultCutValue = 0.1 * mm; //0.1 * mm
  //SetCutValue(defaultCutValue, "gamma");
  //SetCutValue(defaultCutValue, "e-");
  //SetCutValue(defaultCutValue, "e+");

  // By default the low energy limit to produce
  // secondary particles is 990 eV.
  // This value is correct when using the EM Standard Physics.
  // When using the Low Energy Livermore this value can be
  // changed to 250 eV corresponding to the limit
  // of validity of the physics models.
  // Comment out following three lines if the
  // Standard electromagnetic Package is adopted.
  G4double lowLimit = 1000. * eV;
  G4double highLimit = 100. * GeV;

  G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(lowLimit,
                                                                  highLimit);

  // Print the cuts
  if (verboseLevel>0) DumpCutValuesTable();
}
