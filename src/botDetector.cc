
#include "botDetector.hh"
#include "botDetectorHit.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "IsotopeEventAction.hh"
#include "G4EventManager.hh"
botDetector::botDetector(const G4String& name,
                                    const G4String& Hcname)
:G4VSensitiveDetector(name),
HCID(-1),
Collection(NULL)

{
  
  collectionName.insert(Hcname);
}
botDetector::~botDetector()
{

}



void botDetector::Initialize(G4HCofThisEvent* HCE)
{
  // Create hits collection

  Collection = new botDetectorHitsCollection(SensitiveDetectorName,collectionName[0]);

  // Add this collection in HCE
  if(HCID<0)
    {
      HCID=G4SDManager::GetSDMpointer() -> GetCollectionID(Collection); 
    }
  HCE->AddHitsCollection(HCID,Collection);

}





G4bool botDetector::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{

  G4double edep = aStep -> GetTotalEnergyDeposit()/CLHEP::keV;

  if(edep==0.){
      return false;}


 botDetectorHit* newHit = new botDetectorHit();

  IsotopeEventAction* evt = (IsotopeEventAction*)G4EventManager::GetEventManager()->GetUserEventAction();
  newHit -> SetEdep(edep);
  
  newHit -> SetPos(aStep -> GetPreStepPoint() -> GetPosition());



if(evt->GetAnnh()){

    newHit-> SetIs_annh(true);
 }
  else{
    newHit-> SetIs_annh(false);
    
  }

if((aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName())=="botPhysical"){
  newHit -> SetHit(true);
}
  Collection->insert( newHit );
  return true;
}

void botDetector::EndOfEvent(G4HCofThisEvent* HCE)
{;}
void botDetector::clear()
{;}
void botDetector:: DrawAll()
{;}
void botDetector::PrintAll()
{;}






