#include "IsotopeRunAction.hh"


#include "G4Run.hh"
#include "G4UImanager.hh"
#include "G4VVisManager.hh"
#include "G4ios.hh"
#include "Analysis.hh"



#include <ctime>

#include "CLHEP/Random/Random.h"

#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>
using namespace std;


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....




IsotopeRunAction::IsotopeRunAction(long int tid, G4String name)
  :MyTime(tid),Name(name)
{
  ;
}
IsotopeRunAction::~IsotopeRunAction()
{
  delete G4AnalysisManager::Instance(); 
}
void IsotopeRunAction::BeginOfRunAction(const G4Run* aRun)
{

  //this segment will get current date for 
    time_t t = time(0);   // get time now

  string year, month, day, hour, minute, second;
    struct tm * now = localtime( & t );
        year =  to_string((now->tm_year + 1900)) ;
        month =   to_string((now->tm_mon + 1)) ;
        day = to_string(now->tm_mday);
        hour = to_string(now->tm_hour);
        minute = to_string(now->tm_min);
        second = to_string(now->tm_sec);
  string time_c =year + '-' + month + '-' + day + '-' + hour+ '-' + minute + '-' + second;



  G4cout << "### Run " << aRun->GetRunID() << " start." <<G4endl;
  if(G4VVisManager::GetConcreteInstance()) {
    G4UImanager* UI = G4UImanager::GetUIpointer();
    UI->ApplyCommand("/vis/scene/notifyHandlers");
  }
  CLHEP::HepRandom::showEngineStatus();

  string ending = "_BetaP_CADMESH";
  stringstream FileNameStream;
 
  FileNameStream <<Name<<time_c<<ending;
  fileName = FileNameStream.str();
  

  // Create analysis manager
  // The choice of analysis technology is done via selectin of a namespace
  // in B5Analysis.hh
  auto analysisManager = G4AnalysisManager::Instance();
  G4cout << "Using " << analysisManager->GetType() << G4endl;
analysisManager->SetFileName(fileName);


   
// Creating ntuple
    analysisManager->CreateNtuple("BetaP_CADMESH","Position & Energy");
    analysisManager->CreateNtupleDColumn("Edep");         // 0
    analysisManager->CreateNtupleDColumn("Edep_gamma");  // 1
    analysisManager->CreateNtupleDColumn("PosX");       // 2
    analysisManager->CreateNtupleDColumn("PosY");      // 3
    analysisManager->CreateNtupleDColumn("PosZ");     // 4
    analysisManager->CreateNtupleDColumn("tracker"); // 5
    analysisManager->CreateNtupleDColumn("E_Pos");  // 6
    analysisManager->CreateNtupleDColumn("PosX_P"); // 7
    analysisManager->CreateNtupleDColumn("PosY_P"); // 8
    analysisManager->CreateNtupleDColumn("PosZ_P"); //9
    analysisManager->CreateNtupleFColumn("positron_name"); //10
    analysisManager->CreateNtupleFColumn("photon_name"); //11
    
    analysisManager->CreateNtupleDColumn("LYSOEdep");  // 12
    analysisManager->CreateNtupleDColumn("LYSOEdep_gamma");       // 13
    analysisManager->CreateNtupleDColumn("LYSOPosX");      // 14
    analysisManager->CreateNtupleDColumn("LYSOPosY");     // 15
    analysisManager->CreateNtupleDColumn("LYSOPosZ");  // 16
    analysisManager->CreateNtupleDColumn("LYSOPosX_P"); // 17
    analysisManager->CreateNtupleDColumn("LYSOPosY_P"); // 18
    analysisManager->CreateNtupleDColumn("LYSOPosZ_P"); //19
    
    analysisManager->CreateNtupleDColumn("botEdep");  // 20
    analysisManager->CreateNtupleDColumn("botEdep_gamma");       // 21
    analysisManager->CreateNtupleDColumn("botPosX");      // 22
    analysisManager->CreateNtupleDColumn("botbotPosY");     // 23
    analysisManager->CreateNtupleDColumn("botPosZ");  // 24
    analysisManager->CreateNtupleDColumn("botPosX_P"); // 25
    analysisManager->CreateNtupleDColumn("botPosY_P"); // 26
    analysisManager->CreateNtupleDColumn("botPosZ_P"); //27
    analysisManager->CreateNtupleIColumn("rightDet"); //28
    analysisManager->CreateNtupleIColumn("leftDet"); //29
    analysisManager->CreateNtupleDColumn("Total_Events"); //28
   // analysisManager->CreateNtupleIColumn("Photons_PMT", get_photons_2()); //11
   
//removed these because we were not getting enought sig figs....
   // analysisManager->CreateNtupleDColumn("times_1", get_times_1()); //10
   // analysisManager->CreateNtupleDColumn("times_2", get_times_2()); //11
    
    analysisManager->FinishNtuple();

    


// Open an output file
analysisManager->OpenFile();

 
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void IsotopeRunAction::EndOfRunAction(const G4Run* )
{
CLHEP::HepRandom::showEngineStatus();

 // Get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();

    
 
  
  // save histograms & ntuple
  //
analysisManager->Write();
analysisManager->CloseFile();
printf("Data written to:"); 
G4cout<<"Data written to: "<< fileName <<G4endl;
printf(fileName);
printf("\n");


}

G4int IsotopeRunAction::Add_photons(G4String pmt){
  G4int pmt_n;

  if(pmt.substr(5,1) == "+"){
    if(pmt.length() == 7){
      pmt_n = std::stoi(pmt.substr(6,1));
    }
    else if(pmt.length() == 8){
      pmt_n = std::stoi(pmt.substr(6,2));
    }

  }

  else if (pmt.substr(5,1) == "-"){
    if(pmt.length() == 7){
      pmt_n = std::stoi(pmt.substr(6,1));
    }
    else if(pmt.length() == 8){
      pmt_n = std::stoi(pmt.substr(6,2));

    }
    pmt_n = pmt_n*(-1);
  }
  pmt_n = pmt_n+33;


  G4int & value = photons.at(pmt_n);
 
  photons[pmt_n] = value+1;

   return pmt_n ;

}
void IsotopeRunAction::reset_photons(){
std::vector<G4int> init(140,0);
photons = init;}





