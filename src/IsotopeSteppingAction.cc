#include "G4ios.hh"
#include "IsotopeSteppingAction.hh"
#include "G4Track.hh"
#include "globals.hh"
#include "G4SteppingManager.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4DynamicParticle.hh"

#include "G4VTouchable.hh"


IsotopeSteppingAction::IsotopeSteppingAction(IsotopeEventAction* myEA):eventAction(myEA)
{ }



IsotopeSteppingAction::~IsotopeSteppingAction()
{ }



void IsotopeSteppingAction::UserSteppingAction(const G4Step* aStep) 
{  
  G4Track* theTrack =  aStep->GetTrack();  





  
  
  G4String Name = aStep-> GetTrack()->GetDefinition()->GetParticleName();
  
  G4ThreeVector pos = aStep->GetPreStepPoint()-> GetPosition()/CLHEP::mm;
  
  const G4VTouchable* touchable = aStep->GetTrack()->GetOriginTouchable();
  G4double e ;


// make sure this is not the first track (the first track is the creation of the atom that will decay)
if (theTrack->GetTrackID() != 1){

// make sure this is the first step, we only want to do this part once per event
  if(Name=="e+"){
  if (theTrack->GetCurrentStepNumber() == 1) {
    
// make sure that the particle originated from radioactive decay  
    if (theTrack->GetCreatorProcess()->GetProcessName() == "RadioactiveDecayBase")
      {
        
//and make sure that this isnt a nucleus...      	 
      	if(theTrack->GetDefinition()->GetParticleType() != "nucleus") {
          
//make sure this is a positron and then check if it originated from the artery or the vein      	  
      	    
// add the positron energy to the histogram, should only happen once per event
          G4double en = aStep->GetPreStepPoint()->GetKineticEnergy();
             eventAction->SetPositron(en/CLHEP::keV);
              
               if (theTrack->GetOriginTouchable()->GetVolume()->GetName() =="arm_source_phys"){
                           
                eventAction->SetTracker(1);
               }
               else if(theTrack->GetOriginTouchable()->GetVolume()->GetName()=="vein_source_phys"){
                           
                eventAction->SetTracker(2);
               }
               else{eventAction->SetTracker(3);
                 
  }
         	  }}
          
      	}
            }
        }
          
          
      // make sure that the post-step point is not nothing..avoid seg faults 
     


     

/*if((Name="opticalphoton") && (aStep->GetPostStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetName() == "PMT_logical")){
    eventAction->Add_photons(aStep->GetPostStepPoint()->GetPhysicalVolume()->GetName());
    std::string pmt = aStep->GetPostStepPoint()->GetPhysicalVolume()->GetName();
    std::string small= "0_Ch";
    std::string small_2 = "1_Ch";
        if(pmt.compare(0,small.length(),small)==0){
          eventAction->AddPMT_1_photons();
          eventAction->Add_times_1(theTrack->GetGlobalTime());
        }
        else if(pmt.compare(0,small.length(),small_2)==0){
        eventAction->AddPMT_2_photons();
        eventAction->Add_times_2(theTrack->GetGlobalTime());

      }

    theTrack->SetTrackStatus(fStopAndKill);
      } */
          
  


  
   
}

  
  

  


