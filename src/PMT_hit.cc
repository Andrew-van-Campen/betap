
#include "PMT_hit.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"

G4ThreadLocal G4Allocator<PMT_hit>* PMT_hitAllocator;

PMT_hit::PMT_hit()
: G4VHit(),
  PMT_name("")
{;}

PMT_hit::~PMT_hit()
{
 
}

PMT_hit::PMT_hit(const PMT_hit& right)
: G4VHit(),
  PMT_name(right.PMT_name)
{}


const PMT_hit& PMT_hit::operator=(const PMT_hit &right)
{
  PMT_name = right.PMT_name;
  return *this;
  
}

G4bool PMT_hit::operator==(const PMT_hit& right) const
{
  return ( this == &right ) ? true : false;
}

void PMT_hit::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
    {
      
    }
  
}




void PMT_hit::Print()
{;}


// This is a forward declarations of an instantiated G4Allocator<Type> object.
// It has been added in order to make code portable for the GNU g++ 
// (release 2.7.2) compiler. 
// Whenever a new Type is instantiated via G4Allocator, it has to be forward
// declared to make object code (compiled with GNU g++) link successfully. 
// 
#ifdef GNU_GCC
  template class G4Allocator<PMT_hit>;
#endif










