#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"

#include "DetectorConstruction.hh"
#include "DetectorMessenger.hh"

#include "PatientMaterial.hh"
#include "SourceMaterial.hh"
#include "Phantom.hh"

#include "G4PhantomParameterisation.hh"
#include "G4PVParameterised.hh"

#include "G4Version.hh"

DetectorConstruction::DetectorConstruction()
    : SourceChoice(0), Source(0),
      World(0), WorldLog(0), WorldPhys(0)
{
    worldSizeX = 5.0 * m;
    worldSizeY = 5.0 * m;
    worldSizeZ = 5.0 * m;

    phantomFilename = "";
    detectorMessenger = new DetectorMessenger(this);

    pMaterial = new PatientMaterial();
    phantomLoaded = false;
}

DetectorConstruction::~DetectorConstruction()
{
    delete pMaterial;
    delete detectorMessenger;

    if (phantomLoaded)
    {
        delete phantom;
    }
}

void DetectorConstruction::setPhantFilename(G4String filename)
{
    phantomFilename = filename;
}

G4VPhysicalVolume *DetectorConstruction::Construct()
{
    ConstructPhantom();

    return WorldPhys;
}

void DetectorConstruction::ConstructPhantom()
{
    G4Material *air = pMaterial->GetMat("G4_AIR");

    // World volume
    World = new G4Box("World", worldSizeX, worldSizeY, worldSizeZ);
    WorldLog = new G4LogicalVolume(World, air, "World", 0, 0, 0);
    WorldPhys = new G4PVPlacement(0, G4ThreeVector(), "World", WorldLog, 0, false, 0);
    WorldLog->SetVisAttributes(G4VisAttributes::Invisible);
    InitPhantom();
}

void DetectorConstruction::InitPhantom()
{
    if (phantomFilename.size() == 0)
        return;

    this->phantom = new Phantom();
    this->phantom->loadFromEGSphant(phantomFilename);

    XMin = this->phantom->getXmin();
    YMin = this->phantom->getYmin();
    ZMin = this->phantom->getZmin();

    XMax = this->phantom->getXmax();
    YMax = this->phantom->getYmax();
    ZMax = this->phantom->getZmax();

    NumBinX = this->phantom->getNumVoxelsX();
    NumBinY = this->phantom->getNumVoxelsY();
    NumBinZ = this->phantom->getNumVoxelsZ();

    pixel_spacing_X = this->phantom->getXsize();
    pixel_spacing_Y = this->phantom->getYsize();
    pixel_spacing_Z = this->phantom->getZsize();

    totalVox = NumBinX * NumBinY * NumBinZ;

    matNames = this->phantom->getMatNames();

   G4Material *air = pMaterial->GetMat("G4_AIR");

    std::vector<G4Material *> theMaterials = this->phantom->getAllMaterials();
    size_t *materialNumbers = this->phantom->getMatNumbers();

    G4ThreeVector ThePosition;

    G4Box *phantomVoxel_vol = new G4Box("VoxelBox", pixel_spacing_X / 2., pixel_spacing_Y / 2., pixel_spacing_Z / 2.);
    G4LogicalVolume *phantomVoxel_log = new G4LogicalVolume(phantomVoxel_vol, air, "VoxelLog", 0, 0, 0);
    //phantomVoxel_log->SetVisAttributes(G4VisAttributes::Invisible);

    G4double X = XMax - XMin;
    G4double Y = YMax - YMin;
    G4double Z = ZMax - ZMin;

    G4double centerX = XMin + (X / 2.);
    G4double centerY = YMin + (Y / 2.);
    G4double centerZ = ZMin + (Z / 2.);

    G4double halfSizeX = X / 2.;
    G4double halfSizeY = Y / 2.;
    G4double halfSizeZ = Z / 2.;

    G4Box *PatientRealBox = new G4Box("PatientMatBox", halfSizeX, halfSizeY, halfSizeZ);
    G4LogicalVolume *PatientRealBoxLV = new G4LogicalVolume(PatientRealBox, air, "PatientMatBoxLV");
    G4VPhysicalVolume *PatientRealBoxPV = new G4PVPlacement(0,
                                                            G4ThreeVector(centerX, centerY, centerZ),
                                                            PatientRealBoxLV,
                                                            "PatientRealBoxPV",
                                                            WorldLog, false, 0);

    G4PhantomParameterisation *param = new G4PhantomParameterisation();
    param->SetVoxelDimensions(pixel_spacing_X / 2., pixel_spacing_Y / 2., pixel_spacing_Z / 2.);
    param->SetNoVoxel(NumBinX, NumBinY, NumBinZ);
    param->SetMaterials(theMaterials);
    param->SetMaterialIndices(materialNumbers);
    param->SetSkipEqualMaterials(false);
    param->BuildContainerSolid(PatientRealBoxPV);
    param->CheckVoxelsFillContainer(PatientRealBox->GetXHalfLength(), PatientRealBox->GetYHalfLength(), PatientRealBox->GetZHalfLength());

    // If Geant4 version >= 10.02.p03  use kDefined
    // If 10.02.p02 or less use kXAxis
    #if G4VERSION_NUMBER >= 1023
      G4PVParameterised *phantom_phys = new G4PVParameterised("phantom_phys_real", phantomVoxel_log, PatientRealBoxLV, kUndefined, param->GetNoVoxel(), param);
    #else
      G4PVParameterised *phantom_phys = new G4PVParameterised("phantom_phys_real", phantomVoxel_log, PatientRealBoxLV, kXAxis, param->GetNoVoxel(), param);
      phantom_phys->SetRegularStructureId(1); // This needs to be used if using kXAxis or kYAxis
    #endif

    G4cout << "Num Voxels: (" << NumBinX << ", " << NumBinY << ", " << NumBinZ << ")" << G4endl;
    G4cout << "Top left coordinates: (" << XMin << ", " << YMin << ", " << ZMin << ")" << G4endl;
    G4cout << "Voxel Spacing: (" << pixel_spacing_X << ", " << pixel_spacing_Y << ", " << pixel_spacing_Z << ")" << G4endl;
    this->phantomLoaded = true;
}

void DetectorConstruction::PrintDetectorParameters()
{
    G4cout << "----------------" << G4endl
           << "the world is a box with the size: " << G4endl
           << worldSizeX * 2. / cm
           << " cm * "
           << worldSizeY * 2. / cm
           << " cm * "
           << worldSizeZ * 2. / cm
           << " cm" << G4endl
           << "The world is made of "
           << "----------------"
           << G4endl;
}
