
#include "PMT_SD.hh"
#include "PMT_hit.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "IsotopeEventAction.hh"

PMT_SD::PMT_SD(const G4String& name,
                                    const G4String& Hcname)
:G4VSensitiveDetector(name),
HCID(-1),
Collection(NULL)

{
  
  collectionName.insert(Hcname);
}
PMT_SD::~PMT_SD()
{

}



void PMT_SD::Initialize(G4HCofThisEvent* HCE)
{
  // Create hits collection

  Collection = new PMTHitsCollection(SensitiveDetectorName,collectionName[0]);
  
  // Add this collection in HCE
  if(HCID<0)
    {
      HCID=G4SDManager::GetSDMpointer() -> GetCollectionID(Collection); 
    }
  HCE->AddHitsCollection(HCID,Collection);

}





G4bool PMT_SD::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
  //G4Track* aTrack = aStep -> GetTrack();

  
  if((aStep-> GetTrack()->GetDefinition()->GetParticleName()=="opticalphoton") && (aStep->GetPostStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetName() == "PMT_logical"))
  {
      
  PMT_hit* newHit = new PMT_hit();
  newHit -> SetName(aStep->GetPostStepPoint()->GetPhysicalVolume()->GetName());
  Collection->insert( newHit );
  return true;}
  return false;
}

void PMT_SD::EndOfEvent(G4HCofThisEvent* HCE)
{;}
void PMT_SD::clear()
{;}
void PMT_SD:: DrawAll()
{;}
void PMT_SD::PrintAll()
{;}






