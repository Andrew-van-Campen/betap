
#include "SensitiveDetector.hh"
#include "DetectorHit.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "IsotopeEventAction.hh"
#include "G4EventManager.hh"
SensitiveDetector::SensitiveDetector(const G4String& name,
                                    const G4String& Hcname)
:G4VSensitiveDetector(name),
HCID(-1),
Collection(NULL)

{
  
  collectionName.insert(Hcname);
}
SensitiveDetector::~SensitiveDetector()
{

}



void SensitiveDetector::Initialize(G4HCofThisEvent* HCE)
{
  // Create hits collection

  Collection = new DetectorHitsCollection(SensitiveDetectorName,collectionName[0]);
  
  // Add this collection in HCE
  if(HCID<0)
    {
      HCID=G4SDManager::GetSDMpointer() -> GetCollectionID(Collection); 
    }
  HCE->AddHitsCollection(HCID,Collection);

}





G4bool SensitiveDetector::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{

  G4double edep = aStep -> GetTotalEnergyDeposit()/CLHEP::keV;
  if(edep==0.){
      return false;}
  DetectorHit* newHit = new DetectorHit();
  
  newHit -> SetEdep(edep);
  
  newHit -> SetPos(aStep -> GetPreStepPoint() -> GetPosition()); 	
  
//  newHit -> SetParticle(aStep-> GetTrack()->GetDefinition()->GetParticleName());
IsotopeEventAction* evt = (IsotopeEventAction*)G4EventManager::GetEventManager()->GetUserEventAction();
  
  newHit -> SetScint_name(aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName());

  if(evt->GetAnnh()){

    newHit-> SetIs_annh(true);

  }
  else{
    newHit-> SetIs_annh(false);
    
  }

  



  Collection->insert( newHit );
  return true;
}

void SensitiveDetector::EndOfEvent(G4HCofThisEvent* HCE)
{;}
void SensitiveDetector::clear()
{;}
void SensitiveDetector:: DrawAll()
{;}
void SensitiveDetector::PrintAll()
{;}






