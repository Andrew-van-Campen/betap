#include "G4ios.hh"

#include "IsotopeEventAction.hh"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4UImanager.hh"
#include "G4UnitsTable.hh"
#include "SensitiveDetector.hh"
#include "DetectorHit.hh"
#include "botDetectorHit.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"

#include "Analysis.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"
#include "PMT_hit.hh"

using std::array;
using std::vector;

namespace {

// Utility function which finds a hit collection with the given Id
// and print warnings if not found 
G4VHitsCollection* GetHC(const G4Event* event, G4int collId) {
  auto hce = event->GetHCofThisEvent();
  if (!hce) {
      G4ExceptionDescription msg;
      msg << "No hits collection of this event found." << G4endl; 
      G4Exception("IsotopeEventAction::EndOfEventAction()",
                  "Code001", JustWarning, msg);
      return nullptr;
  }

  auto hc = hce->GetHC(collId);
  if ( ! hc) {
    G4ExceptionDescription msg;
    msg << "Hits collection " << collId << " of this event not found." << G4endl; 
    G4Exception("B5EventAction::EndOfEventAction()",
                "B5Code001", JustWarning, msg);
  }
  return hc;  
}

}

extern G4bool drawEvent;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
IsotopeEventAction::IsotopeEventAction(IsotopeRunAction* aRun)
// get's passed the current IsotopeRunAction
: IsotopeRun(aRun),
  drawFlag("all"),
  fCollID(-1)
{
;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
//Destructor
IsotopeEventAction::~IsotopeEventAction()
{
  ;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
//This occurs at the beginning of each event, gets passed the current event
void IsotopeEventAction::BeginOfEventAction(const G4Event* evt)
{
  
//sets all variables to zero

SetTracker(0);
SetEventEnergy(0); 
G4double e = 0;
G4double ep = 0;
G4double LYSOe = 0;
G4double LYSOep = 0;
G4double botDete = 0;
G4double botDetep = 0;
//ensure that each event starts with the annihilation tracker at False
SetAnnh(false);


is_botHit = 0;

PosX = 0;
PosY = 0;
PosZ = 0;
PosXP = 0;
PosYP = 0;
PosZP = 0;

LYSOPosX = 0;
LYSOPosY = 0;
LYSOPosZ = 0;
LYSOPosXP = 0;
LYSOPosYP = 0;
LYSOPosZP = 0;

botDetPosX = 0;
botDetPosY = 0;
botDetPosZ = 0;
botDetPosXP = 0;
botDetPosYP = 0;
botDetPosZP = 0;
SetVenTracker(0);
SetArtTracker(0);


 }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void IsotopeEventAction::EndOfEventAction(const G4Event* evt)
{ 
  

  G4int EvtNb = evt -> GetEventID();
  // get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();

  G4double e = 0;
  G4double ep = 0;
  G4double LYSOe =0;
  G4double LYSOep =0;
  G4double botDete = 0;
  G4double botDetep = 0;
  G4String PMT_name = "";

  //The following 4 variable are used to keep track of the scintillator channel that the detector would record for photons and positrons individually
  G4String positron_name = "";
  G4String photon_name = "";
  G4double pos_e = 0;
  G4double pho_e = 0;
  // check if event originated from the artery 
  // or the vein and fill nTuple/histograms appropriately
 
  //   Hits collection IDs
     G4SDManager* SDMan = G4SDManager::GetSDMpointer();
    fCollID = SDMan ->GetCollectionID("ScintHC");
    PMTID = SDMan ->GetCollectionID("PMTHC");
    botID = SDMan ->GetCollectionID("botDetHC");
    LYSO_ID = SDMan ->GetCollectionID("LYSODetHC");
  

    
//loop through scint hits}

    auto hc = GetHC(evt, fCollID);
    auto LYSOhc = GetHC(evt,LYSO_ID);
    auto botDethc = GetHC(evt,botID);
    int rightDet = 0;
    int leftDet = 0;
    //printf("bot: %i\n",botDethc->GetSize() );
    //printf("LYSO: %i\n",LYSOhc->GetSize() );
    if (!hc&&!LYSOhc&&!botDethc ) return;
    int counter = 0;
    int counterP = 0;
    for (unsigned int i =0; i<hc->GetSize();i++){
      
      auto hit = static_cast<DetectorHit*>(hc->GetHit(i));
      G4String detName = hit->GetScint_name();
      G4int intName = std::stoi(detName);
      if((intName<33) && (intName > -33)){
      if (hit->GetIs_annh())
      {
        ep = ep+hit->GetEdep();
        counterP = counterP+1;
        PosXP = PosXP+ (hit->GetPos()).getX();
        PosYP = PosYP+ (hit->GetPos()).getY();
        PosZP = PosZP+ (hit->GetPos()).getZ();
        if (hit->GetEdep()> pos_e){
          pos_e = hit->GetEdep();
          photon_name = hit->GetScint_name();
        }

      }

      else{
      e = e+hit->GetEdep(); 
      counter = counter +1;
      PosX = PosX+ (hit->GetPos()).getX();
      PosY = PosY+ (hit->GetPos()).getY();
      PosZ = PosZ+ (hit->GetPos()).getZ();
      if (hit->GetEdep()> pho_e){
          pho_e = hit->GetEdep();
          positron_name =detName;
        }
    }
      
    }
    else{
      if(intName>0){
        rightDet = 1;
      }
      else{
        leftDet = 1;
      }
    }
  }

      //get the average of all hit locations. 
      PosX = PosX/counter;
      PosY = PosY/counter;
      PosZ = PosZ/counter;


      PosXP = PosXP/counterP;
      PosYP = PosYP/counterP;
      PosZP = PosZP/counterP;



//loop through LYSO and botDet hits
     hc = GetHC(evt, botID);

    //if (!hc) return;
     counter = 0;
     counterP = 0;
    int botcounter = 0;
    int botcounterP = 0;
    for (unsigned int i =0; i<hc->GetSize();i++){
      auto hit = static_cast<botDetectorHit*>(hc->GetHit(i));
      if (hit->GetHit()==true){
      if (hit->GetIs_annh())
      {

        botDetep = botDetep+hit->GetEdep();
        botcounterP = botcounterP+1;
        botDetPosXP = botDetPosXP+ (hit->GetPos()).getX();
        botDetPosYP = botDetPosYP+ (hit->GetPos()).getY();
        botDetPosZP = botDetPosZP+ (hit->GetPos()).getZ();
        

      }

      else{

      botDete = botDete+hit->GetEdep(); 
      botcounter = botcounter +1;
      botDetPosX = botDetPosX+ (hit->GetPos()).getX();
      botDetPosY = botDetPosY+ (hit->GetPos()).getY();
      botDetPosZ = botDetPosZ+ (hit->GetPos()).getZ();
      
      

    }}

    else{      
      if (hit->GetIs_annh())
      {
        LYSOep = LYSOep+hit->GetEdep();
        counterP = counterP+1;
        LYSOPosXP = LYSOPosXP+ (hit->GetPos()).getX();
        LYSOPosYP = LYSOPosYP+ (hit->GetPos()).getY();
        LYSOPosZP = LYSOPosZP+ (hit->GetPos()).getZ();
        

      }

      else{
      LYSOe = LYSOe+hit->GetEdep(); 
      counter = counter +1;
      LYSOPosX = LYSOPosX+ (hit->GetPos()).getX();
      LYSOPosY = LYSOPosY+ (hit->GetPos()).getY();
      LYSOPosZ = LYSOPosZ+ (hit->GetPos()).getZ();
      
      

    }


    }
}
      //get the average of all bot hit locations. 
      botDetPosX = botDetPosX/botcounter;
      botDetPosY = botDetPosY/botcounter;
      botDetPosZ = botDetPosZ/botcounter;


      botDetPosXP = botDetPosXP/botcounterP;
      botDetPosYP = botDetPosYP/botcounterP;
      botDetPosZP = botDetPosZP/botcounterP;



      //get the average of all LYSO hit locations. 
      LYSOPosX = LYSOPosX/counter;
      LYSOPosY = LYSOPosY/counter;
      LYSOPosZ = LYSOPosZ/counter;


      LYSOPosXP = LYSOPosXP/counterP;
      LYSOPosYP = LYSOPosYP/counterP;
      LYSOPosZP = LYSOPosZP/counterP;

// loop through PMT hits
 /*    hc = GetHC(evt, PMTID);
    if (!hc) return;
    for (unsigned int i =0; i<hc->GetSize();i++){
      auto hit = static_cast<PMT_hit*>(hc->GetHit(i));
      PMT_name = hit->GetName(); 
      IsotopeRun->Add_photons(PMT_name);
      }

   */ 

  

 

  //if (GetTracker()!=0){
        if((e!=0) ||(ep!=0) || LYSOe != 0 || LYSOep !=0 || botDete !=0 || botDetep !=0||rightDet!=0||leftDet!=0){
    analysisManager->FillNtupleDColumn(5,GetTracker());
    analysisManager->FillNtupleDColumn(1,ep);
    analysisManager->FillNtupleDColumn(0,e);
    analysisManager->FillNtupleDColumn(2,PosX);
    analysisManager->FillNtupleDColumn(3,PosY);
    analysisManager->FillNtupleDColumn(4,PosZ);
    analysisManager->FillNtupleDColumn(7,PosXP);
    analysisManager->FillNtupleDColumn(8,PosYP);
    analysisManager->FillNtupleDColumn(9,PosZP);
    if(positron_name!=""){
    analysisManager->FillNtupleFColumn(10,std::stof(positron_name));}
    if(photon_name!=""){
    analysisManager->FillNtupleFColumn(11,std::stof(photon_name));}
    analysisManager->FillNtupleDColumn(30,EvtNb);
  
    analysisManager->FillNtupleDColumn(13,LYSOep);
    analysisManager->FillNtupleDColumn(12,LYSOe);
    analysisManager->FillNtupleDColumn(14,LYSOPosX);
    analysisManager->FillNtupleDColumn(15,LYSOPosY);
    analysisManager->FillNtupleDColumn(16,LYSOPosZ);
    analysisManager->FillNtupleDColumn(17,LYSOPosXP);
    analysisManager->FillNtupleDColumn(18,LYSOPosYP);
    analysisManager->FillNtupleDColumn(19,LYSOPosZP);

    analysisManager->FillNtupleDColumn(21,botDetep);
    analysisManager->FillNtupleDColumn(20,botDete);
    analysisManager->FillNtupleDColumn(22,botDetPosX);
    analysisManager->FillNtupleDColumn(23,botDetPosY);
    analysisManager->FillNtupleDColumn(24,botDetPosZ);
    analysisManager->FillNtupleDColumn(25,botDetPosXP);
    analysisManager->FillNtupleDColumn(26,botDetPosYP);
    analysisManager->FillNtupleDColumn(27,botDetPosZP);
    
    analysisManager->FillNtupleIColumn(28,rightDet);
    analysisManager->FillNtupleIColumn(29,leftDet);

    
    analysisManager->AddNtupleRow();
    
    }
//}
	
  




if(EvtNb%1000==0)
  {
  
  //printf("%d\n", EvtNb);
  G4cout<<">>>>Event nr:"<<EvtNb<<G4endl;
  
   }
  


} 

void IsotopeEventAction::SetPositron(G4double energy)
{
  auto analysisManager = G4AnalysisManager::Instance();
  
  analysisManager->FillNtupleDColumn(6,energy);
}

void IsotopeEventAction::SetGamma(G4double energy)
{
 }

void IsotopeEventAction::AddArtPosition(G4ThreeVector xyz)
{ 
  AddArtTracker();
  
   PosArtX += xyz.getX();
   PosArtY += xyz.getY();
   PosArtZ += xyz.getZ();
 

}

void IsotopeEventAction::AddVenPosition(G4ThreeVector xyz)
{ 
  AddVenTracker();
   PosVenX += xyz.getX();
   PosVenY += xyz.getY();
   PosVenZ += xyz.getZ();
}

/*void IsotopeEventAction::SetDeltaT(){



DeltaT = ((*times_1)[0]-(*times_2)[0]);
};

void IsotopeEventAction::Add_times_1(G4double d){
times_1->push_back(d);

};
void IsotopeEventAction::Add_times_2(G4double d){
times_2->push_back(d);
};

void IsotopeEventAction::clear_times(){
  times_1->clear(); 
  times_2->clear();
  times_1->reserve(200); 
  times_2->reserve(200);
  
   }; */
//void IsotopeEventAction::sort_times(){

  //sort(times_1->begin(),times_1->end()); 
  //sort(times_2->begin(),times_2->end());};

//void IsotopeEventAction::Add_photons(G4String pmt){
 // IsotopeRun->Add_photons(pmt);
 // }



