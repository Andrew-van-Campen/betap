
#include "botDetectorHit.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"

G4ThreadLocal G4Allocator<botDetectorHit>* botDetectorHitAllocator;

botDetectorHit::botDetectorHit()
: G4VHit(),
  is_hit(false)
{;}

botDetectorHit::~botDetectorHit()
{
 
}

botDetectorHit::botDetectorHit(const botDetectorHit& right)
: G4VHit(),
  edep(right.edep),
  is_annh(right.is_annh),
  pos(right.pos)
{}


const botDetectorHit& botDetectorHit::operator=(const botDetectorHit &right)
{
  
  edep = right.edep;
  pos = right.pos;
  is_annh = right.is_annh;
  return *this;
  
}

G4bool botDetectorHit::operator==(const botDetectorHit& right) const
{
  return ( this == &right ) ? true : false;
}

void botDetectorHit::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
    {
      
    }
  
}




void botDetectorHit::Print()
{;}


// This is a forward declarations of an instantiated G4Allocator<Type> object.
// It has been added in order to make code portable for the GNU g++ 
// (release 2.7.2) compiler. 
// Whenever a new Type is instantiated via G4Allocator, it has to be forward
// declared to make object code (compiled with GNU g++) link successfully. 
// 
#ifdef GNU_GCC
  template class G4Allocator<botDetectorHit>;
#endif










