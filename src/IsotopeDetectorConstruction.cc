#include "IsotopeDetectorConstruction.hh"

#include "G4UImanager.hh"
#include "G4Box.hh"
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "Randomize.hh"
#include "G4RandomDirection.hh"
#include "G4NistManager.hh"
#include "G4Tubs.hh"
#include "G4RotationMatrix.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4UserLimits.hh"
#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "IsotopeDetectorMessenger.hh"
#include  "G4PVReplica.hh"
#include "G4RunManager.hh"

#include "G4SDManager.hh"
#include "G4SDChargedFilter.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSEnergyDeposit.hh"
#include "G4PSTrackLength.hh"
#include "SensitiveDetector.hh"
#include "PMT_SD.hh"
#include "math.h"
#include "botDetector.hh"

IsotopeDetectorConstruction::IsotopeDetectorConstruction(G4String in, G4String sep, G4String dep, G4String phant_filename)
    : fDetectorMessenger(0)

{
    geo = in;

    depth = std::stoi(dep);
    separation = std::stoi(sep);


    sourceRadius = 1.15 * mm;
    halfSourceHeight =5.* cm;

    bodyRadius = 60 * mm;
    halfBodyHeight = 20 *cm;

    phantomRadius= 32.65*mm;
    halfPhantomHeight=10.0*cm;

    LYSO_x = 15*mm;
    LYSO_y = phantomRadius;
    LYSO_z = halfPhantomHeight;
    if (geo!="LYSO"){
        LYSO_x = 0.001*mm;
        LYSO_y = 0.001;
        LYSO_z = 0.001;
    }
    printf(geo );
    Al_x = 1.5*mm;
    Al_y = phantomRadius;
    Al_z = halfPhantomHeight;

    cutInnerRad = 0.0*mm;
    cutOuterRad = 1.0*mm;
    halfCutHeight = 10.0 * cm;




    detectorInnerRadius = 0.*mm ;
    detectorOuterRadius = 0.97/2*mm ;
    halfDetectorLength = 5.0 *cm;

    claddingInnerRadius = 0.97/2*mm; 
    CladdingOuterRadius = 1.0/2*mm; 
    halfCladdingLength =5.0 *cm ;

    wrapInnerRadius = 1.0/2*mm; 
    WrapOuterRadius = 1.0254/2*mm; 
    halfWrapLength =5.0 *cm ;

    PMT_inner = 0*cm;
    PMT_outer = 1.0254/2*mm;
    PMT_z = 5 *mm;

    startPhi = 0.;
    spanPhi = 2. * CLHEP::pi;
    spanTheta = CLHEP::pi / 2.;

    shellInnerRadius = phantomRadius+4*WrapOuterRadius;
    shellOuterRadius = shellInnerRadius+1.0*cm;

    angle = CLHEP::pi/2- acos(1-(separation*separation)/(2*(phantomRadius-cutOuterRad-depth)*(phantomRadius-cutOuterRad-depth)));


    fDetectorMessenger = new IsotopeDetectorMessenger(this);

}

IsotopeDetectorConstruction::~IsotopeDetectorConstruction()
{
    delete fDetectorMessenger ;
}

G4VPhysicalVolume* IsotopeDetectorConstruction::Construct()
{





    G4NistManager* man = G4NistManager::Instance();
    G4Element*  H  = man->FindOrBuildElement("H");
    //G4Element*  Fe = man->FindOrBuildElement("Fe");
    //G4Element*  Cr =man->FindOrBuildElement("Cr");
    //G4Element*  Ni = man->FindOrBuildElement("Ni");
    //G4Element*  Mn = man->FindOrBuildElement("Mn");
    G4Element*  Si = man->FindOrBuildElement("Si");
    G4Element*  C  = man->FindOrBuildElement("C");
    G4Element*  O  = man->FindOrBuildElement("O");
    G4Element*  Al  = man->FindOrBuildElement("Al");
    G4Element*  Lu  = man->FindOrBuildElement("Lu");
    G4Element*  Y  = man->FindOrBuildElement("Y");



    //G4Material* Ir = man->FindOrBuildMaterial("G4_Ir");
    //G4Material* Se = man->FindOrBuildMaterial("G4_Se");
    //G4Material* Gd = man->FindOrBuildMaterial("G4_Gd");
    //G4Material* I = man->FindOrBuildMaterial("G4_I");
    //G4Material* Yb = man->FindOrBuildMaterial("G4_Yb");
    //G4Material* W = man->FindOrBuildMaterial("G4_W");
    //G4Material* Pb = man->FindOrBuildMaterial("G4_Pb");
    //G4Material* Sn = man->FindOrBuildMaterial("G4_Sn");
    G4Material* air = man->FindOrBuildMaterial("G4_AIR");
    G4Material* air_PMT = man->FindOrBuildMaterial("G4_AIR");
    G4Material* water = man->FindOrBuildMaterial("G4_WATER");
    //G4Material* Ti = man->FindOrBuildMaterial("G4_Ti");
    //G4Material* pt = man->FindOrBuildMaterial("G4_Pt");
    G4Material* Polystyrene = man->FindOrBuildMaterial("G4_POLYSTYRENE");
    G4Material* NaI = man->FindOrBuildMaterial("G4_SODIUM_IODIDE");
    // Stainless steel
    /*G4Material* steel = new G4Material( "Steel", 8.0*CLHEP::g/CLHEP::cm3, 6 );
      steel->AddElement( Fe, 0.6792 );
      steel->AddElement( Cr, 0.1900 );
      steel->AddElement( Ni, 0.1000 );
      steel->AddElement( Mn, 0.0200 );
      steel->AddElement( Si, 0.0100 );
      steel->AddElement( C , 0.0008 );
      */
    // Plastic scintillator used for detector


    //G4Material* Polystyrene = new G4Material("Polystyrene", 1.05*g/cm3, 2);
    //Polystyrene->AddElement(C, 1);
    // Polystyrene->AddElement(H, 1);

    G4Material* Acrylic = new G4Material("Acrylic", 1.18*g/cm3, 3);
    Acrylic->AddElement(C, 5);
    Acrylic->AddElement(H, 8);
    Acrylic->AddElement(O, 2);

    G4Material* PET = new G4Material("PET", 1.38*g/cm3, 3);
    PET->AddElement(C, 10);
    PET->AddElement(H, 8);
    PET->AddElement(O, 4);

    G4Material* LYSO = new G4Material("LYSO",7.1*g/cm3,4);
    LYSO->AddElement(Lu,9);
    LYSO->AddElement(Y,1);
    LYSO->AddElement(Si,5);
    LYSO->AddElement(O,25);

    G4Material* PLA = new G4Material("PLA",1.3*g/cm3,3);
    PLA->AddElement(C,3);
    PLA->AddElement(H,4);
    PLA->AddElement(O,2);


    //Material Tables
    G4double pst_Energy[] = {2.254*eV,2.4796*eV,2.5829*eV,2.85*eV,3.0995*eV};
    const G4int pstnum = sizeof(pst_Energy)/sizeof(G4double);

    G4double pst_SCINT[] = {0.02,0.22,0.4,1,0.1};
    G4double pst_RIND[]  = { 1.60 , 1.60, 1.60,1.60,1.60};
    assert(sizeof(pst_RIND) == sizeof(pst_Energy));
    G4double pst_ABSL[]  = { 270*cm, 270*cm, 270*cm};
    assert(sizeof(pst_ABSL) == sizeof(pst_Energy));
    fpst_mt = new G4MaterialPropertiesTable();
    fpst_mt->AddProperty("FASTCOMPONENT", pst_Energy, pst_SCINT, pstnum);
    fpst_mt->AddProperty("SLOWCOMPONENT", pst_Energy, pst_SCINT, pstnum);
    fpst_mt->AddProperty("RINDEX",        pst_Energy, pst_RIND,  pstnum);
    fpst_mt->AddProperty("ABSLENGTH",     pst_Energy, pst_ABSL,  pstnum);
    fpst_mt->AddConstProperty("SCINTILLATIONYIELD",8000./MeV);
    fpst_mt->AddConstProperty("RESOLUTIONSCALE",1.0);
    fpst_mt->AddConstProperty("FASTTIMECONSTANT",3.2*ns);
    fpst_mt->AddConstProperty("SLOWTIMECONSTANT",45*ns);
    fpst_mt->AddConstProperty("YIELDRATIO",1.0);
    Polystyrene->SetMaterialPropertiesTable(fpst_mt);



    G4double acl_Energy[] = {2.5*eV};
    const G4int aclnum = sizeof(acl_Energy)/sizeof(G4double);

    G4double acl_SCINT[] = {1};
    G4double acl_RIND[]  = {1.42};
    assert(sizeof(acl_RIND) == sizeof(acl_Energy));
    G4double acl_ABSL[]  = {270*cm};
    assert(sizeof(acl_ABSL) == sizeof(acl_Energy));
    facl_mt = new G4MaterialPropertiesTable();
    facl_mt->AddProperty("RINDEX",        acl_Energy, acl_RIND,  aclnum);
    facl_mt->AddProperty("ABSLENGTH",     acl_Energy, acl_ABSL,  aclnum);
    Acrylic->SetMaterialPropertiesTable(facl_mt);


    G4double pet_Energy[] = {2.5*eV};
    const G4int petnum = sizeof(pet_Energy)/sizeof(G4double);

    G4double pet_SCINT[] = {1};
    G4double pet_RIND[]  = {1.575};
    assert(sizeof(pet_RIND) == sizeof(pet_Energy));
    G4double pet_ABSL[]  = {270*cm};
    assert(sizeof(pet_ABSL) == sizeof(pet_Energy));
    fpet_mt = new G4MaterialPropertiesTable();
    fpet_mt->AddProperty("RINDEX",        pet_Energy, pet_RIND,  petnum);
    fpet_mt->AddProperty("ABSLENGTH",     pet_Energy, pet_ABSL,  petnum);
    PET->SetMaterialPropertiesTable(fpet_mt);

    G4double air_Energy[] = {2.5*eV};
    const G4int airnum = sizeof(air_Energy)/sizeof(G4double);

    G4double air_SCINT[] = {1};
    G4double air_RIND[]  = {1.000275};
    assert(sizeof(air_RIND) == sizeof(air_Energy));
    G4double air_ABSL[]  = {270*cm};
    assert(sizeof(air_ABSL) == sizeof(air_Energy));
    fair_mt = new G4MaterialPropertiesTable();
    fair_mt->AddProperty("RINDEX",        air_Energy, air_RIND,  airnum);
    fair_mt->AddProperty("ABSLENGTH",     air_Energy, air_ABSL,  airnum);
    air_PMT->SetMaterialPropertiesTable(fair_mt);


    G4Sphere* World = new G4Sphere("World", 0.*CLHEP::cm, 1*CLHEP::m, 0.*CLHEP::deg, 360.*CLHEP::deg, 0.*CLHEP::deg, 180.*CLHEP::deg);
    World_log = new G4LogicalVolume(World, air, "World log", 0, 0, 0);
    World_phys = new G4PVPlacement(0, G4ThreeVector(0,0,0), "World_phys", World_log, 0, false, 0);

    //Phantom definition

    /*
    G4Tubs *phantom = new G4Tubs("phantom", 0.0 * mm, phantomRadius, halfPhantomHeight, startPhi, spanPhi);
    phantom_log = new G4LogicalVolume(phantom, water, "phantom_log");
    G4VisAttributes* phantom_att = new G4VisAttributes();
    phantom_att->SetColor(1, 0, 0);
    phantom_att->SetVisibility(true);
    // phantom_att->SetForceSolid(true);
    phantom_log->SetVisAttributes(phantom_att);
    physPhantom=new G4PVPlacement(0, G4ThreeVector(0,0,0*m), "phantom_phys",phantom_log, World_phys, false, 0);
    */

    G4double dummy;
    size_t total_vox;
    std::vector<G4String> mats;
    std::string buffer, buffer2;
    std::stringstream convertor;

    G4cout << "Loading phantom: " << phant_filename << G4endl;

    std::ifstream phantom_file(phant_filename);
    if (!phantom_file)
    {
        G4cout << "Could not find " << phant_filename << G4endl;
        exit(-1);
    }
    this->p_filename = phant_filename;
    std::getline(phantom_file, buffer);
    convertor.str(buffer);

    convertor >> num_mats;
    for (int i = 0; i < num_mats; i++)
    {
        std::getline(phantom_file, buffer);
        material_names.push_back(trim(buffer));
    }
    std::getline(phantom_file, buffer); // dummy

    std::getline(phantom_file, buffer); // number of voxels
    sscanf(buffer.c_str(), "%i %i %i", &num_voxels[0], &num_voxels[1], &num_voxels[2]);
    for (int i = 0; i < 3; i++)
    {
        std::getline(phantom_file, buffer);
        sscanf(buffer.c_str(), "%lf %lf %*s", &topleft[i], &dummy);
        voxel_size[i] = dummy - topleft[i];
        topleft[i] *= cm;
        voxel_size[i] *= cm;
    }

    total_vox = num_voxels[0] * num_voxels[1] * num_voxels[2];
    material_ids = new size_t[total_vox];
    real_ids = new size_t[total_vox];
    int big_counter = 0;
    for (int k = 0; k < num_voxels[2]; k++)
    {
        for (int j = 0; j < num_voxels[1]; j++)
        {
            std::getline(phantom_file, buffer); // Material values
            const char *buf_dup = buffer.c_str();
            for (int i = 0; i < num_voxels[0]; i++)
            {
                // If statement handles EGSphant with more than 9 Materials
                // after Material #1-9 it goes A,B,C,.. etc
                if (buf_dup[i] > 64){
                    material_ids[big_counter] = buf_dup[i] - 'A' + 9;
                }else{
                    material_ids[big_counter] = buf_dup[i] - '0' - 1;
                }
                big_counter++;
            }
        }
        std::getline(phantom_file, buffer);
    }

    big_counter = 0;
    densities = new G4double[total_vox];

    for (int k = 0; k < num_voxels[2]; k++)
    {
        for (int j = 0; j < num_voxels[1]; j++)
        {
            std::getline(phantom_file, buffer); // Density values
            std::vector<std::string> densityValues = split(buffer, ' ');
            for (int i = 0; i < num_voxels[0]; i++)
            {
                densities[big_counter] = std::atof(densityValues[i].c_str()) * g / cm3;
                big_counter++;
            }
        }
        std::getline(phantom_file, buffer);
    }

    phantom_file.close();

    this->constructMaterials();

    //PLA shell around the detector. 
    if (geo!="LYSO"){      
        G4Tubs *shell = new G4Tubs('shell',shellInnerRadius,shellOuterRadius,halfDetectorLength,startPhi,spanPhi);
        shellLog = new G4LogicalVolume(shell,PLA,"shellLog");
        G4VisAttributes* shellAtt = new G4VisAttributes();
        shellAtt->SetColor(1,4,8);
        shellAtt->SetVisibility(true);
        shellAtt->SetForceSolid(true);
        shellLog->SetVisAttributes(shellAtt);
        new G4PVPlacement(0, G4ThreeVector(0,0,0*m), "shellPhys",shellLog,World_phys,false,0);
    }

    // Bottom LYSO detector definition

    G4Box *bottom_detector = new G4Box("bottom_detector", LYSO_x,LYSO_y,LYSO_z);

    bot_log = new G4LogicalVolume(bottom_detector,LYSO, "bot_log");

    G4VisAttributes* bot_att = new G4VisAttributes();
    bot_att->SetColor(1, 0, 0);
    bot_att->SetVisibility(true);
    bot_att->SetForceSolid(true);
    bot_log->SetVisAttributes(bot_att);
    botPhysical =  new G4PVPlacement(0, G4ThreeVector(-1*(phantomRadius+LYSO_x),0,0*m), "botPhysical",bot_log, World_phys, false, 0);

    // This next portion is an Al shield designed to stop any positrons that would pass through it. 
    /*
       G4Box *Al_shield = new G4Box("Al_shield", Al_x,Al_y,Al_z);
       AlLog = new G4LogicalVolume(Al_shield,Al, "AlLog");
       G4VisAttributes* Al_shield_att = new G4VisAttributes();
       Al_shield_att->SetColor(1, 0, 0);
       Al_shield_att->SetVisibility(true);
       Al_shield_att->SetForceSolid(true);
       AlLog->SetVisAttributes(Al_shield_att);
       new G4PVPlacement(0, G4ThreeVector(1*(phantomRadius+2*WrapOuterRadius),0,0*m), "Al_shield_phys",AlLog, World_phys, false, 0);

*/
    // Top LYSO detector definition. 

    G4Box *topLYSO = new G4Box("topLYSO",LYSO_x,LYSO_y,LYSO_z);

    topLYSO_log = new G4LogicalVolume(topLYSO,LYSO,"topLYSO_log");

    G4VisAttributes* topLYSO_att = new G4VisAttributes();
    topLYSO_att->SetColor(1, 0, 0);
    topLYSO_att->SetVisibility(true);
    topLYSO_att->SetForceSolid(true);
    topLYSO_log->SetVisAttributes(bot_att);
    // LYSOphysical =  new G4PVPlacement(0, G4ThreeVector((phantomRadius+LYSO_x+2*Al_x),0,0*m), "LYSOphysical",bot_log, World_phys, false, 0);
    LYSOphysical =  new G4PVPlacement(0, G4ThreeVector((phantomRadius+LYSO_x+4*WrapOuterRadius),0,0*m), "LYSOphysical",bot_log, World_phys, false, 0);





    // Geometry of the "Source"
    // Use this for a cylinder source that can be placed in the phantom arm. 

    G4Tubs *arm_source = new G4Tubs("arm_source", 0.0 * mm, cutOuterRad, halfSourceHeight, startPhi, spanPhi);
    arm_source_log = new G4LogicalVolume(arm_source, water, "arm_source_log");
    G4VisAttributes* arm_source_att = new G4VisAttributes();
    arm_source_att->SetColor(1, 0.75, 0.75);
    arm_source_att->SetVisibility(true);
    arm_source_att->SetForceSolid(true);
    arm_source_log->SetVisAttributes(arm_source_att);
    arm_source_phys = new G4PVPlacement(0, G4ThreeVector((phantomRadius-cutOuterRad-depth)* mm, 0 * mm,0*m), "arm_source_phys",arm_source_log, physPhantom, false, 0);

    //This is used for a veinous source when checking the resolution of the detector. 

    G4Tubs *vein_source = new G4Tubs("vein_source", 0.0 * mm, cutOuterRad, halfSourceHeight, startPhi, spanPhi);
    vein_source_log = new G4LogicalVolume(vein_source, water, "vein_source_log");
    G4VisAttributes* vein_source_att = new G4VisAttributes();
    vein_source_att->SetColor(0, 1, 0);
    vein_source_att->SetVisibility(true);
    vein_source_att->SetForceSolid(true);
    vein_source_log->SetVisAttributes(vein_source_att);
    //vein_source_phys = new G4PVPlacement(0, G4ThreeVector(cos(1 / 2 * CLHEP::pi/4) * 97 * mm, (sin( 1/ 2 * CLHEP::pi/4) * 97) * mm,0.6*m), "vein_source_phys",vein_source_log, World_phys, false, 0);
    //vein_source_phys = new G4PVPlacement(0, G4ThreeVector(sqrt(((phantomRadius-cutOuterRad-depth)*(phantomRadius-cutOuterRad-depth))-(separation*separation)) * mm, separation * mm,0*m), "vein_source_phys",vein_source_log, physPhantom, false, 0);
    vein_source_phys = new G4PVPlacement(0, G4ThreeVector((phantomRadius-cutOuterRad-depth)*sin(angle),(phantomRadius-cutOuterRad-depth)*cos(angle),0*m), "vein_source_phys",vein_source_log, physPhantom, false, 0);

    // This is a basic cylindrical source to represent the body of the patient. 
    G4Tubs *body_source = new G4Tubs("body_source",0.0,bodyRadius,halfBodyHeight,startPhi,spanPhi);
    body_log = new G4LogicalVolume(body_source, water, "body_log");
    G4VisAttributes* body_att = new G4VisAttributes();
    body_att->SetColor(0, 1, 3);
    body_att->SetVisibility(true);
    body_att->SetForceSolid(true);
    body_log->SetVisAttributes(body_att);
    body_phys = new G4PVPlacement(0, G4ThreeVector(phantomRadius+bodyRadius+40*mm,0,0), "body_phys",body_log, World_phys, false, 0);



    G4Tubs* heat_shrink_solid =  new G4Tubs("heat_shrink_solid", wrapInnerRadius, WrapOuterRadius,
            halfDetectorLength, startPhi, spanPhi);
    heat_shrink_logical = new G4LogicalVolume(heat_shrink_solid, Acrylic, "heat_shrink_logical");

    G4VisAttributes *heat_shrink_att = new G4VisAttributes();
    heat_shrink_att->SetColor(0.5, 0.5, 0);
    heat_shrink_att->SetVisibility(true);
    heat_shrink_att->SetForceSolid(true);
    heat_shrink_logical->SetVisAttributes(heat_shrink_att);






    G4Tubs* cladding_solid =  new G4Tubs("cladding_solid", claddingInnerRadius, CladdingOuterRadius,
            halfDetectorLength, startPhi, spanPhi);
    cladding_logical = new G4LogicalVolume(cladding_solid, PET, "cladding_logical");

    G4VisAttributes *cladding_att = new G4VisAttributes();
    cladding_att->SetColor(0, 0.5, 0.5);
    cladding_att->SetVisibility(true);
    cladding_att->SetForceSolid(true);
    cladding_logical->SetVisAttributes(cladding_att);
    /*                              new G4PVPlacement(0,
                                    G4ThreeVector(0,0,0),
                                    cladding_logical,
                                    "cladding_physical",
                                    heat_shrink_logical,
                                    false,
                                    0);
                                    */
    G4Tubs* scint_solid =  new G4Tubs("scint_solid", detectorInnerRadius, detectorOuterRadius,
            halfDetectorLength, startPhi, spanPhi);

    scint_logical = new G4LogicalVolume(scint_solid, Polystyrene, "scint_logical");

    G4VisAttributes *scint_att = new G4VisAttributes();
    scint_att->SetColor(0.5, 0, 0.5);
    scint_att->SetVisibility(true);
    scint_att->SetForceSolid(true);
    scint_logical->SetVisAttributes(scint_att);


    /* scint_physical =                 new G4PVPlacement(0,
       G4ThreeVector(0,0,0),
       scint_logical,
       "scint_physical",
       cladding_logical,
       false,
       0);
       */

    // This is a box placeholder for the light detection component of the PMT

    G4Tubs* PMT_solid = new G4Tubs("PMT_solid", PMT_inner, PMT_outer,
            PMT_z, startPhi, spanPhi);

    PMT_logical = new G4LogicalVolume(PMT_solid, air_PMT, "PMT_logical");


    for(int a=0; a < 15; a++)
    {

        std::string wrap_name = std::to_string(a);
        std::string clad_name =  "Clad" + wrap_name;
        std::string scint_name =  wrap_name;
        std::string PMT_name_0 = "0_Ch_+"+ wrap_name ;
        std::string PMT_name_1 = "1_Ch_+"+ wrap_name ;

        wrap_name = "Wrap" + wrap_name;



        new G4PVPlacement(0,
                G4ThreeVector(33.1627*mm*cos(a*0.03049080341045739),
                    33.1627*mm*sin(a*0.03049080341045739),0),
                heat_shrink_logical,
                wrap_name,
                World_log,
                false,
                0);

        new G4PVPlacement(0,
                G4ThreeVector(33.1627*mm*cos(a*0.03049080341045739),
                    33.1627*mm*sin(a*0.03049080341045739),0),
                cladding_logical,
                clad_name,
                World_log,
                false,
                0);

        new G4PVPlacement(0,
                G4ThreeVector(33.1627*mm*cos(a*0.03049080341045739),
                    33.1627*mm*sin(a*0.03049080341045739),0),
                scint_logical,
                scint_name,
                World_log,
                false,
                0);

        new G4PVPlacement(0,
                G4ThreeVector(33.1627*mm*cos(a*0.03049080341045739),
                    33.1627*mm*sin(a*0.03049080341045739),55*mm),
                PMT_logical,
                PMT_name_0,
                World_log,
                false,
                0);

        new G4PVPlacement(0,
                G4ThreeVector(33.1627*mm*cos(a*0.03049080341045739),
                    33.1627*mm*sin(a*0.03049080341045739),-55*mm),
                PMT_logical,
                PMT_name_1,
                World_log,
                false,
                0);


        if(a!=0)
        {
            int b = -a;
            std::string wrap_name = std::to_string(b);
            std::string clad_name =  "Clad" + wrap_name;
            std::string scint_name =wrap_name;
            std::string PMT_name_0 = "0_Ch_"+ wrap_name ;
            std::string PMT_name_1 = "1_Ch_"+ wrap_name ;
            wrap_name  = "Wrap" + wrap_name;



            new G4PVPlacement(0,
                    G4ThreeVector(33.1627*mm*cos(b*0.03049080341045739),
                        33.1627*mm*sin(b*0.03049080341045739),0),
                    heat_shrink_logical,
                    wrap_name,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(33.1627*mm*cos(b*0.03049080341045739),
                        33.1627*mm*sin(b*0.03049080341045739),0),
                    cladding_logical,
                    clad_name,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(33.1627*mm*cos(b*0.03049080341045739),
                        33.1627*mm*sin(b*0.03049080341045739),0),
                    scint_logical,
                    scint_name,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(33.1627*mm*cos(b*0.03049080341045739),
                        33.1627*mm*sin(b*0.03049080341045739),55*mm),
                    PMT_logical,
                    PMT_name_0,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(33.1627*mm*cos(b*0.03049080341045739),
                        33.1627*mm*sin(b*0.03049080341045739),-55*mm),
                    PMT_logical,
                    PMT_name_1,
                    World_log,
                    false,
                    0);

        }


    }

    if ((geo == "linear_2")||(geo=="LYSO")){
        for(float a=0.5; a < 15; a++)
        {

            std::string wrap_name = std::to_string(a);
            std::string clad_name =  "2_Clad" + wrap_name;
            std::string scint_name =  wrap_name;
            std::string PMT_name_0 = "2_0_Ch_+"+ wrap_name ;
            std::string PMT_name_1 = "2_1_Ch_+"+ wrap_name ;

            wrap_name = "2_Wrap" + wrap_name;



            new G4PVPlacement(0,
                    G4ThreeVector(34.052 *mm*cos(a*0.03049080341045739),
                        34.052 *mm*sin(a*0.03049080341045739),0),
                    heat_shrink_logical,
                    wrap_name,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(34.052 *mm*cos(a*0.03049080341045739),
                        34.052 *mm*sin(a*0.03049080341045739),0),
                    cladding_logical,
                    clad_name,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(34.052 *mm*cos(a*0.03049080341045739),
                        34.052 *mm*sin(a*0.03049080341045739),0),
                    scint_logical,
                    scint_name,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(34.052 *mm*cos(a*0.03049080341045739),
                        34.052 *mm*sin(a*0.03049080341045739),55*mm),
                    PMT_logical,
                    PMT_name_0,
                    World_log,
                    false,
                    0);





            if(a!=0)
            {
                float b = -a;
                std::string wrap_name = std::to_string(b);
                std::string clad_name =  "2_Clad" + wrap_name;
                std::string scint_name =  wrap_name;
                std::string PMT_name_0 = "2_0_Ch_"+ wrap_name ;
                std::string PMT_name_1 = "2_1_Ch_"+ wrap_name ;
                wrap_name  = "2_Wrap" + wrap_name;



                new G4PVPlacement(0,
                        G4ThreeVector(34.052 *mm*cos(b*0.03049080341045739),
                            34.052 *mm*sin(b*0.03049080341045739),0),
                        heat_shrink_logical,
                        wrap_name,
                        World_log,
                        false,
                        0);

                new G4PVPlacement(0,
                        G4ThreeVector(34.052 *mm*cos(b*0.03049080341045739),
                            34.052 *mm*sin(b*0.03049080341045739),0),
                        cladding_logical,
                        clad_name,
                        World_log,
                        false,
                        0);

                new G4PVPlacement(0,
                        G4ThreeVector(34.052 *mm*cos(b*0.03049080341045739),
                            34.052 *mm*sin(b*0.03049080341045739),0),
                        scint_logical,
                        scint_name,
                        World_log,
                        false,
                        0);

                new G4PVPlacement(0,
                        G4ThreeVector(34.052 *mm*cos(b*0.03049080341045739),
                            34.052 *mm*sin(b*0.03049080341045739),55*mm),
                        PMT_logical,
                        PMT_name_0,
                        World_log,
                        false,
                        0);


            }


        }





    }
    if (geo == "linear_2"){
        for(int a=37; a < 67; a++)
        {

            std::string wrap_name = std::to_string(a);
            std::string clad_name =  "Clad" + wrap_name;
            std::string scint_name =  wrap_name;
            std::string PMT_name_0 = "0_Ch_+"+ wrap_name ;
            std::string PMT_name_1 = "1_Ch_+"+ wrap_name ;

            wrap_name = "Wrap" + wrap_name;



            new G4PVPlacement(0,
                    G4ThreeVector(33.1627*mm*cos(a*0.03049080341045739),
                        33.1627*mm*sin(a*0.03049080341045739),0),
                    heat_shrink_logical,
                    wrap_name,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(33.1627*mm*cos(a*0.03049080341045739),
                        33.1627*mm*sin(a*0.03049080341045739),0),
                    cladding_logical,
                    clad_name,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(33.1627*mm*cos(a*0.03049080341045739),
                        33.1627*mm*sin(a*0.03049080341045739),0),
                    scint_logical,
                    scint_name,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(33.1627*mm*cos(a*0.03049080341045739),
                        33.1627*mm*sin(a*0.03049080341045739),55*mm),
                    PMT_logical,
                    PMT_name_0,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(33.1627*mm*cos(a*0.03049080341045739),
                        33.1627*mm*sin(a*0.03049080341045739),-55*mm),
                    PMT_logical,
                    PMT_name_1,
                    World_log,
                    false,
                    0);


            if(a!=0)
            {
                int b = -a;
                std::string wrap_name = std::to_string(b);
                std::string clad_name =  "Clad" + wrap_name;
                std::string scint_name =wrap_name;
                std::string PMT_name_0 = "0_Ch_"+ wrap_name ;
                std::string PMT_name_1 = "1_Ch_"+ wrap_name ;
                wrap_name  = "Wrap" + wrap_name;



                new G4PVPlacement(0,
                        G4ThreeVector(33.1627*mm*cos(b*0.03049080341045739),
                            33.1627*mm*sin(b*0.03049080341045739),0),
                        heat_shrink_logical,
                        wrap_name,
                        World_log,
                        false,
                        0);

                new G4PVPlacement(0,
                        G4ThreeVector(33.1627*mm*cos(b*0.03049080341045739),
                            33.1627*mm*sin(b*0.03049080341045739),0),
                        cladding_logical,
                        clad_name,
                        World_log,
                        false,
                        0);

                new G4PVPlacement(0,
                        G4ThreeVector(33.1627*mm*cos(b*0.03049080341045739),
                            33.1627*mm*sin(b*0.03049080341045739),0),
                        scint_logical,
                        scint_name,
                        World_log,
                        false,
                        0);

                new G4PVPlacement(0,
                        G4ThreeVector(33.1627*mm*cos(b*0.03049080341045739),
                            33.1627*mm*sin(b*0.03049080341045739),55*mm),
                        PMT_logical,
                        PMT_name_0,
                        World_log,
                        false,
                        0);

                new G4PVPlacement(0,
                        G4ThreeVector(33.1627*mm*cos(b*0.03049080341045739),
                            33.1627*mm*sin(b*0.03049080341045739),-55*mm),
                        PMT_logical,
                        PMT_name_1,
                        World_log,
                        false,
                        0);

            }


        }


        for(float a=37.5; a < 67; a++)
        {

            std::string wrap_name = std::to_string(a);
            std::string clad_name =  "2_Clad" + wrap_name;
            std::string scint_name =  wrap_name;
            std::string PMT_name_0 = "2_0_Ch_+"+ wrap_name ;
            std::string PMT_name_1 = "2_1_Ch_+"+ wrap_name ;

            wrap_name = "2_Wrap" + wrap_name;



            new G4PVPlacement(0,
                    G4ThreeVector(34.052 *mm*cos(a*0.03049080341045739),
                        34.052 *mm*sin(a*0.03049080341045739),0),
                    heat_shrink_logical,
                    wrap_name,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(34.052 *mm*cos(a*0.03049080341045739),
                        34.052 *mm*sin(a*0.03049080341045739),0),
                    cladding_logical,
                    clad_name,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(34.052 *mm*cos(a*0.03049080341045739),
                        34.052 *mm*sin(a*0.03049080341045739),0),
                    scint_logical,
                    scint_name,
                    World_log,
                    false,
                    0);

            new G4PVPlacement(0,
                    G4ThreeVector(34.052 *mm*cos(a*0.03049080341045739),
                        34.052 *mm*sin(a*0.03049080341045739),55*mm),
                    PMT_logical,
                    PMT_name_0,
                    World_log,
                    false,
                    0);





            if(a!=0)
            {
                float b = -a;
                std::string wrap_name = std::to_string(b);
                std::string clad_name =  "2_Clad" + wrap_name;
                std::string scint_name =  wrap_name;
                std::string PMT_name_0 = "2_0_Ch_"+ wrap_name ;
                std::string PMT_name_1 = "2_1_Ch_"+ wrap_name ;
                wrap_name  = "2_Wrap" + wrap_name;



                new G4PVPlacement(0,
                        G4ThreeVector(34.052 *mm*cos(b*0.03049080341045739),
                            34.052 *mm*sin(b*0.03049080341045739),0),
                        heat_shrink_logical,
                        wrap_name,
                        World_log,
                        false,
                        0);

                new G4PVPlacement(0,
                        G4ThreeVector(34.052 *mm*cos(b*0.03049080341045739),
                            34.052 *mm*sin(b*0.03049080341045739),0),
                        cladding_logical,
                        clad_name,
                        World_log,
                        false,
                        0);

                new G4PVPlacement(0,
                        G4ThreeVector(34.052 *mm*cos(b*0.03049080341045739),
                            34.052 *mm*sin(b*0.03049080341045739),0),
                        scint_logical,
                        scint_name,
                        World_log,
                        false,
                        0);

                new G4PVPlacement(0,
                        G4ThreeVector(34.052 *mm*cos(b*0.03049080341045739),
                            34.052 *mm*sin(b*0.03049080341045739),55*mm),
                        PMT_logical,
                        PMT_name_0,
                        World_log,
                        false,
                        0);


            }








        }




    }



    return World_phys;

}

void IsotopeDetectorConstruction::ConstructSDandField()
{
    G4SDManager::GetSDMpointer()->SetVerboseLevel(1);

    //
    //Scorers


    //Declare scintillator as a sensitive detector 
    //

    auto ScintillatorSD = new SensitiveDetector("ScintillatorSD","ScintHC");
    G4SDManager::GetSDMpointer()->AddNewDetector(ScintillatorSD);
    SetSensitiveDetector("scint_logical",ScintillatorSD);

    auto PMTSD = new PMT_SD("PMTSD","PMTHC");
    G4SDManager::GetSDMpointer()->AddNewDetector(PMTSD);
    SetSensitiveDetector("PMT_logical",PMTSD);

    auto LYSODetSD = new botDetector("LYSODetSD","LYSODetHC");
    G4SDManager::GetSDMpointer()->AddNewDetector(LYSODetSD);
    SetSensitiveDetector("topLYSO_log",LYSODetSD);

    auto botDetSD = new botDetector("botDetSD","botDetHC");
    G4SDManager::GetSDMpointer()->AddNewDetector(botDetSD);
    SetSensitiveDetector("bot_log",botDetSD);
}







