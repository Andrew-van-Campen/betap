
#include "DetectorHit.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"

G4ThreadLocal G4Allocator<DetectorHit>* DetectorHitAllocator;

DetectorHit::DetectorHit()
: G4VHit(),
  edep(0.),
  pos(G4ThreeVector()),
  part_name(""),
  is_annh(false),
  scint_name("")
{;}

DetectorHit::~DetectorHit()
{
 
}

DetectorHit::DetectorHit(const DetectorHit& right)
: G4VHit(),
  edep(right.edep),
  pos(right.pos),
  is_annh(right.is_annh),
  part_name(right.part_name),
  scint_name(right.scint_name)
{}


const DetectorHit& DetectorHit::operator=(const DetectorHit &right)
{
  edep = right.edep;
  pos = right.pos;
  is_annh = right.is_annh;
  part_name = right.part_name;
  scint_name = right.scint_name;
  return *this;
  
}

G4bool DetectorHit::operator==(const DetectorHit& right) const
{
  return ( this == &right ) ? true : false;
}

void DetectorHit::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
    {
      G4Circle circle(pos);
      circle.SetScreenSize(0.04);
      circle.SetFillStyle(G4Circle::filled);
      G4Colour colour(1.,0.,0.);
      G4VisAttributes attribs(colour);
      circle.SetVisAttributes(attribs);
      pVVisManager->Draw(circle);
    }
  
}




void DetectorHit::Print()
{;}


// This is a forward declarations of an instantiated G4Allocator<Type> object.
// It has been added in order to make code portable for the GNU g++ 
// (release 2.7.2) compiler. 
// Whenever a new Type is instantiated via G4Allocator, it has to be forward
// declared to make object code (compiled with GNU g++) link successfully. 
// 
#ifdef GNU_GCC
  template class G4Allocator<DetectorHit>;
#endif










