using namespace std;

#include "IsotopeStackingAction.hh"
#include "G4EventManager.hh"
#include "G4Event.hh"
#include "G4Track.hh"
#include "G4TrackStatus.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4ios.hh"
#include "G4VTouchable.hh"





IsotopeStackingAction::IsotopeStackingAction()
{
  

}
IsotopeStackingAction::~IsotopeStackingAction(){}
G4ClassificationOfNewTrack IsotopeStackingAction::ClassifyNewTrack(const G4Track * aTrack)
{
  //get current event


G4ClassificationOfNewTrack     classification = fUrgent;

//check to make sure that there is a creation process to avoid seg faults. If there is no creator process, return urgent
if(aTrack->GetCreatorProcess()==0){return fUrgent;}
IsotopeEventAction* evt = (IsotopeEventAction*)G4EventManager::GetEventManager()->GetUserEventAction();
      
//check to see if there has been an annhilation photon yet. If there has, return fUrgent
if(evt->GetAnnh()){
    return fUrgent;
   }
//Check if the creator process is annihilation, if it is then sent to waiting stack,otherwise return fUrgent.
      else if(aTrack->GetCreatorProcess()->GetProcessName()=="annihil"){
              return fWaiting ;

            
              }
      else{
              return fUrgent;
         
            
              }
 


  return classification;    
}

