//#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
//#else
#include "G4RunManager.hh"
//#endif

#include "PhysicsList.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UIsession.hh"
#include "G4UItcsh.hh"
#include "IsotopeDetectorConstruction.hh"


#include "FTFP_BERT.hh"
#include "G4OpticalPhysics.hh"
#include "G4EmStandardPhysics_option4.hh"
#include "G4DecayPhysics.hh"
#include "G4RadioactiveDecayPhysics.hh"
#include "G4EmPenelopePhysics.hh"

#include "PrimaryGeneratorAction.hh"
#include "IsotopeRunAction.hh"
#include "IsotopeEventAction.hh"
#include "IsotopeSteppingAction.hh"
#include "DetectorHit.hh"
#include "SensitiveDetector.hh"
//#include "thulium170.hh"

#include "G4UIExecutive.hh"

#include "VisManager.hh"



#include "G4VisExecutive.hh"


#include "IsotopeStackingAction.hh"

#include "IsotopeActionInitialization.hh"



#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>
using namespace std;

namespace {
  void PrintUsage() {
    G4cerr << " Usage: " << G4endl;
    G4cerr << " BetaP [-n name] [-m macro ] [-u UIsession] [-g geometry (linear,linear_2, mesh, LYSO)] [-o optical (On, Off)] [-s separation] [-d depth]" << G4endl;
    G4cerr << "   note: -t option is available only for multi-threaded mode."
           << G4endl;
  }
}


int main(int argc, char** argv)
{
  
  
  G4String fileName="";
  long int Tid=0;


// we are going to include different arguments for calling the function. 

// The first one will be to call the filename - Default will be the timestamp string
// The second one will be to change the geometry - Default will be the CADMESH geomtry
// The third one will be to control the optical - Default will be no optical
// The fourth will be to perform any macro files - Default is non 
// All three will be optional

G4String Name = "";
G4String geo = "linear_2";
G4String opt = "Off";
G4String session;
G4String sep = "6";
G4String depth = "2"; 
  for ( G4int i=1; i<argc; i=i+2){
    if      ( G4String(argv[i]) == "-m" ) fileName = argv[i+1];
    else if (G4String(argv[i]) == "-u") session = argv[i+1];
    else if (G4String(argv[i]) == "-g") geo = argv[i+1];
    else if (G4String(argv[i]) == "-o") opt = argv[i+1];
    else if (G4String(argv[i]) == "-n") Name = argv[i+1];
    else if (G4String(argv[i]) == "-s") sep = argv[i+1];
    else if (G4String(argv[i]) == "-d") depth = argv[i+1];
   
  //  #ifdef G4MULTITHREADED
  // else if ( G4String(argv[i]) == "-t" ) {
  //  nThreads = G4UIcommand::ConvertToInt(argv[i+1]);
  //  }
    //#endif
  }


 // Detect interactive mode (if no macro provided) and define UI session
  //
  G4UIExecutive* ui = nullptr;
  if ( ! fileName.size() ) {
    ui = new G4UIExecutive(argc, argv, session);
  }


  CLHEP::RanecuEngine *theRanGenerator = new CLHEP::RanecuEngine;
  theRanGenerator->setSeed(Tid);
  CLHEP::HepRandom::setTheEngine(theRanGenerator);
  G4cout <<Tid << G4endl;

    
  //----------------------------------------------------------   
 // Construct the default run manager
#ifdef G4MULTITHREADED
    G4MTRunManager *runManager = new G4MTRunManager;
    runManager->SetNumberOfThreads(6);
#else
    G4RunManager *runManager = new G4RunManager;
#endif
  //----------------------------------------------------------
  // set mandatory initialization classes


//Passes geo to the detector construciton class so that it can place the correct geometries.
 IsotopeDetectorConstruction* Detector = new IsotopeDetectorConstruction(geo,sep,depth,"phantom.egsphant");
 runManager->SetUserInitialization(Detector);

//Can change physics lists based on if we want optical tracking or not. 
if(opt=="On"){
G4VModularPhysicsList* physicsList = new FTFP_BERT;
  physicsList->ReplacePhysics(new G4EmPenelopePhysics());

  G4DecayPhysics* decay = new G4DecayPhysics();
  G4RadioactiveDecayPhysics* rad_decay = new G4RadioactiveDecayPhysics();
  physicsList->RegisterPhysics(decay);
  physicsList->RegisterPhysics(rad_decay);


  G4OpticalPhysics* opticalPhysics = new G4OpticalPhysics();
  opticalPhysics->SetWLSTimeProfile("delta");

  opticalPhysics->SetScintillationYieldFactor(1.0);
  opticalPhysics->SetScintillationExcitationRatio(0.0);

  opticalPhysics->SetMaxNumPhotonsPerStep(0);
  opticalPhysics->SetMaxBetaChangePerStep(10.0);

  opticalPhysics->SetTrackSecondariesFirst(kCerenkov, true);
  opticalPhysics->SetTrackSecondariesFirst(kScintillation, true);

  physicsList->RegisterPhysics(opticalPhysics);
  runManager->SetUserInitialization(physicsList);
  physicsList->SetVerboseLevel(1);
  runManager->SetUserInitialization(physicsList);
 }

if(opt=="Off"){
 PhysicsList* physicsList = new PhysicsList();
  physicsList->SetVerboseLevel(1);
  runManager->SetUserInitialization(physicsList);
}
  


 auto actionInitialization = new IsotopeActionInitialization(Detector,Tid,Name);
  runManager->SetUserInitialization(actionInitialization);



  // Initialize visualization
  //
auto visManager = new G4VisExecutive;
  // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
  // G4VisManager* visManager = new G4VisExecutive("Quiet");
  visManager->Initialize();

  // Get the pointer to the User Interface manager
  auto UImanager = G4UImanager::GetUIpointer();

// Process macro or start UI session
  //
  if ( fileName.size() ) {
    // batch mode
    G4String command = "/control/execute ";
    UImanager->ApplyCommand(command+fileName);
  }
  else  {  
    // interactive mode : define UI session
    UImanager->ApplyCommand("/control/execute init_vis.mac");
    if (ui->IsGUI()) {
      UImanager->ApplyCommand("/control/execute gui.mac");
    }
    ui->SessionStart();
    delete ui;
  }





 G4cout<<"Slutet:  Main"<<G4endl;
 
 // job termination
#ifdef G4VIS_USE
 delete visManager;
#endif
 delete runManager;
 
  G4cout<<"All deleting done"<<G4endl;
   


   
   //return 0;
}




